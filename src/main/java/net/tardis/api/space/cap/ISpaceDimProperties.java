package net.tardis.api.space.cap;

import net.minecraft.entity.Entity;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.nbt.INBT;
import net.minecraft.util.Direction;
import net.minecraft.util.math.vector.Vector3d;
import net.minecraftforge.common.capabilities.Capability;
import net.minecraftforge.common.capabilities.ICapabilitySerializable;
import net.minecraftforge.common.util.INBTSerializable;
import net.minecraftforge.common.util.LazyOptional;

public interface ISpaceDimProperties extends INBTSerializable<CompoundNBT>{

    Vector3d modMotion(Entity ent);

    default boolean hasAir() {
        return true;
    }
    
    public static class Storage implements Capability.IStorage<ISpaceDimProperties> {

        @Override
        public INBT writeNBT(Capability<ISpaceDimProperties> capability, ISpaceDimProperties instance, Direction side) {
            return instance.serializeNBT();
        }

        @Override
        public void readNBT(Capability<ISpaceDimProperties> capability, ISpaceDimProperties instance, Direction side, INBT nbt) {
            instance.deserializeNBT((CompoundNBT) nbt);
        }

    }
    
    public static class Provider implements ICapabilitySerializable<CompoundNBT> {

        LazyOptional<ISpaceDimProperties> opt;

        public Provider(ISpaceDimProperties loc) {
            this.opt = LazyOptional.of(() -> loc);
        }

        @SuppressWarnings("unchecked")
        @Override
        public <T> LazyOptional<T> getCapability(Capability<T> cap, Direction side) {
            return cap == SpaceCapabilities.SPACE_DIM_PROPERTIES ? (LazyOptional<T>) opt : LazyOptional.empty();
        }

        @Override
        public CompoundNBT serializeNBT() {
            return opt.orElse(null).serializeNBT();
        }

        @Override
        public void deserializeNBT(CompoundNBT nbt) {
            opt.ifPresent(cap -> cap.deserializeNBT(nbt));
        }

    }
}
