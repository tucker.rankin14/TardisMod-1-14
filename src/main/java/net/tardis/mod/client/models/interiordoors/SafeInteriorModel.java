package net.tardis.mod.client.models.interiordoors;

import com.mojang.blaze3d.matrix.MatrixStack;
import com.mojang.blaze3d.systems.RenderSystem;
import com.mojang.blaze3d.vertex.IVertexBuilder;

import net.minecraft.client.renderer.entity.model.EntityModel;
import net.minecraft.client.renderer.model.ModelRenderer;
import net.minecraft.entity.Entity;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.math.vector.Vector3f;
import net.tardis.mod.Tardis;
import net.tardis.mod.entity.DoorEntity;
import net.tardis.mod.misc.IDoorType.EnumDoorType;

// Made with Blockbench 3.7.4
// Exported for Minecraft version 1.15
// Paste this class into your mod and generate all required imports


public class SafeInteriorModel extends EntityModel<Entity> implements IInteriorDoorRenderer{
	public static final ResourceLocation TEXTURE = new ResourceLocation(Tardis.MODID, "textures/exteriors/interior/safe.png");
	
	private final ModelRenderer door;
	private final ModelRenderer bb_main;

	public SafeInteriorModel() {
		textureWidth = 128;
		textureHeight = 128;

		door = new ModelRenderer(this);
		door.setRotationPoint(-7.0F, 8.0F, 6.0F);
		door.setTextureOffset(30, 35).addBox(0.0F, -16.0F, -0.05F, 14.0F, 32.0F, 1.0F, 0.0F, false);
		door.setTextureOffset(36, 14).addBox(-0.5F, -14.0F, -1.075F, 2.0F, 6.0F, 2.0F, 0.0F, false);
		door.setTextureOffset(36, 14).addBox(-0.5F, 8.0F, -1.075F, 2.0F, 6.0F, 2.0F, 0.0F, false);
		door.setTextureOffset(36, 30).addBox(5.0F, -9.0F, -0.55F, 1.0F, 4.0F, 1.0F, 0.0F, false);
		door.setTextureOffset(36, 30).addBox(5.0F, 5.0F, -0.55F, 1.0F, 4.0F, 1.0F, 0.0F, false);
		door.setTextureOffset(36, 8).addBox(6.5F, -2.5F, -0.55F, 5.0F, 5.0F, 1.0F, 0.0F, false);
		door.setTextureOffset(36, 4).addBox(0.0F, -8.5F, -0.075F, 14.0F, 3.0F, 1.0F, 0.0F, false);
		door.setTextureOffset(36, 4).addBox(0.0F, 5.5F, -0.075F, 14.0F, 3.0F, 1.0F, 0.0F, false);

		bb_main = new ModelRenderer(this);
		bb_main.setRotationPoint(0.0F, 24.0F, 0.0F);
		bb_main.setTextureOffset(0, 0).addBox(-8.0F, -33.0F, 6.0F, 16.0F, 33.0F, 2.0F, 0.0F, false);
		bb_main.setTextureOffset(0, 35).addBox(-7.0F, -32.0F, 6.975F, 14.0F, 32.0F, 1.0F, 0.0F, false);
	}

    @Override
    public void setRotationAngles(Entity entity, float limbSwing, float limbSwingAmount, float ageInTicks, float netHeadYaw, float headPitch){
        //previously the render function, render code was moved to a method below
    }

    @Override
    public void render(MatrixStack matrixStack, IVertexBuilder buffer, int packedLight, int packedOverlay, float red, float green, float blue, float alpha){
        
    }

    public void setRotationAngle(ModelRenderer modelRenderer, float x, float y, float z) {
        modelRenderer.rotateAngleX = x;
        modelRenderer.rotateAngleY = y;
        modelRenderer.rotateAngleZ = z;
    }

    @Override
    public void render(DoorEntity door, MatrixStack matrixStack, IVertexBuilder buffer, int packedLight,
            int packedOverlay) {
        matrixStack.push();
        RenderSystem.disableCull(); //This is a no-no, but specific for safe interior because of 0 size parts :(
        matrixStack.rotate(Vector3f.YP.rotationDegrees(180));
        matrixStack.translate(0, 0, 0.085);
        this.door.rotateAngleY = (float) Math.toRadians(EnumDoorType.SAFE.getRotationForState(door.getOpenState()));
        
        this.door.render(matrixStack, buffer, packedLight, packedOverlay);
        bb_main.render(matrixStack, buffer, packedLight, packedOverlay);
        RenderSystem.enableCull();
        matrixStack.pop();
        
    }

    @Override
    public ResourceLocation getTexture() {
        return TEXTURE;
    }
}