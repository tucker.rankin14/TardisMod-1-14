package net.tardis.mod.client.models.exteriors;

import com.mojang.blaze3d.matrix.MatrixStack;
import com.mojang.blaze3d.systems.RenderSystem;
import com.mojang.blaze3d.vertex.IVertexBuilder;

import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.RenderType;
import net.minecraft.client.renderer.model.ModelRenderer;
import net.minecraft.client.renderer.texture.OverlayTexture;
import net.minecraft.client.world.ClientWorld;
import net.minecraft.entity.Entity;
import net.minecraft.util.math.vector.Vector3f;
//import net.tardis.mod.entity.TardisEntity;
import net.tardis.mod.client.models.LightModelRenderer;
import net.tardis.mod.client.renderers.boti.BOTIRenderer;
import net.tardis.mod.client.renderers.boti.PortalInfo;
import net.tardis.mod.client.renderers.exteriors.ExteriorRenderer;
import net.tardis.mod.client.renderers.exteriors.SteamExteriorRenderer;
import net.tardis.mod.enums.EnumDoorState;
import net.tardis.mod.helper.WorldHelper;
//import net.tardis.mod.enums.EnumDoorState;
//import net.tardis.mod.helper.ModelHelper;
import net.tardis.mod.tileentities.exteriors.ExteriorTile;

// Made with Blockbench 3.7.4
// Exported for Minecraft version 1.15
// Paste this class into your mod and generate all required imports


public class SteamExteriorModel extends ExteriorModel {
	private final LightModelRenderer glow;
	private final ModelRenderer glow_toplamp_glass;
	private final ModelRenderer glow_front_window;
	private final ModelRenderer glow_roof_windows_front;
	private final ModelRenderer glow_side_1;
	private final ModelRenderer glow_side_window_1;
	private final ModelRenderer glow_roof_windows_side_1;
	private final ModelRenderer glow_side_2;
	private final ModelRenderer glow_side_window_2;
	private final ModelRenderer glow_roof_windows_side_2;
	private final ModelRenderer glow_side_3;
	private final ModelRenderer glow_side_window_3;
	private final ModelRenderer glow_roof_windows_side_3;
	private final ModelRenderer glow_side_4;
	private final ModelRenderer glow_side_window_4;
	private final ModelRenderer glow_roof_windows_side_4;
	private final ModelRenderer glow_side_5;
	private final ModelRenderer glow_side_window_5;
	private final ModelRenderer glow_roof_windows_side_5;
	private final ModelRenderer door_rotate_y;
	private final ModelRenderer handle;
	private final ModelRenderer lock;
	private final ModelRenderer kickplate;
	private final ModelRenderer hinge;
	private final ModelRenderer boti;
	private final ModelRenderer box;
	private final ModelRenderer Lamp;
	private final ModelRenderer lamp_ribs_1;
	private final ModelRenderer lamp_ribs_2;
	private final ModelRenderer lamp_ribs_3;
	private final ModelRenderer lamp_base;
	private final ModelRenderer lamp_cap;
	private final ModelRenderer panel_front;
	private final ModelRenderer floorboards;
	private final ModelRenderer roof_front;
	private final ModelRenderer center_peak;
	private final ModelRenderer wroughtiron_front;
	private final ModelRenderer outer_peak;
	private final ModelRenderer panel_front_top;
	private final ModelRenderer panel_side_1;
	private final ModelRenderer side_1;
	private final ModelRenderer top_panel_1;
	private final ModelRenderer cage;
	private final ModelRenderer middle_panel_1;
	private final ModelRenderer lower_panel_1;
	private final ModelRenderer componets;
	private final ModelRenderer panelbox;
	private final ModelRenderer panelbox2;
	private final ModelRenderer floorboards_1;
	private final ModelRenderer roof_side_1;
	private final ModelRenderer center_peak_1;
	private final ModelRenderer wroughtiron_front_1;
	private final ModelRenderer outer_peak_1;
	private final ModelRenderer panel_side_2;
	private final ModelRenderer side_2;
	private final ModelRenderer top_panel_2;
	private final ModelRenderer cage2;
	private final ModelRenderer middle_panel_2;
	private final ModelRenderer lower_panel_2;
	private final ModelRenderer componets2;
	private final ModelRenderer panelbox3;
	private final ModelRenderer panelbox4;
	private final ModelRenderer floorboards_2;
	private final ModelRenderer roof_side_2;
	private final ModelRenderer center_peak_2;
	private final ModelRenderer wroughtiron_front_2;
	private final ModelRenderer outer_peak_2;
	private final ModelRenderer panel_side_3;
	private final ModelRenderer roof_side_3;
	private final ModelRenderer center_peak_3;
	private final ModelRenderer wroughtiron_front_3;
	private final ModelRenderer outer_peak_3;
	private final ModelRenderer side_3;
	private final ModelRenderer top_panel_3;
	private final ModelRenderer cage3;
	private final ModelRenderer middle_panel_3;
	private final ModelRenderer lower_panel_3;
	private final ModelRenderer componets3;
	private final ModelRenderer gear;
	private final ModelRenderer gear2;
	private final ModelRenderer gear3;
	private final ModelRenderer panelbox5;
	private final ModelRenderer panelbox6;
	private final ModelRenderer floorboards_3;
	private final ModelRenderer panel_side_4;
	private final ModelRenderer roof_side_4;
	private final ModelRenderer center_peak_4;
	private final ModelRenderer wroughtiron_front_4;
	private final ModelRenderer outer_peak_4;
	private final ModelRenderer side_4;
	private final ModelRenderer top_panel_4;
	private final ModelRenderer cage4;
	private final ModelRenderer middle_panel_4;
	private final ModelRenderer lower_panel_4;
	private final ModelRenderer gear4;
	private final ModelRenderer componets4;
	private final ModelRenderer panelbox7;
	private final ModelRenderer panelbox8;
	private final ModelRenderer floorboards_4;
	private final ModelRenderer panel_side_5;
	private final ModelRenderer roof_side_5;
	private final ModelRenderer center_peak_5;
	private final ModelRenderer wroughtiron_front_5;
	private final ModelRenderer outer_peak_5;
	private final ModelRenderer side_5;
	private final ModelRenderer top_panel_5;
	private final ModelRenderer cage5;
	private final ModelRenderer middle_panel_5;
	private final ModelRenderer lower_panel_5;
	private final ModelRenderer componets5;
	private final ModelRenderer panelbox9;
	private final ModelRenderer panelbox10;
	private final ModelRenderer floorboards_5;
	private final ModelRenderer hexspine;
	private final ModelRenderer crossspine;
	private final ModelRenderer leftspine;
	private final ModelRenderer upperrails;
	private final ModelRenderer upperrail_1;
	private final ModelRenderer upperrails2;
	private final ModelRenderer upperrail_2;
	private final ModelRenderer upperrails3;
	private final ModelRenderer upperrail_3;
	private final ModelRenderer upperrails4;
	private final ModelRenderer upperrail_4;
	private final ModelRenderer upperrails5;
	private final ModelRenderer upperrail_5;
	private final ModelRenderer upperrails6;
	private final ModelRenderer upperrail_6;
	private final ModelRenderer rightspine;

	public SteamExteriorModel() {
		textureWidth = 1024;
		textureHeight = 1024;

		glow = new LightModelRenderer(this);
		glow.setRotationPoint(0.0F, 24.0F, 0.0F);
		

		glow_toplamp_glass = new ModelRenderer(this);
		glow_toplamp_glass.setRotationPoint(0.0F, 0.0F, 0.0F);
		glow.addChild(glow_toplamp_glass);
		glow_toplamp_glass.setTextureOffset(512, 146).addBox(-4.0F, -240.0F, -6.0F, 8.0F, 20.0F, 4.0F, 0.0F, false);
		glow_toplamp_glass.setTextureOffset(512, 146).addBox(-4.0F, -240.0F, 6.0F, 8.0F, 20.0F, 4.0F, 0.0F, false);
		glow_toplamp_glass.setTextureOffset(512, 146).addBox(4.0F, -240.0F, -2.0F, 4.0F, 20.0F, 8.0F, 0.0F, false);
		glow_toplamp_glass.setTextureOffset(512, 146).addBox(-8.0F, -240.0F, -2.0F, 4.0F, 20.0F, 8.0F, 0.0F, false);

		glow_front_window = new ModelRenderer(this);
		glow_front_window.setRotationPoint(0.0F, -3.7778F, 1.6667F);
		glow.addChild(glow_front_window);
		glow_front_window.setTextureOffset(525, 162).addBox(6.0F, -176.2222F, -51.8667F, 12.0F, 12.0F, 8.0F, 0.0F, false);
		glow_front_window.setTextureOffset(507, 157).addBox(-18.0F, -176.2222F, -51.8667F, 12.0F, 12.0F, 8.0F, 0.0F, false);

		glow_roof_windows_front = new ModelRenderer(this);
		glow_roof_windows_front.setRotationPoint(0.0F, -191.7778F, -30.3333F);
		glow.addChild(glow_roof_windows_front);
		glow_roof_windows_front.setTextureOffset(464, 150).addBox(-18.6F, -12.2222F, -0.6667F, 36.0F, 8.0F, 4.0F, 0.0F, false);

		glow_side_1 = new ModelRenderer(this);
		glow_side_1.setRotationPoint(0.0F, 0.0F, 2.0F);
		glow.addChild(glow_side_1);
		setRotationAngle(glow_side_1, 0.0F, -1.0472F, 0.0F);
		

		glow_side_window_1 = new ModelRenderer(this);
		glow_side_window_1.setRotationPoint(0.0F, -160.0F, -55.5F);
		glow_side_1.addChild(glow_side_window_1);
		glow_side_window_1.setTextureOffset(505, 137).addBox(-12.0F, -12.0F, -3.5F, 24.0F, 24.0F, 16.0F, 0.0F, false);
		glow_side_window_1.setTextureOffset(505, 137).addBox(-10.0F, -15.0F, -1.5F, 20.0F, 4.0F, 14.0F, 0.0F, false);
		glow_side_window_1.setTextureOffset(505, 137).addBox(-10.0F, 11.0F, -1.5F, 20.0F, 4.0F, 14.0F, 0.0F, false);
		glow_side_window_1.setTextureOffset(505, 137).addBox(11.0F, -10.0F, -1.5F, 4.0F, 20.0F, 14.0F, 0.0F, false);
		glow_side_window_1.setTextureOffset(505, 137).addBox(-15.0F, -10.0F, -1.5F, 4.0F, 20.0F, 14.0F, 0.0F, false);

		glow_roof_windows_side_1 = new ModelRenderer(this);
		glow_roof_windows_side_1.setRotationPoint(0.0F, -191.7778F, -32.3333F);
		glow_side_1.addChild(glow_roof_windows_side_1);
		glow_roof_windows_side_1.setTextureOffset(505, 137).addBox(-19.0F, -12.2222F, -0.6667F, 36.0F, 8.0F, 4.0F, 0.0F, false);

		glow_side_2 = new ModelRenderer(this);
		glow_side_2.setRotationPoint(0.0F, 0.0F, 2.0F);
		glow.addChild(glow_side_2);
		setRotationAngle(glow_side_2, 0.0F, -2.0944F, 0.0F);
		

		glow_side_window_2 = new ModelRenderer(this);
		glow_side_window_2.setRotationPoint(0.0F, -160.0F, -55.5F);
		glow_side_2.addChild(glow_side_window_2);
		glow_side_window_2.setTextureOffset(502, 130).addBox(-12.0F, -12.0F, -3.5F, 24.0F, 24.0F, 16.0F, 0.0F, false);
		glow_side_window_2.setTextureOffset(502, 130).addBox(-10.0F, -15.0F, -1.5F, 20.0F, 4.0F, 14.0F, 0.0F, false);
		glow_side_window_2.setTextureOffset(502, 130).addBox(-10.0F, 11.0F, -1.5F, 20.0F, 4.0F, 14.0F, 0.0F, false);
		glow_side_window_2.setTextureOffset(502, 130).addBox(11.0F, -10.0F, -1.5F, 4.0F, 20.0F, 14.0F, 0.0F, false);
		glow_side_window_2.setTextureOffset(502, 130).addBox(-15.0F, -10.0F, -1.5F, 4.0F, 20.0F, 14.0F, 0.0F, false);

		glow_roof_windows_side_2 = new ModelRenderer(this);
		glow_roof_windows_side_2.setRotationPoint(0.0F, -191.7778F, -32.3333F);
		glow_side_2.addChild(glow_roof_windows_side_2);
		glow_roof_windows_side_2.setTextureOffset(509, 132).addBox(-19.0F, -12.2222F, -0.6667F, 36.0F, 8.0F, 4.0F, 0.0F, false);

		glow_side_3 = new ModelRenderer(this);
		glow_side_3.setRotationPoint(0.0F, 0.0F, 2.0F);
		glow.addChild(glow_side_3);
		setRotationAngle(glow_side_3, 0.0F, 3.1416F, 0.0F);
		

		glow_side_window_3 = new ModelRenderer(this);
		glow_side_window_3.setRotationPoint(0.0F, -160.0F, -55.5F);
		glow_side_3.addChild(glow_side_window_3);
		glow_side_window_3.setTextureOffset(496, 130).addBox(-12.0F, -12.0F, -3.5F, 24.0F, 24.0F, 16.0F, 0.0F, false);
		glow_side_window_3.setTextureOffset(496, 130).addBox(-10.0F, -15.0F, -1.5F, 20.0F, 4.0F, 14.0F, 0.0F, false);
		glow_side_window_3.setTextureOffset(496, 130).addBox(-10.0F, 11.0F, -1.5F, 20.0F, 4.0F, 14.0F, 0.0F, false);
		glow_side_window_3.setTextureOffset(496, 130).addBox(11.0F, -10.0F, -1.5F, 4.0F, 20.0F, 14.0F, 0.0F, false);
		glow_side_window_3.setTextureOffset(496, 130).addBox(-15.0F, -10.0F, -1.5F, 4.0F, 20.0F, 14.0F, 0.0F, false);

		glow_roof_windows_side_3 = new ModelRenderer(this);
		glow_roof_windows_side_3.setRotationPoint(0.0F, -191.7778F, -32.3333F);
		glow_side_3.addChild(glow_roof_windows_side_3);
		glow_roof_windows_side_3.setTextureOffset(496, 130).addBox(-19.0F, -12.2222F, -0.6667F, 36.0F, 8.0F, 4.0F, 0.0F, false);

		glow_side_4 = new ModelRenderer(this);
		glow_side_4.setRotationPoint(0.0F, 0.0F, 2.0F);
		glow.addChild(glow_side_4);
		setRotationAngle(glow_side_4, 0.0F, 2.0944F, 0.0F);
		

		glow_side_window_4 = new ModelRenderer(this);
		glow_side_window_4.setRotationPoint(0.0F, -160.0F, -55.5F);
		glow_side_4.addChild(glow_side_window_4);
		glow_side_window_4.setTextureOffset(507, 134).addBox(-12.0F, -12.0F, -3.5F, 24.0F, 24.0F, 16.0F, 0.0F, false);
		glow_side_window_4.setTextureOffset(507, 134).addBox(-10.0F, -15.0F, -1.5F, 20.0F, 4.0F, 14.0F, 0.0F, false);
		glow_side_window_4.setTextureOffset(507, 134).addBox(-10.0F, 11.0F, -1.5F, 20.0F, 4.0F, 14.0F, 0.0F, false);
		glow_side_window_4.setTextureOffset(507, 134).addBox(11.0F, -10.0F, -1.5F, 4.0F, 20.0F, 14.0F, 0.0F, false);
		glow_side_window_4.setTextureOffset(507, 134).addBox(-15.0F, -10.0F, -1.5F, 4.0F, 20.0F, 14.0F, 0.0F, false);

		glow_roof_windows_side_4 = new ModelRenderer(this);
		glow_roof_windows_side_4.setRotationPoint(0.0F, -191.7778F, -32.3333F);
		glow_side_4.addChild(glow_roof_windows_side_4);
		glow_roof_windows_side_4.setTextureOffset(507, 134).addBox(-19.0F, -12.2222F, -0.6667F, 36.0F, 8.0F, 4.0F, 0.0F, false);

		glow_side_5 = new ModelRenderer(this);
		glow_side_5.setRotationPoint(0.0F, 0.0F, 2.0F);
		glow.addChild(glow_side_5);
		setRotationAngle(glow_side_5, 0.0F, 1.0472F, 0.0F);
		

		glow_side_window_5 = new ModelRenderer(this);
		glow_side_window_5.setRotationPoint(0.0F, -160.0F, -55.5F);
		glow_side_5.addChild(glow_side_window_5);
		glow_side_window_5.setTextureOffset(500, 132).addBox(-12.0F, -12.0F, -3.5F, 24.0F, 24.0F, 16.0F, 0.0F, false);
		glow_side_window_5.setTextureOffset(500, 132).addBox(-10.0F, -15.0F, -1.5F, 20.0F, 4.0F, 14.0F, 0.0F, false);
		glow_side_window_5.setTextureOffset(500, 132).addBox(-10.0F, 11.0F, -1.5F, 20.0F, 4.0F, 14.0F, 0.0F, false);
		glow_side_window_5.setTextureOffset(500, 132).addBox(11.0F, -10.0F, -1.5F, 4.0F, 20.0F, 14.0F, 0.0F, false);
		glow_side_window_5.setTextureOffset(500, 132).addBox(-15.0F, -10.0F, -1.5F, 4.0F, 20.0F, 14.0F, 0.0F, false);

		glow_roof_windows_side_5 = new ModelRenderer(this);
		glow_roof_windows_side_5.setRotationPoint(0.0F, -191.7778F, -32.3333F);
		glow_side_5.addChild(glow_roof_windows_side_5);
		glow_roof_windows_side_5.setTextureOffset(500, 132).addBox(-19.0F, -12.2222F, -0.6667F, 36.0F, 8.0F, 4.0F, 0.0F, false);

		door_rotate_y = new ModelRenderer(this);
		door_rotate_y.setRotationPoint(28.0F, -22.0F, -44.0F);
		door_rotate_y.setTextureOffset(171, 374).addBox(-56.0F, 14.0F, -4.0F, 56.0F, 20.0F, 4.0F, 0.0F, false);
		door_rotate_y.setTextureOffset(242, 25).addBox(-56.0F, -106.0F, -3.0F, 56.0F, 140.0F, 2.0F, 0.0F, false);
		door_rotate_y.setTextureOffset(162, 258).addBox(-56.0F, -106.0F, -4.0F, 56.0F, 8.0F, 4.0F, 0.0F, false);
		door_rotate_y.setTextureOffset(128, 274).addBox(-12.0F, -98.0F, -4.0F, 12.0F, 112.0F, 4.0F, 0.0F, false);
		door_rotate_y.setTextureOffset(173, 285).addBox(-56.0F, -98.0F, -4.0F, 12.0F, 112.0F, 4.0F, 0.0F, false);
		door_rotate_y.setTextureOffset(171, 262).addBox(-42.0F, -96.0F, -4.0F, 28.0F, 4.0F, 4.0F, 0.0F, false);
		door_rotate_y.setTextureOffset(173, 379).addBox(-42.0F, 4.0F, -4.0F, 28.0F, 8.0F, 4.0F, 0.0F, false);
		door_rotate_y.setTextureOffset(173, 285).addBox(-42.0F, -92.0F, -4.0F, 4.0F, 96.0F, 4.0F, 0.0F, false);
		door_rotate_y.setTextureOffset(173, 285).addBox(-18.0F, -92.0F, -4.0F, 4.0F, 96.0F, 4.0F, 0.0F, false);
		door_rotate_y.setTextureOffset(173, 269).addBox(-36.0F, -90.0F, -4.0F, 16.0F, 16.0F, 4.0F, 0.0F, false);
		door_rotate_y.setTextureOffset(182, 372).addBox(-36.0F, -14.0F, -4.0F, 16.0F, 16.0F, 4.0F, 0.0F, false);
		door_rotate_y.setTextureOffset(171, 269).addBox(-36.0F, -72.0F, -4.0F, 16.0F, 56.0F, 4.0F, 0.0F, false);

		handle = new ModelRenderer(this);
		handle.setRotationPoint(-4.0F, 34.0F, 0.0F);
		door_rotate_y.addChild(handle);
		handle.setTextureOffset(509, 43).addBox(-46.0F, -86.0F, -5.0F, 4.0F, 12.0F, 6.0F, 0.0F, false);

		lock = new ModelRenderer(this);
		lock.setRotationPoint(-44.0F, -69.8F, -3.8F);
		handle.addChild(lock);
		setRotationAngle(lock, 0.0873F, 0.0873F, -0.7854F);
		lock.setTextureOffset(509, 43).addBox(-2.0F, -2.0F, -1.0F, 4.0F, 4.0F, 2.0F, 0.0F, false);

		kickplate = new ModelRenderer(this);
		kickplate.setRotationPoint(-4.0F, 34.0F, 0.0F);
		door_rotate_y.addChild(kickplate);
		kickplate.setTextureOffset(466, 36).addBox(-46.0F, -18.0F, -5.0F, 44.0F, 16.0F, 4.0F, 0.0F, false);
		kickplate.setTextureOffset(466, 36).addBox(-45.0F, -17.0F, -5.8F, 2.0F, 2.0F, 4.0F, 0.0F, false);
		kickplate.setTextureOffset(466, 36).addBox(-45.0F, -5.0F, -5.8F, 2.0F, 2.0F, 4.0F, 0.0F, false);
		kickplate.setTextureOffset(466, 36).addBox(-5.0F, -5.0F, -5.8F, 2.0F, 2.0F, 4.0F, 0.0F, false);
		kickplate.setTextureOffset(466, 36).addBox(-5.0F, -17.0F, -5.8F, 2.0F, 2.0F, 4.0F, 0.0F, false);

		hinge = new ModelRenderer(this);
		hinge.setRotationPoint(0.0F, -44.0F, -0.2F);
		door_rotate_y.addChild(hinge);
		setRotationAngle(hinge, 0.0F, -0.6981F, 0.0F);
		hinge.setTextureOffset(497, 74).addBox(-2.0F, -54.0F, -2.0F, 4.0F, 12.0F, 4.0F, 0.0F, false);
		hinge.setTextureOffset(497, 74).addBox(-2.0F, -8.0F, -2.0F, 4.0F, 12.0F, 4.0F, 0.0F, false);
		hinge.setTextureOffset(497, 74).addBox(-2.0F, 44.0F, -2.0F, 4.0F, 12.0F, 4.0F, 0.0F, false);

		boti = new ModelRenderer(this);
		boti.setRotationPoint(0.0F, 24.0F, 0.0F);
		boti.setTextureOffset(0, 0).addBox(-31.0F, -196.0F, -43.0F, 61.0F, 192.0F, 0.0F, 0.0F, false);

		box = new ModelRenderer(this);
		box.setRotationPoint(0.0F, 24.0F, 0.0F);
		

		Lamp = new ModelRenderer(this);
		Lamp.setRotationPoint(0.0F, 0.0F, 0.0F);
		box.addChild(Lamp);
		

		lamp_ribs_1 = new ModelRenderer(this);
		lamp_ribs_1.setRotationPoint(0.0F, 0.0F, 0.0F);
		Lamp.addChild(lamp_ribs_1);
		lamp_ribs_1.setTextureOffset(697, 50).addBox(-6.0F, -225.0F, -7.0F, 12.0F, 2.0F, 4.0F, 0.0F, false);
		lamp_ribs_1.setTextureOffset(697, 50).addBox(-6.0F, -225.0F, 7.0F, 12.0F, 2.0F, 4.0F, 0.0F, false);
		lamp_ribs_1.setTextureOffset(697, 50).addBox(1.0F, -225.0F, -4.0F, 8.0F, 2.0F, 12.0F, 0.0F, false);
		lamp_ribs_1.setTextureOffset(697, 50).addBox(-9.0F, -225.0F, -4.0F, 8.0F, 2.0F, 12.0F, 0.0F, false);

		lamp_ribs_2 = new ModelRenderer(this);
		lamp_ribs_2.setRotationPoint(0.0F, 0.0F, 0.0F);
		Lamp.addChild(lamp_ribs_2);
		lamp_ribs_2.setTextureOffset(681, 54).addBox(-6.0F, -230.0F, -7.0F, 12.0F, 2.0F, 4.0F, 0.0F, false);
		lamp_ribs_2.setTextureOffset(681, 54).addBox(-6.0F, -230.0F, 7.0F, 12.0F, 2.0F, 4.0F, 0.0F, false);
		lamp_ribs_2.setTextureOffset(681, 54).addBox(1.0F, -230.0F, -4.0F, 8.0F, 2.0F, 12.0F, 0.0F, false);
		lamp_ribs_2.setTextureOffset(681, 54).addBox(-9.0F, -230.0F, -4.0F, 8.0F, 2.0F, 12.0F, 0.0F, false);

		lamp_ribs_3 = new ModelRenderer(this);
		lamp_ribs_3.setRotationPoint(0.0F, 0.0F, 0.0F);
		Lamp.addChild(lamp_ribs_3);
		lamp_ribs_3.setTextureOffset(688, 61).addBox(-6.0F, -235.0F, -7.0F, 12.0F, 2.0F, 4.0F, 0.0F, false);
		lamp_ribs_3.setTextureOffset(688, 61).addBox(-6.0F, -235.0F, 7.0F, 12.0F, 2.0F, 4.0F, 0.0F, false);
		lamp_ribs_3.setTextureOffset(688, 61).addBox(1.0F, -235.0F, -4.0F, 8.0F, 2.0F, 12.0F, 0.0F, false);
		lamp_ribs_3.setTextureOffset(688, 61).addBox(-9.0F, -235.0F, -4.0F, 8.0F, 2.0F, 12.0F, 0.0F, false);

		lamp_base = new ModelRenderer(this);
		lamp_base.setRotationPoint(0.0F, 0.0F, 0.0F);
		Lamp.addChild(lamp_base);
		lamp_base.setTextureOffset(701, 70).addBox(-6.0F, -220.0F, -7.0F, 12.0F, 4.0F, 4.0F, 0.0F, false);
		lamp_base.setTextureOffset(701, 70).addBox(-6.0F, -220.0F, 7.0F, 12.0F, 4.0F, 4.0F, 0.0F, false);
		lamp_base.setTextureOffset(701, 70).addBox(1.0F, -220.0F, -4.0F, 8.0F, 4.0F, 12.0F, 0.0F, false);
		lamp_base.setTextureOffset(701, 70).addBox(-9.0F, -220.0F, -4.0F, 8.0F, 4.0F, 12.0F, 0.0F, false);

		lamp_cap = new ModelRenderer(this);
		lamp_cap.setRotationPoint(0.0F, 0.0F, 0.0F);
		Lamp.addChild(lamp_cap);
		lamp_cap.setTextureOffset(697, 41).addBox(-6.0F, -242.0F, -7.0F, 12.0F, 4.0F, 8.0F, 0.0F, false);
		lamp_cap.setTextureOffset(697, 41).addBox(-6.0F, -242.0F, 3.0F, 12.0F, 4.0F, 8.0F, 0.0F, false);
		lamp_cap.setTextureOffset(697, 41).addBox(1.0F, -242.0F, -4.0F, 8.0F, 4.0F, 12.0F, 0.0F, false);
		lamp_cap.setTextureOffset(697, 41).addBox(-9.0F, -242.0F, -4.0F, 8.0F, 4.0F, 12.0F, 0.0F, false);
		lamp_cap.setTextureOffset(697, 41).addBox(-6.0F, -244.0F, -4.0F, 12.0F, 4.0F, 12.0F, 0.0F, false);
		lamp_cap.setTextureOffset(697, 41).addBox(-2.0F, -245.2F, 0.0F, 4.0F, 4.0F, 4.0F, 0.0F, false);

		panel_front = new ModelRenderer(this);
		panel_front.setRotationPoint(0.0F, -3.7778F, 1.6667F);
		box.addChild(panel_front);
		panel_front.setTextureOffset(160, 392).addBox(-28.0F, -8.2222F, -50.6667F, 56.0F, 8.0F, 4.0F, 0.0F, false);
		panel_front.setTextureOffset(148, 220).addBox(-28.0F, -180.2222F, -50.6667F, 4.0F, 172.0F, 4.0F, 0.0F, false);
		panel_front.setTextureOffset(288, 226).addBox(24.0F, -180.2222F, -50.6667F, 4.0F, 172.0F, 4.0F, 0.0F, false);
		panel_front.setTextureOffset(189, 253).addBox(-28.0F, -188.2222F, -50.6667F, 56.0F, 8.0F, 4.0F, 0.0F, false);
		panel_front.setTextureOffset(98, 299).addBox(-24.0F, -160.2222F, -50.6667F, 48.0F, 12.0F, 4.0F, 0.0F, false);
		panel_front.setTextureOffset(180, 285).addBox(-28.0F, -191.4222F, -52.6667F, 56.0F, 4.0F, 4.0F, 0.0F, false);
		panel_front.setTextureOffset(445, 54).addBox(-28.4F, -156.2222F, -52.2667F, 56.0F, 4.0F, 6.0F, 0.0F, false);

		floorboards = new ModelRenderer(this);
		floorboards.setRotationPoint(0.0F, 0.0F, 0.0F);
		panel_front.addChild(floorboards);
		floorboards.setTextureOffset(237, 185).addBox(-32.0F, -0.2222F, -58.6667F, 64.0F, 4.0F, 4.0F, 0.0F, false);
		floorboards.setTextureOffset(237, 185).addBox(-26.0F, -0.2222F, -46.6667F, 52.0F, 4.0F, 4.0F, 0.0F, false);
		floorboards.setTextureOffset(237, 185).addBox(-24.0F, -0.2222F, -42.6667F, 48.0F, 4.0F, 4.0F, 0.0F, false);
		floorboards.setTextureOffset(237, 185).addBox(-22.0F, -0.2222F, -38.6667F, 44.0F, 4.0F, 4.0F, 0.0F, false);
		floorboards.setTextureOffset(237, 185).addBox(-18.0F, -0.2222F, -34.6667F, 36.0F, 4.0F, 4.0F, 0.0F, false);
		floorboards.setTextureOffset(237, 185).addBox(-16.0F, -0.2222F, -30.6667F, 32.0F, 4.0F, 4.0F, 0.0F, false);
		floorboards.setTextureOffset(237, 185).addBox(-14.0F, -0.2222F, -26.6667F, 28.0F, 4.0F, 4.0F, 0.0F, false);
		floorboards.setTextureOffset(237, 185).addBox(-12.0F, -0.2222F, -22.6667F, 24.0F, 4.0F, 4.0F, 0.0F, false);
		floorboards.setTextureOffset(237, 185).addBox(-10.0F, -0.2222F, -18.6667F, 20.0F, 4.0F, 4.0F, 0.0F, false);
		floorboards.setTextureOffset(237, 185).addBox(-8.0F, -0.2222F, -14.6667F, 16.0F, 4.0F, 4.0F, 0.0F, false);
		floorboards.setTextureOffset(237, 185).addBox(-6.0F, -0.2222F, -10.6667F, 12.0F, 4.0F, 4.0F, 0.0F, false);
		floorboards.setTextureOffset(237, 185).addBox(-4.0F, -0.2222F, -6.6667F, 8.0F, 4.0F, 4.0F, 0.0F, false);
		floorboards.setTextureOffset(237, 185).addBox(-30.0F, -0.2222F, -54.6667F, 60.0F, 4.0F, 4.0F, 0.0F, false);
		floorboards.setTextureOffset(237, 185).addBox(-28.0F, -0.2222F, -50.6667F, 56.0F, 4.0F, 4.0F, 0.0F, false);

		roof_front = new ModelRenderer(this);
		roof_front.setRotationPoint(0.0F, -188.0F, -48.0F);
		panel_front.addChild(roof_front);
		

		center_peak = new ModelRenderer(this);
		center_peak.setRotationPoint(0.0F, 0.0F, 16.0F);
		roof_front.addChild(center_peak);
		setRotationAngle(center_peak, 0.1047F, 0.0F, 0.0F);
		center_peak.setTextureOffset(182, 299).addBox(-18.0F, -13.8222F, -1.4667F, 37.0F, 2.0F, 4.0F, 0.0F, false);
		center_peak.setTextureOffset(182, 299).addBox(-6.0F, -18.2222F, 22.5333F, 12.0F, 8.0F, 4.0F, 0.0F, false);
		center_peak.setTextureOffset(182, 299).addBox(-12.0F, -12.2222F, -1.0667F, 4.0F, 8.0F, 8.0F, 0.0F, false);
		center_peak.setTextureOffset(182, 299).addBox(-2.0F, -12.2222F, -1.0667F, 4.0F, 8.0F, 8.0F, 0.0F, false);
		center_peak.setTextureOffset(182, 299).addBox(8.0F, -12.2222F, -1.0667F, 4.0F, 8.0F, 8.0F, 0.0F, false);
		center_peak.setTextureOffset(180, 292).addBox(-16.0F, -13.8222F, 2.5333F, 32.0F, 2.0F, 4.0F, 0.0F, false);
		center_peak.setTextureOffset(173, 276).addBox(-14.0F, -13.8222F, 6.5333F, 28.0F, 2.0F, 4.0F, 0.0F, false);
		center_peak.setTextureOffset(180, 292).addBox(-12.0F, -13.8222F, 10.5333F, 24.0F, 2.0F, 4.0F, 0.0F, false);
		center_peak.setTextureOffset(187, 278).addBox(-10.0F, -13.8222F, 14.5333F, 20.0F, 2.0F, 4.0F, 0.0F, false);
		center_peak.setTextureOffset(185, 272).addBox(-8.0F, -13.8222F, 18.5333F, 16.0F, 2.0F, 4.0F, 0.0F, false);

		wroughtiron_front = new ModelRenderer(this);
		wroughtiron_front.setRotationPoint(0.0F, 191.7778F, 46.3333F);
		roof_front.addChild(wroughtiron_front);
		wroughtiron_front.setTextureOffset(6, 420).addBox(-32.0F, -220.0F, -54.0F, 64.0F, 24.0F, 0.0F, 0.0F, false);

		outer_peak = new ModelRenderer(this);
		outer_peak.setRotationPoint(0.0F, 0.0F, 0.0F);
		roof_front.addChild(outer_peak);
		setRotationAngle(outer_peak, 0.0873F, 0.0F, 0.0F);
		outer_peak.setTextureOffset(182, 299).addBox(-32.0F, -5.0222F, -10.6667F, 64.0F, 2.0F, 7.0F, 0.0F, false);
		outer_peak.setTextureOffset(182, 299).addBox(-29.8F, -3.0222F, -7.2667F, 60.0F, 2.0F, 8.0F, 0.0F, false);
		outer_peak.setTextureOffset(182, 299).addBox(-25.6F, -5.0222F, 0.3333F, 52.0F, 4.0F, 4.0F, 0.0F, false);
		outer_peak.setTextureOffset(134, 299).addBox(-23.6F, -5.0222F, 4.3333F, 48.0F, 4.0F, 4.0F, 0.0F, false);
		outer_peak.setTextureOffset(196, 285).addBox(-21.6F, -5.0222F, 8.3333F, 44.0F, 4.0F, 4.0F, 0.0F, false);
		outer_peak.setTextureOffset(182, 299).addBox(-19.6F, -5.0222F, 12.3333F, 40.0F, 4.0F, 4.0F, 0.0F, false);
		outer_peak.setTextureOffset(182, 299).addBox(-4.0F, -24.0222F, 42.3333F, 8.0F, 20.0F, 4.0F, 0.0F, false);
		outer_peak.setTextureOffset(196, 290).addBox(-27.6F, -5.0222F, -3.6667F, 56.0F, 4.0F, 4.0F, 0.0F, false);

		panel_front_top = new ModelRenderer(this);
		panel_front_top.setRotationPoint(0.0F, 0.0F, 0.0F);
		panel_front.addChild(panel_front_top);
		panel_front_top.setTextureOffset(274, 73).addBox(-24.0F, -180.2222F, -49.6667F, 48.0F, 24.0F, 4.0F, 0.0F, false);
		panel_front_top.setTextureOffset(464, 38).addBox(5.0F, -177.2222F, -51.0667F, 14.0F, 14.0F, 6.0F, 0.0F, false);
		panel_front_top.setTextureOffset(464, 38).addBox(-19.0F, -177.2222F, -51.0667F, 14.0F, 14.0F, 6.0F, 0.0F, false);

		panel_side_1 = new ModelRenderer(this);
		panel_side_1.setRotationPoint(0.0F, -3.7778F, 1.6667F);
		box.addChild(panel_side_1);
		setRotationAngle(panel_side_1, 0.0F, -1.0472F, 0.0F);
		

		side_1 = new ModelRenderer(this);
		side_1.setRotationPoint(0.0F, 3.7778F, -1.6667F);
		panel_side_1.addChild(side_1);
		side_1.setTextureOffset(146, 393).addBox(-28.0F, -36.0F, -49.0F, 56.0F, 4.0F, 4.0F, 0.0F, false);
		side_1.setTextureOffset(139, 249).addBox(-28.0F, -184.0F, -49.0F, 4.0F, 148.0F, 4.0F, 0.0F, false);
		side_1.setTextureOffset(144, 246).addBox(24.0F, -184.0F, -49.0F, 4.0F, 148.0F, 4.0F, 0.0F, false);
		side_1.setTextureOffset(235, 246).addBox(-28.0F, -192.0F, -49.0F, 56.0F, 8.0F, 4.0F, 0.0F, false);
		side_1.setTextureOffset(171, 352).addBox(-24.0F, -136.0F, -49.0F, 48.0F, 12.0F, 4.0F, 0.0F, false);
		side_1.setTextureOffset(278, 374).addBox(-24.0F, -80.0F, -49.0F, 48.0F, 20.0F, 4.0F, 0.0F, false);
		side_1.setTextureOffset(118, 267).addBox(-28.0F, -195.2F, -51.0F, 56.0F, 4.0F, 4.0F, 0.0F, false);
		side_1.setTextureOffset(452, 18).addBox(-28.0F, -132.0F, -50.6F, 56.0F, 4.0F, 6.0F, 0.0F, false);
		side_1.setTextureOffset(448, 13).addBox(-28.0F, -76.0F, -50.6F, 56.0F, 4.0F, 6.0F, 0.0F, false);
		side_1.setTextureOffset(448, 84).addBox(-28.0F, -66.0F, -50.6F, 56.0F, 4.0F, 6.0F, 0.0F, false);
		side_1.setTextureOffset(459, 6).addBox(-28.0F, -33.0F, -50.6F, 56.0F, 4.0F, 6.0F, 0.0F, false);
		side_1.setTextureOffset(441, 93).addBox(-28.0F, -16.0F, -50.6F, 56.0F, 4.0F, 6.0F, 0.0F, false);
		side_1.setTextureOffset(288, 422).addBox(-28.0F, -32.0F, -49.0F, 56.0F, 16.0F, 4.0F, 0.0F, false);
		side_1.setTextureOffset(140, 392).addBox(-28.0F, -12.0F, -49.0F, 56.0F, 8.0F, 4.0F, 0.0F, false);

		top_panel_1 = new ModelRenderer(this);
		top_panel_1.setRotationPoint(0.0F, 0.0F, 0.0F);
		panel_side_1.addChild(top_panel_1);
		top_panel_1.setTextureOffset(269, 64).addBox(-24.0F, -180.2222F, -49.6667F, 48.0F, 48.0F, 4.0F, 0.0F, false);

		cage = new ModelRenderer(this);
		cage.setRotationPoint(0.0F, 3.7778F, -1.6667F);
		top_panel_1.addChild(cage);
		cage.setTextureOffset(480, 38).addBox(-9.0F, -177.0F, -58.0F, 4.0F, 12.0F, 17.0F, 0.0F, false);
		cage.setTextureOffset(480, 38).addBox(5.0F, -177.0F, -58.0F, 4.0F, 8.0F, 17.0F, 0.0F, false);
		cage.setTextureOffset(480, 38).addBox(-17.0F, -169.0F, -58.6F, 11.0F, 4.0F, 18.0F, 0.0F, false);
		cage.setTextureOffset(480, 38).addBox(-17.0F, -155.0F, -58.6F, 11.0F, 4.0F, 18.0F, 0.0F, false);
		cage.setTextureOffset(480, 38).addBox(5.0F, -151.0F, -58.0F, 4.0F, 8.0F, 17.0F, 0.0F, false);
		cage.setTextureOffset(480, 38).addBox(-9.0F, -151.0F, -58.0F, 4.0F, 8.0F, 17.0F, 0.0F, false);
		cage.setTextureOffset(480, 38).addBox(6.0F, -155.0F, -58.6F, 11.0F, 4.0F, 18.0F, 0.0F, false);
		cage.setTextureOffset(480, 38).addBox(6.0F, -169.0F, -58.6F, 11.0F, 4.0F, 18.0F, 0.0F, false);
		cage.setTextureOffset(480, 38).addBox(-6.0F, -169.0F, -59.6F, 12.0F, 4.0F, 19.0F, 0.0F, false);
		cage.setTextureOffset(480, 38).addBox(-6.0F, -155.0F, -59.6F, 12.0F, 4.0F, 19.0F, 0.0F, false);
		cage.setTextureOffset(480, 38).addBox(-9.0F, -165.0F, -58.0F, 4.0F, 12.0F, 17.0F, 0.0F, false);
		cage.setTextureOffset(480, 38).addBox(5.0F, -165.0F, -58.0F, 4.0F, 12.0F, 17.0F, 0.0F, false);
		cage.setTextureOffset(480, 38).addBox(-16.0F, -176.0F, -52.0F, 32.0F, 32.0F, 10.0F, 0.0F, false);

		middle_panel_1 = new ModelRenderer(this);
		middle_panel_1.setRotationPoint(0.0F, 0.0F, 0.0F);
		panel_side_1.addChild(middle_panel_1);
		middle_panel_1.setTextureOffset(262, 137).addBox(-24.0F, -120.2222F, -49.6667F, 48.0F, 44.0F, 4.0F, 0.0F, false);
		middle_panel_1.setTextureOffset(452, 38).addBox(-18.0F, -114.2222F, -51.6667F, 36.0F, 32.0F, 4.0F, 0.0F, false);
		middle_panel_1.setTextureOffset(452, 38).addBox(13.0F, -113.2222F, -52.6667F, 4.0F, 4.0F, 4.0F, 0.0F, false);
		middle_panel_1.setTextureOffset(452, 38).addBox(-17.0F, -113.2222F, -52.6667F, 4.0F, 4.0F, 4.0F, 0.0F, false);
		middle_panel_1.setTextureOffset(452, 38).addBox(-17.0F, -87.2222F, -52.6667F, 4.0F, 4.0F, 4.0F, 0.0F, false);
		middle_panel_1.setTextureOffset(452, 38).addBox(13.0F, -87.2222F, -52.6667F, 4.0F, 4.0F, 4.0F, 0.0F, false);

		lower_panel_1 = new ModelRenderer(this);
		lower_panel_1.setRotationPoint(0.0F, 0.0F, 0.0F);
		panel_side_1.addChild(lower_panel_1);
		

		componets = new ModelRenderer(this);
		componets.setRotationPoint(0.0F, 0.0F, 0.0F);
		lower_panel_1.addChild(componets);
		componets.setTextureOffset(491, 48).addBox(-21.2F, -53.2222F, -46.0667F, 2.0F, 2.0F, 2.0F, 0.0F, false);
		componets.setTextureOffset(491, 48).addBox(-15.0F, -53.2222F, -46.0667F, 2.0F, 2.0F, 2.0F, 0.0F, false);
		componets.setTextureOffset(509, 276).addBox(-8.0F, -53.2222F, -48.6667F, 12.0F, 8.0F, 4.0F, 0.0F, false);
		componets.setTextureOffset(491, 48).addBox(-4.0F, -45.2222F, -46.6667F, 2.0F, 6.0F, 3.0F, 0.0F, false);
		componets.setTextureOffset(475, 288).addBox(-14.0F, -42.2222F, -47.2667F, 8.0F, 8.0F, 4.0F, 0.0F, false);
		componets.setTextureOffset(491, 48).addBox(-8.0F, -39.2222F, -46.6667F, 6.0F, 2.0F, 3.0F, 0.0F, false);
		componets.setTextureOffset(534, 310).addBox(2.0F, -42.2222F, -47.2667F, 8.0F, 8.0F, 4.0F, 0.0F, false);
		componets.setTextureOffset(694, 180).addBox(11.2F, -54.2222F, -46.0667F, 8.0F, 20.0F, 2.0F, 0.0F, false);
		componets.setTextureOffset(491, 48).addBox(-18.2F, -53.2222F, -46.0667F, 2.0F, 2.0F, 2.0F, 0.0F, false);
		componets.setTextureOffset(491, 48).addBox(-15.0F, -50.2222F, -46.0667F, 2.0F, 2.0F, 2.0F, 0.0F, false);
		componets.setTextureOffset(491, 48).addBox(-18.2F, -50.2222F, -46.0667F, 2.0F, 2.0F, 2.0F, 0.0F, false);
		componets.setTextureOffset(491, 48).addBox(-21.2F, -50.2222F, -46.0667F, 2.0F, 2.0F, 2.0F, 0.0F, false);
		componets.setTextureOffset(491, 48).addBox(-21.2F, -47.2222F, -46.0667F, 2.0F, 2.0F, 2.0F, 0.0F, false);
		componets.setTextureOffset(491, 48).addBox(-18.2F, -47.2222F, -46.0667F, 2.0F, 2.0F, 2.0F, 0.0F, false);
		componets.setTextureOffset(491, 48).addBox(-15.0F, -47.2222F, -46.0667F, 2.0F, 2.0F, 2.0F, 0.0F, false);
		componets.setTextureOffset(491, 48).addBox(-2.0F, -45.2222F, -46.6667F, 2.0F, 6.0F, 3.0F, 0.0F, false);
		componets.setTextureOffset(491, 48).addBox(-2.0F, -39.2222F, -46.6667F, 6.0F, 2.0F, 3.0F, 0.0F, false);
		componets.setTextureOffset(704, 210).addBox(-6.0F, -37.2222F, -46.6667F, 10.0F, 2.0F, 3.0F, 0.0F, false);

		panelbox = new ModelRenderer(this);
		panelbox.setRotationPoint(0.0F, 0.0F, 0.0F);
		lower_panel_1.addChild(panelbox);
		panelbox.setTextureOffset(665, 48).addBox(-24.0F, -57.2222F, -45.2667F, 48.0F, 27.0F, 0.0F, 0.0F, false);
		panelbox.setTextureOffset(658, 52).addBox(-23.0F, -57.2222F, -49.2667F, 46.0F, 2.0F, 4.0F, 0.0F, false);
		panelbox.setTextureOffset(667, 59).addBox(-23.0F, -33.2222F, -49.2667F, 46.0F, 2.0F, 4.0F, 0.0F, false);

		panelbox2 = new ModelRenderer(this);
		panelbox2.setRotationPoint(0.0F, 0.0F, 0.0F);
		lower_panel_1.addChild(panelbox2);
		panelbox2.setTextureOffset(669, 45).addBox(-25.0F, -57.2222F, -49.2667F, 2.0F, 26.0F, 4.0F, 0.0F, false);
		panelbox2.setTextureOffset(715, 59).addBox(23.0F, -57.2222F, -49.2667F, 2.0F, 26.0F, 4.0F, 0.0F, false);

		floorboards_1 = new ModelRenderer(this);
		floorboards_1.setRotationPoint(0.0F, 0.0F, 0.0F);
		panel_side_1.addChild(floorboards_1);
		floorboards_1.setTextureOffset(242, 185).addBox(-32.4F, -0.2222F, -58.6667F, 64.0F, 4.0F, 4.0F, 0.0F, false);
		floorboards_1.setTextureOffset(242, 185).addBox(-26.0F, -0.2222F, -46.6667F, 52.0F, 4.0F, 4.0F, 0.0F, false);
		floorboards_1.setTextureOffset(242, 185).addBox(-24.0F, -0.2222F, -42.6667F, 48.0F, 4.0F, 4.0F, 0.0F, false);
		floorboards_1.setTextureOffset(242, 185).addBox(-22.0F, -0.2222F, -38.6667F, 44.0F, 4.0F, 4.0F, 0.0F, false);
		floorboards_1.setTextureOffset(242, 185).addBox(-18.0F, -0.2222F, -34.6667F, 36.0F, 4.0F, 4.0F, 0.0F, false);
		floorboards_1.setTextureOffset(242, 185).addBox(-16.0F, -0.2222F, -30.6667F, 32.0F, 4.0F, 4.0F, 0.0F, false);
		floorboards_1.setTextureOffset(242, 185).addBox(-14.0F, -0.2222F, -26.6667F, 28.0F, 4.0F, 4.0F, 0.0F, false);
		floorboards_1.setTextureOffset(242, 185).addBox(-12.0F, -0.2222F, -22.6667F, 24.0F, 4.0F, 4.0F, 0.0F, false);
		floorboards_1.setTextureOffset(242, 185).addBox(-10.0F, -0.2222F, -18.6667F, 20.0F, 4.0F, 4.0F, 0.0F, false);
		floorboards_1.setTextureOffset(242, 185).addBox(-8.0F, -0.2222F, -14.6667F, 16.0F, 4.0F, 4.0F, 0.0F, false);
		floorboards_1.setTextureOffset(242, 185).addBox(-6.0F, -0.2222F, -10.6667F, 12.0F, 4.0F, 4.0F, 0.0F, false);
		floorboards_1.setTextureOffset(242, 185).addBox(-4.0F, -0.2222F, -6.6667F, 8.0F, 4.0F, 4.0F, 0.0F, false);
		floorboards_1.setTextureOffset(242, 185).addBox(-30.8F, -0.2222F, -54.6667F, 60.0F, 4.0F, 4.0F, 0.0F, false);
		floorboards_1.setTextureOffset(242, 185).addBox(-28.0F, -0.2222F, -50.6667F, 56.0F, 4.0F, 4.0F, 0.0F, false);

		roof_side_1 = new ModelRenderer(this);
		roof_side_1.setRotationPoint(0.0F, -188.0F, -48.0F);
		panel_side_1.addChild(roof_side_1);
		

		center_peak_1 = new ModelRenderer(this);
		center_peak_1.setRotationPoint(0.0F, 0.0F, 16.0F);
		roof_side_1.addChild(center_peak_1);
		setRotationAngle(center_peak_1, 0.1047F, 0.0F, 0.0F);
		center_peak_1.setTextureOffset(196, 306).addBox(-18.0F, -13.8222F, -1.4667F, 37.0F, 2.0F, 4.0F, 0.0F, false);
		center_peak_1.setTextureOffset(162, 306).addBox(-6.0F, -18.2222F, 22.5333F, 12.0F, 8.0F, 8.0F, 0.0F, false);
		center_peak_1.setTextureOffset(162, 306).addBox(-6.0F, -18.2222F, 30.5333F, 12.0F, 4.0F, 4.0F, 0.0F, false);
		center_peak_1.setTextureOffset(162, 306).addBox(-12.0F, -12.2222F, -1.0667F, 4.0F, 8.0F, 8.0F, 0.0F, false);
		center_peak_1.setTextureOffset(162, 306).addBox(-2.0F, -12.2222F, -1.0667F, 4.0F, 8.0F, 8.0F, 0.0F, false);
		center_peak_1.setTextureOffset(162, 306).addBox(8.0F, -12.2222F, -1.0667F, 4.0F, 8.0F, 8.0F, 0.0F, false);
		center_peak_1.setTextureOffset(162, 306).addBox(-16.0F, -13.8222F, 2.5333F, 32.0F, 2.0F, 4.0F, 0.0F, false);
		center_peak_1.setTextureOffset(114, 313).addBox(-14.0F, -13.8222F, 6.5333F, 28.0F, 2.0F, 4.0F, 0.0F, false);
		center_peak_1.setTextureOffset(162, 306).addBox(-12.0F, -13.8222F, 10.5333F, 24.0F, 2.0F, 4.0F, 0.0F, false);
		center_peak_1.setTextureOffset(185, 288).addBox(-10.0F, -13.8222F, 14.5333F, 20.0F, 2.0F, 4.0F, 0.0F, false);
		center_peak_1.setTextureOffset(162, 306).addBox(-8.0F, -13.8222F, 18.5333F, 16.0F, 2.0F, 4.0F, 0.0F, false);

		wroughtiron_front_1 = new ModelRenderer(this);
		wroughtiron_front_1.setRotationPoint(0.0F, 191.7778F, 46.3333F);
		roof_side_1.addChild(wroughtiron_front_1);
		wroughtiron_front_1.setTextureOffset(6, 420).addBox(-32.0F, -220.0F, -54.0F, 64.0F, 24.0F, 0.0F, 0.0F, false);

		outer_peak_1 = new ModelRenderer(this);
		outer_peak_1.setRotationPoint(0.0F, 0.0F, 0.0F);
		roof_side_1.addChild(outer_peak_1);
		setRotationAngle(outer_peak_1, 0.0873F, 0.0F, 0.0F);
		outer_peak_1.setTextureOffset(139, 320).addBox(-32.0F, -5.0222F, -10.6667F, 64.0F, 2.0F, 7.0F, 0.0F, false);
		outer_peak_1.setTextureOffset(139, 320).addBox(-29.8F, -3.0222F, -7.2667F, 60.0F, 2.0F, 8.0F, 0.0F, false);
		outer_peak_1.setTextureOffset(139, 320).addBox(-25.6F, -5.0222F, 0.3333F, 52.0F, 4.0F, 4.0F, 0.0F, false);
		outer_peak_1.setTextureOffset(139, 320).addBox(-23.6F, -5.0222F, 4.3333F, 48.0F, 4.0F, 4.0F, 0.0F, false);
		outer_peak_1.setTextureOffset(139, 320).addBox(-21.6F, -5.0222F, 8.3333F, 44.0F, 4.0F, 4.0F, 0.0F, false);
		outer_peak_1.setTextureOffset(139, 320).addBox(-19.6F, -5.0222F, 12.3333F, 40.0F, 4.0F, 4.0F, 0.0F, false);
		outer_peak_1.setTextureOffset(139, 320).addBox(-4.0F, -24.0222F, 42.3333F, 8.0F, 20.0F, 4.0F, 0.0F, false);
		outer_peak_1.setTextureOffset(139, 320).addBox(-27.6F, -5.0222F, -3.6667F, 56.0F, 4.0F, 4.0F, 0.0F, false);

		panel_side_2 = new ModelRenderer(this);
		panel_side_2.setRotationPoint(0.0F, -3.7778F, 1.6667F);
		box.addChild(panel_side_2);
		setRotationAngle(panel_side_2, 0.0F, -2.0944F, 0.0F);
		

		side_2 = new ModelRenderer(this);
		side_2.setRotationPoint(0.0F, 3.7778F, -1.6667F);
		panel_side_2.addChild(side_2);
		side_2.setTextureOffset(146, 393).addBox(-28.0F, -36.0F, -49.0F, 56.0F, 4.0F, 4.0F, 0.0F, false);
		side_2.setTextureOffset(139, 249).addBox(-28.0F, -184.0F, -49.0F, 4.0F, 148.0F, 4.0F, 0.0F, false);
		side_2.setTextureOffset(144, 246).addBox(24.0F, -184.0F, -49.0F, 4.0F, 148.0F, 4.0F, 0.0F, false);
		side_2.setTextureOffset(235, 246).addBox(-28.0F, -192.0F, -49.0F, 56.0F, 8.0F, 4.0F, 0.0F, false);
		side_2.setTextureOffset(171, 352).addBox(-24.0F, -136.0F, -49.0F, 48.0F, 12.0F, 4.0F, 0.0F, false);
		side_2.setTextureOffset(278, 374).addBox(-24.0F, -80.0F, -49.0F, 48.0F, 20.0F, 4.0F, 0.0F, false);
		side_2.setTextureOffset(118, 267).addBox(-28.0F, -195.2F, -51.0F, 56.0F, 4.0F, 4.0F, 0.0F, false);
		side_2.setTextureOffset(452, 18).addBox(-28.0F, -132.0F, -50.6F, 56.0F, 4.0F, 6.0F, 0.0F, false);
		side_2.setTextureOffset(448, 13).addBox(-28.0F, -76.0F, -50.6F, 56.0F, 4.0F, 6.0F, 0.0F, false);
		side_2.setTextureOffset(448, 84).addBox(-28.0F, -66.0F, -50.6F, 56.0F, 4.0F, 6.0F, 0.0F, false);
		side_2.setTextureOffset(459, 6).addBox(-28.0F, -33.0F, -50.6F, 56.0F, 4.0F, 6.0F, 0.0F, false);
		side_2.setTextureOffset(441, 93).addBox(-28.0F, -16.0F, -50.6F, 56.0F, 4.0F, 6.0F, 0.0F, false);
		side_2.setTextureOffset(288, 422).addBox(-28.0F, -32.0F, -49.0F, 56.0F, 16.0F, 4.0F, 0.0F, false);
		side_2.setTextureOffset(140, 392).addBox(-28.0F, -12.0F, -49.0F, 56.0F, 8.0F, 4.0F, 0.0F, false);

		top_panel_2 = new ModelRenderer(this);
		top_panel_2.setRotationPoint(0.0F, 0.0F, 0.0F);
		panel_side_2.addChild(top_panel_2);
		top_panel_2.setTextureOffset(260, 82).addBox(-24.0F, -180.2222F, -49.6667F, 48.0F, 48.0F, 4.0F, 0.0F, false);

		cage2 = new ModelRenderer(this);
		cage2.setRotationPoint(0.0F, 3.7778F, -1.6667F);
		top_panel_2.addChild(cage2);
		cage2.setTextureOffset(480, 38).addBox(-9.0F, -177.0F, -58.0F, 4.0F, 12.0F, 17.0F, 0.0F, false);
		cage2.setTextureOffset(480, 38).addBox(5.0F, -177.0F, -58.0F, 4.0F, 8.0F, 17.0F, 0.0F, false);
		cage2.setTextureOffset(480, 38).addBox(-17.0F, -169.0F, -58.6F, 11.0F, 4.0F, 18.0F, 0.0F, false);
		cage2.setTextureOffset(480, 38).addBox(-17.0F, -155.0F, -58.6F, 11.0F, 4.0F, 18.0F, 0.0F, false);
		cage2.setTextureOffset(480, 38).addBox(5.0F, -151.0F, -58.0F, 4.0F, 8.0F, 17.0F, 0.0F, false);
		cage2.setTextureOffset(480, 38).addBox(-9.0F, -151.0F, -58.0F, 4.0F, 8.0F, 17.0F, 0.0F, false);
		cage2.setTextureOffset(480, 38).addBox(6.0F, -155.0F, -58.6F, 11.0F, 4.0F, 18.0F, 0.0F, false);
		cage2.setTextureOffset(480, 38).addBox(6.0F, -169.0F, -58.6F, 11.0F, 4.0F, 18.0F, 0.0F, false);
		cage2.setTextureOffset(480, 38).addBox(-6.0F, -169.0F, -59.6F, 12.0F, 4.0F, 19.0F, 0.0F, false);
		cage2.setTextureOffset(480, 38).addBox(-6.0F, -155.0F, -59.6F, 12.0F, 4.0F, 19.0F, 0.0F, false);
		cage2.setTextureOffset(480, 38).addBox(-9.0F, -165.0F, -58.0F, 4.0F, 12.0F, 17.0F, 0.0F, false);
		cage2.setTextureOffset(480, 38).addBox(5.0F, -165.0F, -58.0F, 4.0F, 12.0F, 17.0F, 0.0F, false);
		cage2.setTextureOffset(480, 38).addBox(-16.0F, -176.0F, -52.0F, 32.0F, 32.0F, 10.0F, 0.0F, false);

		middle_panel_2 = new ModelRenderer(this);
		middle_panel_2.setRotationPoint(0.0F, 0.0F, 0.0F);
		panel_side_2.addChild(middle_panel_2);
		middle_panel_2.setTextureOffset(276, 137).addBox(-24.0F, -120.2222F, -49.6667F, 48.0F, 44.0F, 4.0F, 0.0F, false);
		middle_panel_2.setTextureOffset(452, 38).addBox(-18.0F, -114.2222F, -51.6667F, 36.0F, 32.0F, 4.0F, 0.0F, false);
		middle_panel_2.setTextureOffset(452, 38).addBox(13.0F, -113.2222F, -52.6667F, 4.0F, 4.0F, 4.0F, 0.0F, false);
		middle_panel_2.setTextureOffset(452, 38).addBox(-17.0F, -113.2222F, -52.6667F, 4.0F, 4.0F, 4.0F, 0.0F, false);
		middle_panel_2.setTextureOffset(452, 38).addBox(-17.0F, -87.2222F, -52.6667F, 4.0F, 4.0F, 4.0F, 0.0F, false);
		middle_panel_2.setTextureOffset(452, 38).addBox(13.0F, -87.2222F, -52.6667F, 4.0F, 4.0F, 4.0F, 0.0F, false);

		lower_panel_2 = new ModelRenderer(this);
		lower_panel_2.setRotationPoint(0.0F, 0.0F, 0.0F);
		panel_side_2.addChild(lower_panel_2);
		

		componets2 = new ModelRenderer(this);
		componets2.setRotationPoint(0.0F, 0.0F, 0.0F);
		lower_panel_2.addChild(componets2);
		componets2.setTextureOffset(571, 157).addBox(-21.2F, -53.2222F, -46.0667F, 2.0F, 2.0F, 2.0F, 0.0F, false);
		componets2.setTextureOffset(582, 137).addBox(-15.0F, -53.2222F, -46.0667F, 2.0F, 2.0F, 2.0F, 0.0F, false);
		componets2.setTextureOffset(576, 153).addBox(-21.0F, -41.2222F, -46.0667F, 2.0F, 2.0F, 2.0F, 0.0F, false);
		componets2.setTextureOffset(573, 155).addBox(-15.0F, -50.2222F, -46.0667F, 2.0F, 2.0F, 2.0F, 0.0F, false);
		componets2.setTextureOffset(564, 148).addBox(-21.0F, -38.2222F, -46.0667F, 2.0F, 2.0F, 2.0F, 0.0F, false);
		componets2.setTextureOffset(564, 146).addBox(-21.2F, -50.2222F, -46.0667F, 2.0F, 2.0F, 2.0F, 0.0F, false);
		componets2.setTextureOffset(576, 157).addBox(-21.2F, -47.2222F, -46.0667F, 2.0F, 2.0F, 2.0F, 0.0F, false);
		componets2.setTextureOffset(585, 150).addBox(-21.0F, -35.2222F, -46.0667F, 2.0F, 2.0F, 2.0F, 0.0F, false);
		componets2.setTextureOffset(571, 141).addBox(-15.0F, -47.2222F, -46.0667F, 2.0F, 2.0F, 2.0F, 0.0F, false);
		componets2.setTextureOffset(505, 52).addBox(-3.8F, -50.0222F, -46.6667F, 20.0F, 2.0F, 3.0F, 0.0F, false);
		componets2.setTextureOffset(505, 52).addBox(3.0F, -44.2222F, -46.6667F, 20.0F, 2.0F, 3.0F, 0.0F, false);
		componets2.setTextureOffset(678, 180).addBox(16.2F, -51.0222F, -46.6667F, 4.0F, 4.0F, 3.0F, 0.0F, false);
		componets2.setTextureOffset(505, 52).addBox(-5.8F, -50.0222F, -46.6667F, 2.0F, 8.0F, 3.0F, 0.0F, false);
		componets2.setTextureOffset(683, 155).addBox(-0.8F, -45.0222F, -46.6667F, 4.0F, 4.0F, 3.0F, 0.0F, false);
		componets2.setTextureOffset(505, 52).addBox(-25.8F, -44.0222F, -46.6667F, 20.0F, 2.0F, 3.0F, 0.0F, false);
		componets2.setTextureOffset(505, 52).addBox(-13.8F, -42.0222F, -46.6667F, 2.0F, 6.0F, 3.0F, 0.0F, false);
		componets2.setTextureOffset(505, 52).addBox(-13.8F, -38.0222F, -46.6667F, 26.0F, 2.0F, 3.0F, 0.0F, false);
		componets2.setTextureOffset(505, 52).addBox(10.2F, -36.0222F, -46.6667F, 2.0F, 4.0F, 3.0F, 0.0F, false);

		panelbox3 = new ModelRenderer(this);
		panelbox3.setRotationPoint(0.0F, 0.0F, 0.0F);
		lower_panel_2.addChild(panelbox3);
		panelbox3.setTextureOffset(674, 41).addBox(-24.0F, -57.2222F, -45.2667F, 48.0F, 27.0F, 0.0F, 0.0F, false);
		panelbox3.setTextureOffset(692, 43).addBox(-23.0F, -57.2222F, -49.2667F, 46.0F, 2.0F, 4.0F, 0.0F, false);
		panelbox3.setTextureOffset(660, 57).addBox(-23.0F, -33.2222F, -49.2667F, 46.0F, 2.0F, 4.0F, 0.0F, false);

		panelbox4 = new ModelRenderer(this);
		panelbox4.setRotationPoint(0.0F, 0.0F, 0.0F);
		lower_panel_2.addChild(panelbox4);
		panelbox4.setTextureOffset(683, 57).addBox(-25.0F, -57.2222F, -49.2667F, 2.0F, 26.0F, 4.0F, 0.0F, false);
		panelbox4.setTextureOffset(706, 43).addBox(23.0F, -57.2222F, -49.2667F, 2.0F, 26.0F, 4.0F, 0.0F, false);

		floorboards_2 = new ModelRenderer(this);
		floorboards_2.setRotationPoint(0.0F, 0.0F, 0.0F);
		panel_side_2.addChild(floorboards_2);
		floorboards_2.setTextureOffset(240, 178).addBox(-32.4F, -0.2222F, -58.6667F, 64.0F, 4.0F, 4.0F, 0.0F, false);
		floorboards_2.setTextureOffset(240, 178).addBox(-26.0F, -0.2222F, -46.6667F, 52.0F, 4.0F, 4.0F, 0.0F, false);
		floorboards_2.setTextureOffset(240, 178).addBox(-24.0F, -0.2222F, -42.6667F, 48.0F, 4.0F, 4.0F, 0.0F, false);
		floorboards_2.setTextureOffset(240, 178).addBox(-22.0F, -0.2222F, -38.6667F, 44.0F, 4.0F, 4.0F, 0.0F, false);
		floorboards_2.setTextureOffset(240, 178).addBox(-18.0F, -0.2222F, -34.6667F, 36.0F, 4.0F, 4.0F, 0.0F, false);
		floorboards_2.setTextureOffset(240, 178).addBox(-16.0F, -0.2222F, -30.6667F, 32.0F, 4.0F, 4.0F, 0.0F, false);
		floorboards_2.setTextureOffset(240, 178).addBox(-14.0F, -0.2222F, -26.6667F, 28.0F, 4.0F, 4.0F, 0.0F, false);
		floorboards_2.setTextureOffset(240, 178).addBox(-12.0F, -0.2222F, -22.6667F, 24.0F, 4.0F, 4.0F, 0.0F, false);
		floorboards_2.setTextureOffset(240, 178).addBox(-10.0F, -0.2222F, -18.6667F, 20.0F, 4.0F, 4.0F, 0.0F, false);
		floorboards_2.setTextureOffset(240, 178).addBox(-8.0F, -0.2222F, -14.6667F, 16.0F, 4.0F, 4.0F, 0.0F, false);
		floorboards_2.setTextureOffset(240, 178).addBox(-6.0F, -0.2222F, -10.6667F, 12.0F, 4.0F, 4.0F, 0.0F, false);
		floorboards_2.setTextureOffset(240, 178).addBox(-4.0F, -0.2222F, -6.6667F, 8.0F, 4.0F, 4.0F, 0.0F, false);
		floorboards_2.setTextureOffset(240, 178).addBox(-30.8F, -0.2222F, -54.6667F, 60.0F, 4.0F, 4.0F, 0.0F, false);
		floorboards_2.setTextureOffset(240, 178).addBox(-28.0F, -0.2222F, -50.6667F, 56.0F, 4.0F, 4.0F, 0.0F, false);

		roof_side_2 = new ModelRenderer(this);
		roof_side_2.setRotationPoint(0.0F, -188.0F, -48.0F);
		panel_side_2.addChild(roof_side_2);
		

		center_peak_2 = new ModelRenderer(this);
		center_peak_2.setRotationPoint(0.0F, 0.0F, 16.0F);
		roof_side_2.addChild(center_peak_2);
		setRotationAngle(center_peak_2, 0.1047F, 0.0F, 0.0F);
		center_peak_2.setTextureOffset(196, 306).addBox(-18.0F, -13.8222F, -1.4667F, 37.0F, 2.0F, 4.0F, 0.0F, false);
		center_peak_2.setTextureOffset(162, 306).addBox(-6.0F, -18.2222F, 22.5333F, 12.0F, 8.0F, 8.0F, 0.0F, false);
		center_peak_2.setTextureOffset(162, 306).addBox(-6.0F, -18.2222F, 30.5333F, 12.0F, 4.0F, 4.0F, 0.0F, false);
		center_peak_2.setTextureOffset(162, 306).addBox(-12.0F, -12.2222F, -1.0667F, 4.0F, 8.0F, 8.0F, 0.0F, false);
		center_peak_2.setTextureOffset(162, 306).addBox(-2.0F, -12.2222F, -1.0667F, 4.0F, 8.0F, 8.0F, 0.0F, false);
		center_peak_2.setTextureOffset(162, 306).addBox(8.0F, -12.2222F, -1.0667F, 4.0F, 8.0F, 8.0F, 0.0F, false);
		center_peak_2.setTextureOffset(162, 306).addBox(-16.0F, -13.8222F, 2.5333F, 32.0F, 2.0F, 4.0F, 0.0F, false);
		center_peak_2.setTextureOffset(114, 313).addBox(-14.0F, -13.8222F, 6.5333F, 28.0F, 2.0F, 4.0F, 0.0F, false);
		center_peak_2.setTextureOffset(162, 306).addBox(-12.0F, -13.8222F, 10.5333F, 24.0F, 2.0F, 4.0F, 0.0F, false);
		center_peak_2.setTextureOffset(185, 288).addBox(-10.0F, -13.8222F, 14.5333F, 20.0F, 2.0F, 4.0F, 0.0F, false);
		center_peak_2.setTextureOffset(162, 306).addBox(-8.0F, -13.8222F, 18.5333F, 16.0F, 2.0F, 4.0F, 0.0F, false);

		wroughtiron_front_2 = new ModelRenderer(this);
		wroughtiron_front_2.setRotationPoint(0.0F, 191.7778F, 46.3333F);
		roof_side_2.addChild(wroughtiron_front_2);
		wroughtiron_front_2.setTextureOffset(6, 420).addBox(-32.0F, -220.0F, -54.0F, 64.0F, 24.0F, 0.0F, 0.0F, false);

		outer_peak_2 = new ModelRenderer(this);
		outer_peak_2.setRotationPoint(0.0F, 0.0F, 0.0F);
		roof_side_2.addChild(outer_peak_2);
		setRotationAngle(outer_peak_2, 0.0873F, 0.0F, 0.0F);
		outer_peak_2.setTextureOffset(139, 320).addBox(-32.0F, -5.0222F, -10.6667F, 64.0F, 2.0F, 7.0F, 0.0F, false);
		outer_peak_2.setTextureOffset(139, 320).addBox(-29.8F, -3.0222F, -7.2667F, 60.0F, 2.0F, 8.0F, 0.0F, false);
		outer_peak_2.setTextureOffset(139, 320).addBox(-25.6F, -5.0222F, 0.3333F, 52.0F, 4.0F, 4.0F, 0.0F, false);
		outer_peak_2.setTextureOffset(139, 320).addBox(-23.6F, -5.0222F, 4.3333F, 48.0F, 4.0F, 4.0F, 0.0F, false);
		outer_peak_2.setTextureOffset(139, 320).addBox(-21.6F, -5.0222F, 8.3333F, 44.0F, 4.0F, 4.0F, 0.0F, false);
		outer_peak_2.setTextureOffset(139, 320).addBox(-19.6F, -5.0222F, 12.3333F, 40.0F, 4.0F, 4.0F, 0.0F, false);
		outer_peak_2.setTextureOffset(139, 320).addBox(-4.0F, -24.0222F, 42.3333F, 8.0F, 20.0F, 4.0F, 0.0F, false);
		outer_peak_2.setTextureOffset(139, 320).addBox(-27.6F, -5.0222F, -3.6667F, 56.0F, 4.0F, 4.0F, 0.0F, false);

		panel_side_3 = new ModelRenderer(this);
		panel_side_3.setRotationPoint(0.0F, -3.7778F, 1.6667F);
		box.addChild(panel_side_3);
		setRotationAngle(panel_side_3, 0.0F, 3.1416F, 0.0F);
		

		roof_side_3 = new ModelRenderer(this);
		roof_side_3.setRotationPoint(0.0F, -188.0F, -48.0F);
		panel_side_3.addChild(roof_side_3);
		

		center_peak_3 = new ModelRenderer(this);
		center_peak_3.setRotationPoint(0.0F, 0.0F, 16.0F);
		roof_side_3.addChild(center_peak_3);
		setRotationAngle(center_peak_3, 0.1047F, 0.0F, 0.0F);
		center_peak_3.setTextureOffset(196, 306).addBox(-18.0F, -13.8222F, -1.4667F, 37.0F, 2.0F, 4.0F, 0.0F, false);
		center_peak_3.setTextureOffset(162, 306).addBox(-6.0F, -18.2222F, 22.5333F, 12.0F, 8.0F, 8.0F, 0.0F, false);
		center_peak_3.setTextureOffset(162, 306).addBox(-6.0F, -18.2222F, 30.5333F, 12.0F, 4.0F, 4.0F, 0.0F, false);
		center_peak_3.setTextureOffset(162, 306).addBox(-12.0F, -12.2222F, -1.0667F, 4.0F, 8.0F, 8.0F, 0.0F, false);
		center_peak_3.setTextureOffset(162, 306).addBox(-2.0F, -12.2222F, -1.0667F, 4.0F, 8.0F, 8.0F, 0.0F, false);
		center_peak_3.setTextureOffset(162, 306).addBox(8.0F, -12.2222F, -1.0667F, 4.0F, 8.0F, 8.0F, 0.0F, false);
		center_peak_3.setTextureOffset(162, 306).addBox(-16.0F, -13.8222F, 2.5333F, 32.0F, 2.0F, 4.0F, 0.0F, false);
		center_peak_3.setTextureOffset(114, 313).addBox(-14.0F, -13.8222F, 6.5333F, 28.0F, 2.0F, 4.0F, 0.0F, false);
		center_peak_3.setTextureOffset(162, 306).addBox(-12.0F, -13.8222F, 10.5333F, 24.0F, 2.0F, 4.0F, 0.0F, false);
		center_peak_3.setTextureOffset(185, 288).addBox(-10.0F, -13.8222F, 14.5333F, 20.0F, 2.0F, 4.0F, 0.0F, false);
		center_peak_3.setTextureOffset(162, 306).addBox(-8.0F, -13.8222F, 18.5333F, 16.0F, 2.0F, 4.0F, 0.0F, false);

		wroughtiron_front_3 = new ModelRenderer(this);
		wroughtiron_front_3.setRotationPoint(0.0F, 191.7778F, 46.3333F);
		roof_side_3.addChild(wroughtiron_front_3);
		wroughtiron_front_3.setTextureOffset(6, 420).addBox(-32.0F, -220.0F, -54.0F, 64.0F, 24.0F, 0.0F, 0.0F, false);

		outer_peak_3 = new ModelRenderer(this);
		outer_peak_3.setRotationPoint(0.0F, 0.0F, 0.0F);
		roof_side_3.addChild(outer_peak_3);
		setRotationAngle(outer_peak_3, 0.0873F, 0.0F, 0.0F);
		outer_peak_3.setTextureOffset(139, 320).addBox(-32.0F, -5.0222F, -10.6667F, 64.0F, 2.0F, 7.0F, 0.0F, false);
		outer_peak_3.setTextureOffset(139, 320).addBox(-29.8F, -3.0222F, -7.2667F, 60.0F, 2.0F, 8.0F, 0.0F, false);
		outer_peak_3.setTextureOffset(139, 320).addBox(-25.6F, -5.0222F, 0.3333F, 52.0F, 4.0F, 4.0F, 0.0F, false);
		outer_peak_3.setTextureOffset(139, 320).addBox(-23.6F, -5.0222F, 4.3333F, 48.0F, 4.0F, 4.0F, 0.0F, false);
		outer_peak_3.setTextureOffset(139, 320).addBox(-21.6F, -5.0222F, 8.3333F, 44.0F, 4.0F, 4.0F, 0.0F, false);
		outer_peak_3.setTextureOffset(139, 320).addBox(-19.6F, -5.0222F, 12.3333F, 40.0F, 4.0F, 4.0F, 0.0F, false);
		outer_peak_3.setTextureOffset(139, 320).addBox(-4.0F, -24.0222F, 42.3333F, 8.0F, 20.0F, 4.0F, 0.0F, false);
		outer_peak_3.setTextureOffset(139, 320).addBox(-27.6F, -5.0222F, -3.6667F, 56.0F, 4.0F, 4.0F, 0.0F, false);

		side_3 = new ModelRenderer(this);
		side_3.setRotationPoint(0.0F, 3.7778F, -1.6667F);
		panel_side_3.addChild(side_3);
		side_3.setTextureOffset(146, 393).addBox(-28.0F, -36.0F, -49.0F, 56.0F, 4.0F, 4.0F, 0.0F, false);
		side_3.setTextureOffset(139, 249).addBox(-28.0F, -184.0F, -49.0F, 4.0F, 148.0F, 4.0F, 0.0F, false);
		side_3.setTextureOffset(144, 246).addBox(24.0F, -184.0F, -49.0F, 4.0F, 148.0F, 4.0F, 0.0F, false);
		side_3.setTextureOffset(235, 246).addBox(-28.0F, -192.0F, -49.0F, 56.0F, 8.0F, 4.0F, 0.0F, false);
		side_3.setTextureOffset(171, 352).addBox(-24.0F, -136.0F, -49.0F, 48.0F, 12.0F, 4.0F, 0.0F, false);
		side_3.setTextureOffset(278, 374).addBox(-24.0F, -80.0F, -49.0F, 48.0F, 20.0F, 4.0F, 0.0F, false);
		side_3.setTextureOffset(118, 267).addBox(-28.0F, -195.2F, -51.0F, 56.0F, 4.0F, 4.0F, 0.0F, false);
		side_3.setTextureOffset(452, 18).addBox(-28.0F, -132.0F, -50.6F, 56.0F, 4.0F, 6.0F, 0.0F, false);
		side_3.setTextureOffset(448, 13).addBox(-28.0F, -76.0F, -50.6F, 56.0F, 4.0F, 6.0F, 0.0F, false);
		side_3.setTextureOffset(448, 84).addBox(-28.0F, -66.0F, -50.6F, 56.0F, 4.0F, 6.0F, 0.0F, false);
		side_3.setTextureOffset(459, 6).addBox(-28.0F, -33.0F, -50.6F, 56.0F, 4.0F, 6.0F, 0.0F, false);
		side_3.setTextureOffset(441, 93).addBox(-28.0F, -16.0F, -50.6F, 56.0F, 4.0F, 6.0F, 0.0F, false);
		side_3.setTextureOffset(288, 422).addBox(-28.0F, -32.0F, -49.0F, 56.0F, 16.0F, 4.0F, 0.0F, false);
		side_3.setTextureOffset(140, 392).addBox(-28.0F, -12.0F, -49.0F, 56.0F, 8.0F, 4.0F, 0.0F, false);

		top_panel_3 = new ModelRenderer(this);
		top_panel_3.setRotationPoint(0.0F, 0.0F, 0.0F);
		panel_side_3.addChild(top_panel_3);
		top_panel_3.setTextureOffset(272, 70).addBox(-24.0F, -180.2222F, -49.6667F, 48.0F, 48.0F, 4.0F, 0.0F, false);

		cage3 = new ModelRenderer(this);
		cage3.setRotationPoint(0.0F, 3.7778F, -1.6667F);
		top_panel_3.addChild(cage3);
		cage3.setTextureOffset(480, 38).addBox(-9.0F, -177.0F, -58.0F, 4.0F, 12.0F, 17.0F, 0.0F, false);
		cage3.setTextureOffset(480, 38).addBox(5.0F, -177.0F, -58.0F, 4.0F, 8.0F, 17.0F, 0.0F, false);
		cage3.setTextureOffset(480, 38).addBox(-17.0F, -169.0F, -58.6F, 11.0F, 4.0F, 18.0F, 0.0F, false);
		cage3.setTextureOffset(480, 38).addBox(-17.0F, -155.0F, -58.6F, 11.0F, 4.0F, 18.0F, 0.0F, false);
		cage3.setTextureOffset(480, 38).addBox(5.0F, -151.0F, -58.0F, 4.0F, 8.0F, 17.0F, 0.0F, false);
		cage3.setTextureOffset(480, 38).addBox(-9.0F, -151.0F, -58.0F, 4.0F, 8.0F, 17.0F, 0.0F, false);
		cage3.setTextureOffset(480, 38).addBox(6.0F, -155.0F, -58.6F, 11.0F, 4.0F, 18.0F, 0.0F, false);
		cage3.setTextureOffset(480, 38).addBox(6.0F, -169.0F, -58.6F, 11.0F, 4.0F, 18.0F, 0.0F, false);
		cage3.setTextureOffset(480, 38).addBox(-6.0F, -169.0F, -59.6F, 12.0F, 4.0F, 19.0F, 0.0F, false);
		cage3.setTextureOffset(480, 38).addBox(-6.0F, -155.0F, -59.6F, 12.0F, 4.0F, 19.0F, 0.0F, false);
		cage3.setTextureOffset(480, 38).addBox(-9.0F, -165.0F, -58.0F, 4.0F, 12.0F, 17.0F, 0.0F, false);
		cage3.setTextureOffset(480, 38).addBox(5.0F, -165.0F, -58.0F, 4.0F, 12.0F, 17.0F, 0.0F, false);
		cage3.setTextureOffset(480, 38).addBox(-16.0F, -176.0F, -52.0F, 32.0F, 32.0F, 10.0F, 0.0F, false);

		middle_panel_3 = new ModelRenderer(this);
		middle_panel_3.setRotationPoint(0.0F, 0.0F, 0.0F);
		panel_side_3.addChild(middle_panel_3);
		middle_panel_3.setTextureOffset(265, 134).addBox(-24.0F, -120.2222F, -49.6667F, 48.0F, 44.0F, 4.0F, 0.0F, false);
		middle_panel_3.setTextureOffset(452, 38).addBox(-18.0F, -114.2222F, -51.6667F, 36.0F, 32.0F, 4.0F, 0.0F, false);
		middle_panel_3.setTextureOffset(452, 38).addBox(13.0F, -113.2222F, -52.6667F, 4.0F, 4.0F, 4.0F, 0.0F, false);
		middle_panel_3.setTextureOffset(452, 38).addBox(-17.0F, -113.2222F, -52.6667F, 4.0F, 4.0F, 4.0F, 0.0F, false);
		middle_panel_3.setTextureOffset(452, 38).addBox(-17.0F, -87.2222F, -52.6667F, 4.0F, 4.0F, 4.0F, 0.0F, false);
		middle_panel_3.setTextureOffset(452, 38).addBox(13.0F, -87.2222F, -52.6667F, 4.0F, 4.0F, 4.0F, 0.0F, false);

		lower_panel_3 = new ModelRenderer(this);
		lower_panel_3.setRotationPoint(0.0F, 0.0F, 0.0F);
		panel_side_3.addChild(lower_panel_3);
		

		componets3 = new ModelRenderer(this);
		componets3.setRotationPoint(0.0F, 0.0F, 0.0F);
		lower_panel_3.addChild(componets3);
		componets3.setTextureOffset(496, 290).addBox(-21.2F, -54.4222F, -46.6667F, 8.0F, 8.0F, 4.0F, 0.0F, false);
		componets3.setTextureOffset(454, 201).addBox(-20.8F, -53.8222F, -47.6667F, 7.0F, 7.0F, 4.0F, 0.0F, false);
		componets3.setTextureOffset(491, 38).addBox(-3.2F, -58.4222F, -46.6667F, 1.0F, 28.0F, 4.0F, 0.0F, false);
		componets3.setTextureOffset(491, 38).addBox(-5.2F, -58.4222F, -46.6667F, 1.0F, 28.0F, 4.0F, 0.0F, false);
		componets3.setTextureOffset(491, 38).addBox(-9.4F, -58.4222F, -46.6667F, 1.0F, 8.0F, 4.0F, 0.0F, false);
		componets3.setTextureOffset(491, 38).addBox(-13.4F, -50.4222F, -46.6667F, 5.0F, 1.0F, 4.0F, 0.0F, false);
		componets3.setTextureOffset(491, 38).addBox(-17.4F, -46.4222F, -46.6667F, 1.0F, 16.0F, 4.0F, 0.0F, false);
		componets3.setTextureOffset(692, 166).addBox(-14.2F, -42.4222F, -46.6667F, 4.0F, 4.0F, 4.0F, 0.0F, false);
		componets3.setTextureOffset(694, 155).addBox(0.6F, -38.4222F, -46.6667F, 4.0F, 4.0F, 4.0F, 0.0F, false);
		componets3.setTextureOffset(665, 171).addBox(0.6F, -54.4222F, -46.6667F, 4.0F, 4.0F, 4.0F, 0.0F, false);
		componets3.setTextureOffset(491, 38).addBox(-10.2F, -40.6222F, -46.6667F, 5.0F, 1.0F, 4.0F, 0.0F, false);
		componets3.setTextureOffset(491, 38).addBox(2.0F, -50.4222F, -46.6667F, 1.0F, 12.0F, 4.0F, 0.0F, false);

		gear = new ModelRenderer(this);
		gear.setRotationPoint(6.8F, -42.4222F, -45.8334F);
		componets3.addChild(gear);
		setRotationAngle(gear, 0.0F, 0.0F, -0.6109F);
		gear.setTextureOffset(491, 38).addBox(9.2F, -1.2F, -1.8333F, 2.0F, 4.0F, 4.0F, 0.0F, false);
		gear.setTextureOffset(491, 38).addBox(1.2F, -3.2F, -1.8333F, 8.0F, 8.0F, 4.0F, 0.0F, false);
		gear.setTextureOffset(491, 38).addBox(3.2F, -5.2F, -1.8333F, 4.0F, 2.0F, 4.0F, 0.0F, false);
		gear.setTextureOffset(491, 38).addBox(-0.8F, -1.2F, -1.8333F, 2.0F, 4.0F, 4.0F, 0.0F, false);
		gear.setTextureOffset(491, 38).addBox(3.2F, 4.8F, -1.8333F, 4.0F, 2.0F, 4.0F, 0.0F, false);
		gear.setTextureOffset(704, 48).addBox(4.2F, -0.2F, -2.8333F, 2.0F, 2.0F, 4.0F, 0.0F, false);

		gear2 = new ModelRenderer(this);
		gear2.setRotationPoint(6.8F, -42.4222F, -45.8334F);
		componets3.addChild(gear2);
		setRotationAngle(gear2, 0.0F, 0.0F, -0.6109F);
		gear2.setTextureOffset(491, 38).addBox(20.0F, -6.0F, -1.8333F, 2.0F, 4.0F, 4.0F, 0.0F, false);
		gear2.setTextureOffset(491, 38).addBox(12.0F, -8.0F, -1.8333F, 8.0F, 8.0F, 4.0F, 0.0F, false);
		gear2.setTextureOffset(491, 38).addBox(14.0F, -10.0F, -1.8333F, 4.0F, 2.0F, 4.0F, 0.0F, false);
		gear2.setTextureOffset(491, 38).addBox(10.0F, -6.0F, -1.8333F, 2.0F, 4.0F, 4.0F, 0.0F, false);
		gear2.setTextureOffset(491, 38).addBox(14.0F, 0.0F, -1.8333F, 4.0F, 2.0F, 4.0F, 0.0F, false);
		gear2.setTextureOffset(699, 45).addBox(15.0F, -5.0F, -2.8333F, 2.0F, 2.0F, 4.0F, 0.0F, false);

		gear3 = new ModelRenderer(this);
		gear3.setRotationPoint(6.8F, -42.4222F, -45.8334F);
		componets3.addChild(gear3);
		setRotationAngle(gear3, 0.0F, 0.0F, -0.6109F);
		gear3.setTextureOffset(491, 38).addBox(13.6F, 9.2F, -1.8333F, 2.0F, 4.0F, 4.0F, 0.0F, false);
		gear3.setTextureOffset(491, 38).addBox(5.6F, 7.2F, -1.8333F, 8.0F, 8.0F, 4.0F, 0.0F, false);
		gear3.setTextureOffset(491, 38).addBox(7.6F, 5.2F, -1.8333F, 4.0F, 2.0F, 4.0F, 0.0F, false);
		gear3.setTextureOffset(491, 38).addBox(3.6F, 9.2F, -1.8333F, 2.0F, 4.0F, 4.0F, 0.0F, false);
		gear3.setTextureOffset(491, 38).addBox(7.6F, 15.2F, -1.8333F, 4.0F, 2.0F, 4.0F, 0.0F, false);
		gear3.setTextureOffset(724, 57).addBox(8.6F, 10.2F, -2.8333F, 2.0F, 2.0F, 4.0F, 0.0F, false);

		panelbox5 = new ModelRenderer(this);
		panelbox5.setRotationPoint(0.0F, 0.0F, 0.0F);
		lower_panel_3.addChild(panelbox5);
		panelbox5.setTextureOffset(649, 36).addBox(-24.0F, -57.2222F, -45.2667F, 48.0F, 27.0F, 0.0F, 0.0F, false);
		panelbox5.setTextureOffset(651, 50).addBox(-23.0F, -57.2222F, -49.2667F, 46.0F, 2.0F, 4.0F, 0.0F, false);
		panelbox5.setTextureOffset(674, 57).addBox(-23.0F, -33.2222F, -49.2667F, 46.0F, 2.0F, 4.0F, 0.0F, false);

		panelbox6 = new ModelRenderer(this);
		panelbox6.setRotationPoint(0.0F, 0.0F, 0.0F);
		lower_panel_3.addChild(panelbox6);
		panelbox6.setTextureOffset(683, 41).addBox(-25.0F, -57.2222F, -49.2667F, 2.0F, 26.0F, 4.0F, 0.0F, false);
		panelbox6.setTextureOffset(676, 27).addBox(23.0F, -57.2222F, -49.2667F, 2.0F, 26.0F, 4.0F, 0.0F, false);

		floorboards_3 = new ModelRenderer(this);
		floorboards_3.setRotationPoint(0.0F, 0.0F, 0.0F);
		panel_side_3.addChild(floorboards_3);
		floorboards_3.setTextureOffset(246, 189).addBox(-32.4F, -0.2222F, -58.6667F, 64.0F, 4.0F, 4.0F, 0.0F, false);
		floorboards_3.setTextureOffset(246, 189).addBox(-26.0F, -0.2222F, -46.6667F, 52.0F, 4.0F, 4.0F, 0.0F, false);
		floorboards_3.setTextureOffset(246, 189).addBox(-24.0F, -0.2222F, -42.6667F, 48.0F, 4.0F, 4.0F, 0.0F, false);
		floorboards_3.setTextureOffset(246, 189).addBox(-22.0F, -0.2222F, -38.6667F, 44.0F, 4.0F, 4.0F, 0.0F, false);
		floorboards_3.setTextureOffset(246, 189).addBox(-18.0F, -0.2222F, -34.6667F, 36.0F, 4.0F, 4.0F, 0.0F, false);
		floorboards_3.setTextureOffset(246, 189).addBox(-16.0F, -0.2222F, -30.6667F, 32.0F, 4.0F, 4.0F, 0.0F, false);
		floorboards_3.setTextureOffset(246, 189).addBox(-14.0F, -0.2222F, -26.6667F, 28.0F, 4.0F, 4.0F, 0.0F, false);
		floorboards_3.setTextureOffset(246, 189).addBox(-12.0F, -0.2222F, -22.6667F, 24.0F, 4.0F, 4.0F, 0.0F, false);
		floorboards_3.setTextureOffset(246, 189).addBox(-10.0F, -0.2222F, -18.6667F, 20.0F, 4.0F, 4.0F, 0.0F, false);
		floorboards_3.setTextureOffset(246, 189).addBox(-8.0F, -0.2222F, -14.6667F, 16.0F, 4.0F, 4.0F, 0.0F, false);
		floorboards_3.setTextureOffset(246, 189).addBox(-6.0F, -0.2222F, -10.6667F, 12.0F, 4.0F, 4.0F, 0.0F, false);
		floorboards_3.setTextureOffset(246, 189).addBox(-4.0F, -0.2222F, -6.6667F, 8.0F, 4.0F, 4.0F, 0.0F, false);
		floorboards_3.setTextureOffset(246, 189).addBox(-30.8F, -0.2222F, -54.6667F, 60.0F, 4.0F, 4.0F, 0.0F, false);
		floorboards_3.setTextureOffset(246, 189).addBox(-28.0F, -0.2222F, -50.6667F, 56.0F, 4.0F, 4.0F, 0.0F, false);

		panel_side_4 = new ModelRenderer(this);
		panel_side_4.setRotationPoint(0.0F, -3.7778F, 1.6667F);
		box.addChild(panel_side_4);
		setRotationAngle(panel_side_4, 0.0F, 2.0944F, 0.0F);
		

		roof_side_4 = new ModelRenderer(this);
		roof_side_4.setRotationPoint(0.0F, -188.0F, -48.0F);
		panel_side_4.addChild(roof_side_4);
		

		center_peak_4 = new ModelRenderer(this);
		center_peak_4.setRotationPoint(0.0F, 0.0F, 16.0F);
		roof_side_4.addChild(center_peak_4);
		setRotationAngle(center_peak_4, 0.1047F, 0.0F, 0.0F);
		center_peak_4.setTextureOffset(196, 306).addBox(-18.0F, -13.8222F, -1.4667F, 37.0F, 2.0F, 4.0F, 0.0F, false);
		center_peak_4.setTextureOffset(162, 306).addBox(-6.0F, -18.2222F, 22.5333F, 12.0F, 8.0F, 8.0F, 0.0F, false);
		center_peak_4.setTextureOffset(162, 306).addBox(-6.0F, -18.2222F, 30.5333F, 12.0F, 4.0F, 4.0F, 0.0F, false);
		center_peak_4.setTextureOffset(162, 306).addBox(-12.0F, -12.2222F, -1.0667F, 4.0F, 8.0F, 8.0F, 0.0F, false);
		center_peak_4.setTextureOffset(162, 306).addBox(-2.0F, -12.2222F, -1.0667F, 4.0F, 8.0F, 8.0F, 0.0F, false);
		center_peak_4.setTextureOffset(162, 306).addBox(8.0F, -12.2222F, -1.0667F, 4.0F, 8.0F, 8.0F, 0.0F, false);
		center_peak_4.setTextureOffset(162, 306).addBox(-16.0F, -13.8222F, 2.5333F, 32.0F, 2.0F, 4.0F, 0.0F, false);
		center_peak_4.setTextureOffset(114, 313).addBox(-14.0F, -13.8222F, 6.5333F, 28.0F, 2.0F, 4.0F, 0.0F, false);
		center_peak_4.setTextureOffset(162, 306).addBox(-12.0F, -13.8222F, 10.5333F, 24.0F, 2.0F, 4.0F, 0.0F, false);
		center_peak_4.setTextureOffset(185, 288).addBox(-10.0F, -13.8222F, 14.5333F, 20.0F, 2.0F, 4.0F, 0.0F, false);
		center_peak_4.setTextureOffset(162, 306).addBox(-8.0F, -13.8222F, 18.5333F, 16.0F, 2.0F, 4.0F, 0.0F, false);

		wroughtiron_front_4 = new ModelRenderer(this);
		wroughtiron_front_4.setRotationPoint(0.0F, 191.7778F, 46.3333F);
		roof_side_4.addChild(wroughtiron_front_4);
		wroughtiron_front_4.setTextureOffset(6, 420).addBox(-32.0F, -220.0F, -54.0F, 64.0F, 24.0F, 0.0F, 0.0F, false);

		outer_peak_4 = new ModelRenderer(this);
		outer_peak_4.setRotationPoint(0.0F, 0.0F, 0.0F);
		roof_side_4.addChild(outer_peak_4);
		setRotationAngle(outer_peak_4, 0.0873F, 0.0F, 0.0F);
		outer_peak_4.setTextureOffset(139, 320).addBox(-32.0F, -5.0222F, -10.6667F, 64.0F, 2.0F, 7.0F, 0.0F, false);
		outer_peak_4.setTextureOffset(139, 320).addBox(-29.8F, -3.0222F, -7.2667F, 60.0F, 2.0F, 8.0F, 0.0F, false);
		outer_peak_4.setTextureOffset(139, 320).addBox(-25.6F, -5.0222F, 0.3333F, 52.0F, 4.0F, 4.0F, 0.0F, false);
		outer_peak_4.setTextureOffset(139, 320).addBox(-23.6F, -5.0222F, 4.3333F, 48.0F, 4.0F, 4.0F, 0.0F, false);
		outer_peak_4.setTextureOffset(139, 320).addBox(-21.6F, -5.0222F, 8.3333F, 44.0F, 4.0F, 4.0F, 0.0F, false);
		outer_peak_4.setTextureOffset(139, 320).addBox(-19.6F, -5.0222F, 12.3333F, 40.0F, 4.0F, 4.0F, 0.0F, false);
		outer_peak_4.setTextureOffset(139, 320).addBox(-4.0F, -24.0222F, 42.3333F, 8.0F, 20.0F, 4.0F, 0.0F, false);
		outer_peak_4.setTextureOffset(139, 320).addBox(-27.6F, -5.0222F, -3.6667F, 56.0F, 4.0F, 4.0F, 0.0F, false);

		side_4 = new ModelRenderer(this);
		side_4.setRotationPoint(0.0F, 3.7778F, -1.6667F);
		panel_side_4.addChild(side_4);
		side_4.setTextureOffset(146, 393).addBox(-28.0F, -36.0F, -49.0F, 56.0F, 4.0F, 4.0F, 0.0F, false);
		side_4.setTextureOffset(139, 249).addBox(-28.0F, -184.0F, -49.0F, 4.0F, 148.0F, 4.0F, 0.0F, false);
		side_4.setTextureOffset(144, 246).addBox(24.0F, -184.0F, -49.0F, 4.0F, 148.0F, 4.0F, 0.0F, false);
		side_4.setTextureOffset(235, 246).addBox(-28.0F, -192.0F, -49.0F, 56.0F, 8.0F, 4.0F, 0.0F, false);
		side_4.setTextureOffset(171, 352).addBox(-24.0F, -136.0F, -49.0F, 48.0F, 12.0F, 4.0F, 0.0F, false);
		side_4.setTextureOffset(278, 374).addBox(-24.0F, -80.0F, -49.0F, 48.0F, 20.0F, 4.0F, 0.0F, false);
		side_4.setTextureOffset(118, 267).addBox(-28.0F, -195.2F, -51.0F, 56.0F, 4.0F, 4.0F, 0.0F, false);
		side_4.setTextureOffset(452, 18).addBox(-28.0F, -132.0F, -50.6F, 56.0F, 4.0F, 6.0F, 0.0F, false);
		side_4.setTextureOffset(448, 13).addBox(-28.0F, -76.0F, -50.6F, 56.0F, 4.0F, 6.0F, 0.0F, false);
		side_4.setTextureOffset(448, 84).addBox(-28.0F, -66.0F, -50.6F, 56.0F, 4.0F, 6.0F, 0.0F, false);
		side_4.setTextureOffset(459, 6).addBox(-28.0F, -33.0F, -50.6F, 56.0F, 4.0F, 6.0F, 0.0F, false);
		side_4.setTextureOffset(441, 93).addBox(-28.0F, -16.0F, -50.6F, 56.0F, 4.0F, 6.0F, 0.0F, false);
		side_4.setTextureOffset(288, 422).addBox(-28.0F, -32.0F, -49.0F, 56.0F, 16.0F, 4.0F, 0.0F, false);
		side_4.setTextureOffset(140, 392).addBox(-28.0F, -12.0F, -49.0F, 56.0F, 8.0F, 4.0F, 0.0F, false);

		top_panel_4 = new ModelRenderer(this);
		top_panel_4.setRotationPoint(0.0F, 0.0F, 0.0F);
		panel_side_4.addChild(top_panel_4);
		top_panel_4.setTextureOffset(262, 80).addBox(-24.0F, -180.2222F, -49.6667F, 48.0F, 48.0F, 4.0F, 0.0F, false);

		cage4 = new ModelRenderer(this);
		cage4.setRotationPoint(0.0F, 3.7778F, -1.6667F);
		top_panel_4.addChild(cage4);
		cage4.setTextureOffset(480, 38).addBox(-9.0F, -177.0F, -58.0F, 4.0F, 12.0F, 17.0F, 0.0F, false);
		cage4.setTextureOffset(480, 38).addBox(5.0F, -177.0F, -58.0F, 4.0F, 8.0F, 17.0F, 0.0F, false);
		cage4.setTextureOffset(480, 38).addBox(-17.0F, -169.0F, -58.6F, 11.0F, 4.0F, 18.0F, 0.0F, false);
		cage4.setTextureOffset(480, 38).addBox(-17.0F, -155.0F, -58.6F, 11.0F, 4.0F, 18.0F, 0.0F, false);
		cage4.setTextureOffset(480, 38).addBox(5.0F, -151.0F, -58.0F, 4.0F, 8.0F, 17.0F, 0.0F, false);
		cage4.setTextureOffset(480, 38).addBox(-9.0F, -151.0F, -58.0F, 4.0F, 8.0F, 17.0F, 0.0F, false);
		cage4.setTextureOffset(480, 38).addBox(6.0F, -155.0F, -58.6F, 11.0F, 4.0F, 18.0F, 0.0F, false);
		cage4.setTextureOffset(480, 38).addBox(6.0F, -169.0F, -58.6F, 11.0F, 4.0F, 18.0F, 0.0F, false);
		cage4.setTextureOffset(480, 38).addBox(-6.0F, -169.0F, -59.6F, 12.0F, 4.0F, 19.0F, 0.0F, false);
		cage4.setTextureOffset(480, 38).addBox(-6.0F, -155.0F, -59.6F, 12.0F, 4.0F, 19.0F, 0.0F, false);
		cage4.setTextureOffset(480, 38).addBox(-9.0F, -165.0F, -58.0F, 4.0F, 12.0F, 17.0F, 0.0F, false);
		cage4.setTextureOffset(480, 38).addBox(5.0F, -165.0F, -58.0F, 4.0F, 12.0F, 17.0F, 0.0F, false);
		cage4.setTextureOffset(480, 38).addBox(-16.0F, -176.0F, -52.0F, 32.0F, 32.0F, 10.0F, 0.0F, false);

		middle_panel_4 = new ModelRenderer(this);
		middle_panel_4.setRotationPoint(0.0F, 0.0F, 0.0F);
		panel_side_4.addChild(middle_panel_4);
		middle_panel_4.setTextureOffset(272, 114).addBox(-24.0F, -120.2222F, -49.6667F, 48.0F, 44.0F, 4.0F, 0.0F, false);
		middle_panel_4.setTextureOffset(452, 38).addBox(-18.0F, -114.2222F, -51.6667F, 36.0F, 32.0F, 4.0F, 0.0F, false);
		middle_panel_4.setTextureOffset(452, 38).addBox(13.0F, -113.2222F, -52.6667F, 4.0F, 4.0F, 4.0F, 0.0F, false);
		middle_panel_4.setTextureOffset(452, 38).addBox(-17.0F, -113.2222F, -52.6667F, 4.0F, 4.0F, 4.0F, 0.0F, false);
		middle_panel_4.setTextureOffset(452, 38).addBox(-17.0F, -87.2222F, -52.6667F, 4.0F, 4.0F, 4.0F, 0.0F, false);
		middle_panel_4.setTextureOffset(452, 38).addBox(13.0F, -87.2222F, -52.6667F, 4.0F, 4.0F, 4.0F, 0.0F, false);

		lower_panel_4 = new ModelRenderer(this);
		lower_panel_4.setRotationPoint(0.0F, 0.0F, 0.0F);
		panel_side_4.addChild(lower_panel_4);
		

		gear4 = new ModelRenderer(this);
		gear4.setRotationPoint(6.8F, -42.4222F, -45.8334F);
		lower_panel_4.addChild(gear4);
		setRotationAngle(gear4, 0.0F, 0.0F, -0.6109F);
		gear4.setTextureOffset(544, 34).addBox(-10.8F, -22.2F, -0.8333F, 2.0F, 4.0F, 4.0F, 0.0F, false);
		gear4.setTextureOffset(544, 34).addBox(-18.8F, -24.2F, -0.8333F, 8.0F, 8.0F, 4.0F, 0.0F, false);
		gear4.setTextureOffset(544, 34).addBox(-16.8F, -26.2F, -0.8333F, 4.0F, 2.0F, 4.0F, 0.0F, false);
		gear4.setTextureOffset(544, 34).addBox(-20.8F, -22.2F, -0.8333F, 2.0F, 4.0F, 4.0F, 0.0F, false);
		gear4.setTextureOffset(544, 34).addBox(-16.8F, -16.2F, -0.8333F, 4.0F, 2.0F, 4.0F, 0.0F, false);
		gear4.setTextureOffset(678, 48).addBox(-15.8F, -21.2F, -1.8333F, 2.0F, 2.0F, 4.0F, 0.0F, false);
		gear4.setTextureOffset(544, 34).addBox(-23.4F, -17.6F, -0.8333F, 4.0F, 4.0F, 4.0F, 0.0F, false);
		gear4.setTextureOffset(706, 68).addBox(-22.2F, -16.6F, -1.8333F, 2.0F, 2.0F, 4.0F, 0.0F, false);

		componets4 = new ModelRenderer(this);
		componets4.setRotationPoint(0.0F, 0.0F, 0.0F);
		lower_panel_4.addChild(componets4);
		componets4.setTextureOffset(486, 278).addBox(-8.0F, -53.2222F, -48.6667F, 12.0F, 8.0F, 4.0F, 0.0F, false);
		componets4.setTextureOffset(544, 34).addBox(-4.0F, -45.2222F, -46.6667F, 4.0F, 6.0F, 3.0F, 0.0F, false);
		componets4.setTextureOffset(486, 267).addBox(-14.0F, -42.2222F, -47.2667F, 8.0F, 8.0F, 4.0F, 0.0F, false);
		componets4.setTextureOffset(544, 34).addBox(-24.0F, -39.2222F, -46.6667F, 24.0F, 4.0F, 3.0F, 0.0F, false);
		componets4.setTextureOffset(708, 139).addBox(11.0F, -42.2222F, -47.2667F, 2.0F, 8.0F, 4.0F, 0.0F, false);
		componets4.setTextureOffset(496, 274).addBox(15.2F, -38.2222F, -46.0667F, 4.0F, 4.0F, 2.0F, 0.0F, false);
		componets4.setTextureOffset(544, 34).addBox(16.2F, -58.2222F, -46.0667F, 2.0F, 20.0F, 2.0F, 0.0F, false);
		componets4.setTextureOffset(544, 34).addBox(10.2F, -48.2222F, -46.0667F, 6.0F, 2.0F, 2.0F, 0.0F, false);
		componets4.setTextureOffset(480, 281).addBox(6.2F, -49.2222F, -46.0667F, 4.0F, 4.0F, 2.0F, 0.0F, false);
		componets4.setTextureOffset(699, 162).addBox(8.0F, -42.2222F, -47.2667F, 2.0F, 8.0F, 4.0F, 0.0F, false);
		componets4.setTextureOffset(694, 178).addBox(5.0F, -42.2222F, -47.2667F, 2.0F, 8.0F, 4.0F, 0.0F, false);

		panelbox7 = new ModelRenderer(this);
		panelbox7.setRotationPoint(0.0F, 0.0F, 0.0F);
		lower_panel_4.addChild(panelbox7);
		panelbox7.setTextureOffset(651, 45).addBox(-24.0F, -57.2222F, -45.2667F, 48.0F, 27.0F, 0.0F, 0.0F, false);
		panelbox7.setTextureOffset(646, 36).addBox(-23.0F, -57.2222F, -49.2667F, 46.0F, 2.0F, 4.0F, 0.0F, false);
		panelbox7.setTextureOffset(656, 45).addBox(-23.0F, -33.2222F, -49.2667F, 46.0F, 2.0F, 4.0F, 0.0F, false);

		panelbox8 = new ModelRenderer(this);
		panelbox8.setRotationPoint(0.0F, 0.0F, 0.0F);
		lower_panel_4.addChild(panelbox8);
		panelbox8.setTextureOffset(708, 36).addBox(-25.0F, -57.2222F, -49.2667F, 2.0F, 26.0F, 4.0F, 0.0F, false);
		panelbox8.setTextureOffset(692, 29).addBox(23.0F, -57.2222F, -49.2667F, 2.0F, 26.0F, 4.0F, 0.0F, false);

		floorboards_4 = new ModelRenderer(this);
		floorboards_4.setRotationPoint(0.0F, 0.0F, 0.0F);
		panel_side_4.addChild(floorboards_4);
		floorboards_4.setTextureOffset(246, 173).addBox(-32.4F, -0.2222F, -58.6667F, 64.0F, 4.0F, 4.0F, 0.0F, false);
		floorboards_4.setTextureOffset(246, 173).addBox(-26.0F, -0.2222F, -46.6667F, 52.0F, 4.0F, 4.0F, 0.0F, false);
		floorboards_4.setTextureOffset(246, 173).addBox(-24.0F, -0.2222F, -42.6667F, 48.0F, 4.0F, 4.0F, 0.0F, false);
		floorboards_4.setTextureOffset(246, 173).addBox(-22.0F, -0.2222F, -38.6667F, 44.0F, 4.0F, 4.0F, 0.0F, false);
		floorboards_4.setTextureOffset(246, 173).addBox(-18.0F, -0.2222F, -34.6667F, 36.0F, 4.0F, 4.0F, 0.0F, false);
		floorboards_4.setTextureOffset(246, 173).addBox(-16.0F, -0.2222F, -30.6667F, 32.0F, 4.0F, 4.0F, 0.0F, false);
		floorboards_4.setTextureOffset(246, 173).addBox(-14.0F, -0.2222F, -26.6667F, 28.0F, 4.0F, 4.0F, 0.0F, false);
		floorboards_4.setTextureOffset(246, 173).addBox(-12.0F, -0.2222F, -22.6667F, 24.0F, 4.0F, 4.0F, 0.0F, false);
		floorboards_4.setTextureOffset(246, 173).addBox(-10.0F, -0.2222F, -18.6667F, 20.0F, 4.0F, 4.0F, 0.0F, false);
		floorboards_4.setTextureOffset(246, 173).addBox(-8.0F, -0.2222F, -14.6667F, 16.0F, 4.0F, 4.0F, 0.0F, false);
		floorboards_4.setTextureOffset(246, 173).addBox(-6.0F, -0.2222F, -10.6667F, 12.0F, 4.0F, 4.0F, 0.0F, false);
		floorboards_4.setTextureOffset(246, 173).addBox(-4.0F, -0.2222F, -6.6667F, 8.0F, 4.0F, 4.0F, 0.0F, false);
		floorboards_4.setTextureOffset(246, 173).addBox(-30.8F, -0.2222F, -54.6667F, 60.0F, 4.0F, 4.0F, 0.0F, false);
		floorboards_4.setTextureOffset(246, 173).addBox(-28.0F, -0.2222F, -50.6667F, 56.0F, 4.0F, 4.0F, 0.0F, false);

		panel_side_5 = new ModelRenderer(this);
		panel_side_5.setRotationPoint(0.0F, -3.7778F, 1.6667F);
		box.addChild(panel_side_5);
		setRotationAngle(panel_side_5, 0.0F, 1.0472F, 0.0F);
		

		roof_side_5 = new ModelRenderer(this);
		roof_side_5.setRotationPoint(0.0F, -188.0F, -48.0F);
		panel_side_5.addChild(roof_side_5);
		

		center_peak_5 = new ModelRenderer(this);
		center_peak_5.setRotationPoint(0.0F, 0.0F, 16.0F);
		roof_side_5.addChild(center_peak_5);
		setRotationAngle(center_peak_5, 0.1047F, 0.0F, 0.0F);
		center_peak_5.setTextureOffset(196, 306).addBox(-18.0F, -13.8222F, -1.4667F, 37.0F, 2.0F, 4.0F, 0.0F, false);
		center_peak_5.setTextureOffset(162, 306).addBox(-6.0F, -18.2222F, 22.5333F, 12.0F, 8.0F, 8.0F, 0.0F, false);
		center_peak_5.setTextureOffset(162, 306).addBox(-6.0F, -18.2222F, 30.5333F, 12.0F, 4.0F, 4.0F, 0.0F, false);
		center_peak_5.setTextureOffset(162, 306).addBox(-12.0F, -12.2222F, -1.0667F, 4.0F, 8.0F, 8.0F, 0.0F, false);
		center_peak_5.setTextureOffset(162, 306).addBox(-2.0F, -12.2222F, -1.0667F, 4.0F, 8.0F, 8.0F, 0.0F, false);
		center_peak_5.setTextureOffset(162, 306).addBox(8.0F, -12.2222F, -1.0667F, 4.0F, 8.0F, 8.0F, 0.0F, false);
		center_peak_5.setTextureOffset(162, 306).addBox(-16.0F, -13.8222F, 2.5333F, 32.0F, 2.0F, 4.0F, 0.0F, false);
		center_peak_5.setTextureOffset(114, 313).addBox(-14.0F, -13.8222F, 6.5333F, 28.0F, 2.0F, 4.0F, 0.0F, false);
		center_peak_5.setTextureOffset(162, 306).addBox(-12.0F, -13.8222F, 10.5333F, 24.0F, 2.0F, 4.0F, 0.0F, false);
		center_peak_5.setTextureOffset(185, 288).addBox(-10.0F, -13.8222F, 14.5333F, 20.0F, 2.0F, 4.0F, 0.0F, false);
		center_peak_5.setTextureOffset(162, 306).addBox(-8.0F, -13.8222F, 18.5333F, 16.0F, 2.0F, 4.0F, 0.0F, false);

		wroughtiron_front_5 = new ModelRenderer(this);
		wroughtiron_front_5.setRotationPoint(0.0F, 191.7778F, 46.3333F);
		roof_side_5.addChild(wroughtiron_front_5);
		wroughtiron_front_5.setTextureOffset(6, 420).addBox(-32.0F, -220.0F, -54.0F, 64.0F, 24.0F, 0.0F, 0.0F, false);

		outer_peak_5 = new ModelRenderer(this);
		outer_peak_5.setRotationPoint(0.0F, 0.0F, 0.0F);
		roof_side_5.addChild(outer_peak_5);
		setRotationAngle(outer_peak_5, 0.0873F, 0.0F, 0.0F);
		outer_peak_5.setTextureOffset(139, 320).addBox(-32.0F, -5.0222F, -10.6667F, 64.0F, 2.0F, 7.0F, 0.0F, false);
		outer_peak_5.setTextureOffset(139, 320).addBox(-29.8F, -3.0222F, -7.2667F, 60.0F, 2.0F, 8.0F, 0.0F, false);
		outer_peak_5.setTextureOffset(139, 320).addBox(-25.6F, -5.0222F, 0.3333F, 52.0F, 4.0F, 4.0F, 0.0F, false);
		outer_peak_5.setTextureOffset(139, 320).addBox(-23.6F, -5.0222F, 4.3333F, 48.0F, 4.0F, 4.0F, 0.0F, false);
		outer_peak_5.setTextureOffset(139, 320).addBox(-21.6F, -5.0222F, 8.3333F, 44.0F, 4.0F, 4.0F, 0.0F, false);
		outer_peak_5.setTextureOffset(139, 320).addBox(-19.6F, -5.0222F, 12.3333F, 40.0F, 4.0F, 4.0F, 0.0F, false);
		outer_peak_5.setTextureOffset(139, 320).addBox(-4.0F, -24.0222F, 42.3333F, 8.0F, 20.0F, 4.0F, 0.0F, false);
		outer_peak_5.setTextureOffset(139, 320).addBox(-27.6F, -5.0222F, -3.6667F, 56.0F, 4.0F, 4.0F, 0.0F, false);

		side_5 = new ModelRenderer(this);
		side_5.setRotationPoint(0.0F, 3.7778F, -1.6667F);
		panel_side_5.addChild(side_5);
		side_5.setTextureOffset(146, 393).addBox(-28.0F, -36.0F, -49.0F, 56.0F, 4.0F, 4.0F, 0.0F, false);
		side_5.setTextureOffset(139, 249).addBox(-28.0F, -184.0F, -49.0F, 4.0F, 148.0F, 4.0F, 0.0F, false);
		side_5.setTextureOffset(144, 246).addBox(24.0F, -184.0F, -49.0F, 4.0F, 148.0F, 4.0F, 0.0F, false);
		side_5.setTextureOffset(235, 246).addBox(-28.0F, -192.0F, -49.0F, 56.0F, 8.0F, 4.0F, 0.0F, false);
		side_5.setTextureOffset(171, 352).addBox(-24.0F, -136.0F, -49.0F, 48.0F, 12.0F, 4.0F, 0.0F, false);
		side_5.setTextureOffset(278, 374).addBox(-24.0F, -80.0F, -49.0F, 48.0F, 20.0F, 4.0F, 0.0F, false);
		side_5.setTextureOffset(118, 267).addBox(-28.0F, -195.2F, -51.0F, 56.0F, 4.0F, 4.0F, 0.0F, false);
		side_5.setTextureOffset(452, 18).addBox(-28.0F, -132.0F, -50.6F, 56.0F, 4.0F, 6.0F, 0.0F, false);
		side_5.setTextureOffset(448, 13).addBox(-28.0F, -76.0F, -50.6F, 56.0F, 4.0F, 6.0F, 0.0F, false);
		side_5.setTextureOffset(448, 84).addBox(-28.0F, -66.0F, -50.6F, 56.0F, 4.0F, 6.0F, 0.0F, false);
		side_5.setTextureOffset(459, 6).addBox(-28.0F, -33.0F, -50.6F, 56.0F, 4.0F, 6.0F, 0.0F, false);
		side_5.setTextureOffset(441, 93).addBox(-28.0F, -16.0F, -50.6F, 56.0F, 4.0F, 6.0F, 0.0F, false);
		side_5.setTextureOffset(288, 422).addBox(-28.0F, -32.0F, -49.0F, 56.0F, 16.0F, 4.0F, 0.0F, false);
		side_5.setTextureOffset(140, 392).addBox(-28.0F, -12.0F, -49.0F, 56.0F, 8.0F, 4.0F, 0.0F, false);

		top_panel_5 = new ModelRenderer(this);
		top_panel_5.setRotationPoint(0.0F, 0.0F, 0.0F);
		panel_side_5.addChild(top_panel_5);
		top_panel_5.setTextureOffset(262, 96).addBox(-24.0F, -180.2222F, -49.6667F, 48.0F, 48.0F, 4.0F, 0.0F, false);

		cage5 = new ModelRenderer(this);
		cage5.setRotationPoint(0.0F, 3.7778F, -1.6667F);
		top_panel_5.addChild(cage5);
		cage5.setTextureOffset(480, 38).addBox(-9.0F, -177.0F, -58.0F, 4.0F, 12.0F, 17.0F, 0.0F, false);
		cage5.setTextureOffset(480, 38).addBox(5.0F, -177.0F, -58.0F, 4.0F, 8.0F, 17.0F, 0.0F, false);
		cage5.setTextureOffset(480, 38).addBox(-17.0F, -169.0F, -58.6F, 11.0F, 4.0F, 18.0F, 0.0F, false);
		cage5.setTextureOffset(480, 38).addBox(-17.0F, -155.0F, -58.6F, 11.0F, 4.0F, 18.0F, 0.0F, false);
		cage5.setTextureOffset(480, 38).addBox(5.0F, -151.0F, -58.0F, 4.0F, 8.0F, 17.0F, 0.0F, false);
		cage5.setTextureOffset(480, 38).addBox(-9.0F, -151.0F, -58.0F, 4.0F, 8.0F, 17.0F, 0.0F, false);
		cage5.setTextureOffset(480, 38).addBox(6.0F, -155.0F, -58.6F, 11.0F, 4.0F, 18.0F, 0.0F, false);
		cage5.setTextureOffset(480, 38).addBox(6.0F, -169.0F, -58.6F, 11.0F, 4.0F, 18.0F, 0.0F, false);
		cage5.setTextureOffset(480, 38).addBox(-6.0F, -169.0F, -59.6F, 12.0F, 4.0F, 19.0F, 0.0F, false);
		cage5.setTextureOffset(480, 38).addBox(-6.0F, -155.0F, -59.6F, 12.0F, 4.0F, 19.0F, 0.0F, false);
		cage5.setTextureOffset(480, 38).addBox(-9.0F, -165.0F, -58.0F, 4.0F, 12.0F, 17.0F, 0.0F, false);
		cage5.setTextureOffset(480, 38).addBox(5.0F, -165.0F, -58.0F, 4.0F, 12.0F, 17.0F, 0.0F, false);
		cage5.setTextureOffset(480, 38).addBox(-16.0F, -176.0F, -52.0F, 32.0F, 32.0F, 10.0F, 0.0F, false);

		middle_panel_5 = new ModelRenderer(this);
		middle_panel_5.setRotationPoint(0.0F, 0.0F, 0.0F);
		panel_side_5.addChild(middle_panel_5);
		middle_panel_5.setTextureOffset(256, 105).addBox(-24.0F, -120.2222F, -49.6667F, 48.0F, 44.0F, 4.0F, 0.0F, false);
		middle_panel_5.setTextureOffset(452, 38).addBox(-18.0F, -114.2222F, -51.6667F, 36.0F, 32.0F, 4.0F, 0.0F, false);
		middle_panel_5.setTextureOffset(452, 38).addBox(13.0F, -113.2222F, -52.6667F, 4.0F, 4.0F, 4.0F, 0.0F, false);
		middle_panel_5.setTextureOffset(452, 38).addBox(-17.0F, -113.2222F, -52.6667F, 4.0F, 4.0F, 4.0F, 0.0F, false);
		middle_panel_5.setTextureOffset(452, 38).addBox(-17.0F, -87.2222F, -52.6667F, 4.0F, 4.0F, 4.0F, 0.0F, false);
		middle_panel_5.setTextureOffset(452, 38).addBox(13.0F, -87.2222F, -52.6667F, 4.0F, 4.0F, 4.0F, 0.0F, false);

		lower_panel_5 = new ModelRenderer(this);
		lower_panel_5.setRotationPoint(0.0F, 0.0F, 0.0F);
		panel_side_5.addChild(lower_panel_5);
		

		componets5 = new ModelRenderer(this);
		componets5.setRotationPoint(0.0F, 0.0F, 0.0F);
		lower_panel_5.addChild(componets5);
		componets5.setTextureOffset(505, 64).addBox(16.4F, -43.2222F, -46.0667F, 2.0F, 2.0F, 2.0F, 0.0F, false);
		componets5.setTextureOffset(505, 64).addBox(19.4F, -53.2222F, -46.0667F, 2.0F, 2.0F, 2.0F, 0.0F, false);
		componets5.setTextureOffset(505, 64).addBox(-4.0F, -46.4222F, -48.6667F, 6.0F, 12.0F, 4.0F, 0.0F, false);
		componets5.setTextureOffset(564, 148).addBox(16.2F, -53.2222F, -46.0667F, 2.0F, 2.0F, 2.0F, 0.0F, false);
		componets5.setTextureOffset(505, 64).addBox(19.4F, -50.2222F, -46.0667F, 2.0F, 2.0F, 2.0F, 0.0F, false);
		componets5.setTextureOffset(505, 64).addBox(16.2F, -50.2222F, -46.0667F, 2.0F, 2.0F, 2.0F, 0.0F, false);
		componets5.setTextureOffset(466, 144).addBox(16.4F, -40.2222F, -46.0667F, 2.0F, 2.0F, 2.0F, 0.0F, false);
		componets5.setTextureOffset(505, 64).addBox(16.4F, -37.2222F, -46.0667F, 2.0F, 2.0F, 2.0F, 0.0F, false);
		componets5.setTextureOffset(505, 64).addBox(16.2F, -47.2222F, -46.0667F, 2.0F, 2.0F, 2.0F, 0.0F, false);
		componets5.setTextureOffset(498, 203).addBox(19.4F, -47.2222F, -46.0667F, 2.0F, 2.0F, 2.0F, 0.0F, false);
		componets5.setTextureOffset(500, 278).addBox(-2.8F, -58.2222F, -47.6667F, 4.0F, 12.0F, 2.0F, 0.0F, false);
		componets5.setTextureOffset(514, 180).addBox(19.4F, -43.2222F, -46.0667F, 2.0F, 2.0F, 2.0F, 0.0F, false);
		componets5.setTextureOffset(505, 64).addBox(19.4F, -40.2222F, -46.0667F, 2.0F, 2.0F, 2.0F, 0.0F, false);
		componets5.setTextureOffset(505, 64).addBox(19.4F, -37.2222F, -46.0667F, 2.0F, 2.0F, 2.0F, 0.0F, false);
		componets5.setTextureOffset(489, 276).addBox(-10.4F, -58.2222F, -47.6667F, 4.0F, 12.0F, 2.0F, 0.0F, false);
		componets5.setTextureOffset(505, 64).addBox(-11.2F, -46.4222F, -48.6667F, 6.0F, 12.0F, 4.0F, 0.0F, false);
		componets5.setTextureOffset(493, 288).addBox(-17.6F, -58.2222F, -47.6667F, 4.0F, 12.0F, 2.0F, 0.0F, false);
		componets5.setTextureOffset(505, 64).addBox(-18.4F, -46.4222F, -48.6667F, 6.0F, 12.0F, 4.0F, 0.0F, false);
		componets5.setTextureOffset(505, 64).addBox(-4.0F, -58.4222F, -48.6667F, 6.0F, 4.0F, 4.0F, 0.0F, false);
		componets5.setTextureOffset(505, 64).addBox(-11.6F, -58.4222F, -48.6667F, 6.0F, 4.0F, 4.0F, 0.0F, false);
		componets5.setTextureOffset(505, 64).addBox(-18.8F, -58.4222F, -48.6667F, 6.0F, 4.0F, 4.0F, 0.0F, false);
		componets5.setTextureOffset(672, 166).addBox(-2.8F, -44.6222F, -49.6667F, 4.0F, 8.0F, 2.0F, 0.0F, false);
		componets5.setTextureOffset(676, 180).addBox(-10.0F, -44.6222F, -49.6667F, 4.0F, 8.0F, 2.0F, 0.0F, false);
		componets5.setTextureOffset(683, 160).addBox(-17.2F, -44.6222F, -49.6667F, 4.0F, 8.0F, 2.0F, 0.0F, false);
		componets5.setTextureOffset(505, 64).addBox(5.2F, -58.2222F, -47.6667F, 2.0F, 28.0F, 2.0F, 0.0F, false);
		componets5.setTextureOffset(505, 64).addBox(9.2F, -58.2222F, -47.6667F, 2.0F, 28.0F, 2.0F, 0.0F, false);

		panelbox9 = new ModelRenderer(this);
		panelbox9.setRotationPoint(0.0F, 0.0F, 0.0F);
		lower_panel_5.addChild(panelbox9);
		panelbox9.setTextureOffset(665, 36).addBox(-24.0F, -57.2222F, -45.2667F, 48.0F, 27.0F, 0.0F, 0.0F, false);
		panelbox9.setTextureOffset(646, 57).addBox(-23.0F, -57.2222F, -49.2667F, 46.0F, 2.0F, 4.0F, 0.0F, false);
		panelbox9.setTextureOffset(660, 66).addBox(-23.0F, -33.2222F, -49.2667F, 46.0F, 2.0F, 4.0F, 0.0F, false);

		panelbox10 = new ModelRenderer(this);
		panelbox10.setRotationPoint(0.0F, 0.0F, 0.0F);
		lower_panel_5.addChild(panelbox10);
		panelbox10.setTextureOffset(688, 45).addBox(-25.0F, -57.2222F, -49.2667F, 2.0F, 26.0F, 4.0F, 0.0F, false);
		panelbox10.setTextureOffset(692, 48).addBox(23.0F, -57.2222F, -49.2667F, 2.0F, 26.0F, 4.0F, 0.0F, false);

		floorboards_5 = new ModelRenderer(this);
		floorboards_5.setRotationPoint(0.0F, 0.0F, 0.0F);
		panel_side_5.addChild(floorboards_5);
		floorboards_5.setTextureOffset(249, 171).addBox(-32.4F, -0.2222F, -58.6667F, 64.0F, 4.0F, 4.0F, 0.0F, false);
		floorboards_5.setTextureOffset(249, 171).addBox(-26.0F, -0.2222F, -46.6667F, 52.0F, 4.0F, 4.0F, 0.0F, false);
		floorboards_5.setTextureOffset(249, 171).addBox(-24.0F, -0.2222F, -42.6667F, 48.0F, 4.0F, 4.0F, 0.0F, false);
		floorboards_5.setTextureOffset(249, 171).addBox(-22.0F, -0.2222F, -38.6667F, 44.0F, 4.0F, 4.0F, 0.0F, false);
		floorboards_5.setTextureOffset(249, 171).addBox(-18.0F, -0.2222F, -34.6667F, 36.0F, 4.0F, 4.0F, 0.0F, false);
		floorboards_5.setTextureOffset(249, 171).addBox(-16.0F, -0.2222F, -30.6667F, 32.0F, 4.0F, 4.0F, 0.0F, false);
		floorboards_5.setTextureOffset(249, 171).addBox(-14.0F, -0.2222F, -26.6667F, 28.0F, 4.0F, 4.0F, 0.0F, false);
		floorboards_5.setTextureOffset(249, 171).addBox(-12.0F, -0.2222F, -22.6667F, 24.0F, 4.0F, 4.0F, 0.0F, false);
		floorboards_5.setTextureOffset(249, 171).addBox(-10.0F, -0.2222F, -18.6667F, 20.0F, 4.0F, 4.0F, 0.0F, false);
		floorboards_5.setTextureOffset(249, 171).addBox(-8.0F, -0.2222F, -14.6667F, 16.0F, 4.0F, 4.0F, 0.0F, false);
		floorboards_5.setTextureOffset(249, 171).addBox(-6.0F, -0.2222F, -10.6667F, 12.0F, 4.0F, 4.0F, 0.0F, false);
		floorboards_5.setTextureOffset(249, 171).addBox(-4.0F, -0.2222F, -6.6667F, 8.0F, 4.0F, 4.0F, 0.0F, false);
		floorboards_5.setTextureOffset(249, 171).addBox(-30.8F, -0.2222F, -54.6667F, 60.0F, 4.0F, 4.0F, 0.0F, false);
		floorboards_5.setTextureOffset(249, 171).addBox(-28.0F, -0.2222F, -50.6667F, 56.0F, 4.0F, 4.0F, 0.0F, false);

		hexspine = new ModelRenderer(this);
		hexspine.setRotationPoint(0.0F, -2.0F, 2.0F);
		box.addChild(hexspine);
		

		crossspine = new ModelRenderer(this);
		crossspine.setRotationPoint(0.0F, 0.0F, 0.0F);
		hexspine.addChild(crossspine);
		crossspine.setTextureOffset(59, 398).addBox(-68.0F, -3.0F, -2.0F, 136.0F, 6.0F, 4.0F, 0.0F, false);
		crossspine.setTextureOffset(239, 212).addBox(-62.0F, -190.0F, -2.0F, 8.0F, 188.0F, 4.0F, 0.0F, false);
		crossspine.setTextureOffset(161, 218).addBox(54.0F, -190.0F, -2.0F, 8.0F, 188.0F, 4.0F, 0.0F, false);
		crossspine.setTextureOffset(491, 304).addBox(-65.0F, -218.0F, -2.0F, 4.0F, 24.0F, 4.0F, 0.0F, false);
		crossspine.setTextureOffset(466, 294).addBox(61.0F, -218.0F, -2.0F, 4.0F, 24.0F, 4.0F, 0.0F, false);

		leftspine = new ModelRenderer(this);
		leftspine.setRotationPoint(0.25F, -151.8125F, 0.0F);
		hexspine.addChild(leftspine);
		setRotationAngle(leftspine, 0.0F, 1.0472F, 0.0F);
		leftspine.setTextureOffset(59, 397).addBox(-68.25F, 148.8125F, -2.0F, 136.0F, 6.0F, 4.0F, 0.0F, false);
		leftspine.setTextureOffset(61, 214).addBox(53.75F, -38.1875F, -2.0F, 8.0F, 188.0F, 4.0F, 0.0F, false);
		leftspine.setTextureOffset(330, 218).addBox(-62.25F, -38.1875F, -2.0F, 8.0F, 188.0F, 4.0F, 0.0F, false);
		leftspine.setTextureOffset(468, 301).addBox(-65.25F, -66.1875F, -2.0F, 4.0F, 24.0F, 4.0F, 0.0F, false);
		leftspine.setTextureOffset(498, 297).addBox(60.75F, -66.1875F, -2.0F, 4.0F, 24.0F, 4.0F, 0.0F, false);

		upperrails = new ModelRenderer(this);
		upperrails.setRotationPoint(0.0F, 0.0F, 0.0F);
		leftspine.addChild(upperrails);
		

		upperrail_1 = new ModelRenderer(this);
		upperrail_1.setRotationPoint(10.0F, -52.0F, 0.0F);
		upperrails.addChild(upperrail_1);
		setRotationAngle(upperrail_1, 0.0F, 0.0F, 0.0873F);
		upperrail_1.setTextureOffset(173, 306).addBox(-5.85F, -2.5875F, -2.0F, 36.0F, 8.0F, 4.0F, 0.0F, false);
		upperrail_1.setTextureOffset(189, 310).addBox(-5.85F, 4.8125F, -2.0F, 64.0F, 3.0F, 4.0F, 0.0F, false);
		upperrail_1.setTextureOffset(189, 310).addBox(-5.85F, 6.8125F, -2.0F, 62.0F, 2.0F, 4.0F, 0.0F, false);
		upperrail_1.setTextureOffset(189, 310).addBox(-5.85F, 8.8125F, -2.0F, 60.0F, 2.0F, 4.0F, 0.0F, false);
		upperrail_1.setTextureOffset(692, 180).addBox(-4.85F, -8.5875F, -2.0F, 8.0F, 6.0F, 4.0F, 0.0F, false);
		upperrail_1.setTextureOffset(708, 54).addBox(3.15F, -9.5875F, -3.0F, 2.0F, 7.0F, 6.0F, 0.0F, false);

		upperrails2 = new ModelRenderer(this);
		upperrails2.setRotationPoint(0.0F, 0.0F, 0.0F);
		leftspine.addChild(upperrails2);
		setRotationAngle(upperrails2, 0.0F, 1.0472F, 0.0F);
		

		upperrail_2 = new ModelRenderer(this);
		upperrail_2.setRotationPoint(10.0F, -52.0F, 0.0F);
		upperrails2.addChild(upperrail_2);
		setRotationAngle(upperrail_2, 0.0F, 0.0F, 0.0873F);
		upperrail_2.setTextureOffset(228, 288).addBox(-5.85F, -2.5875F, -2.0F, 36.0F, 8.0F, 4.0F, 0.0F, false);
		upperrail_2.setTextureOffset(178, 240).addBox(-5.85F, 4.8125F, -2.0F, 64.0F, 2.0F, 4.0F, 0.0F, false);
		upperrail_2.setTextureOffset(262, 240).addBox(-5.85F, 6.8125F, -2.0F, 62.0F, 2.0F, 4.0F, 0.0F, false);
		upperrail_2.setTextureOffset(278, 292).addBox(-5.85F, 8.8125F, -2.0F, 60.0F, 2.0F, 4.0F, 0.0F, false);
		upperrail_2.setTextureOffset(688, 171).addBox(-3.85F, -8.5875F, -2.0F, 8.0F, 6.0F, 4.0F, 0.0F, false);
		upperrail_2.setTextureOffset(662, 38).addBox(4.15F, -9.5875F, -3.0F, 2.0F, 7.0F, 6.0F, 0.0F, false);

		upperrails3 = new ModelRenderer(this);
		upperrails3.setRotationPoint(0.0F, 0.0F, 0.0F);
		leftspine.addChild(upperrails3);
		setRotationAngle(upperrails3, 0.0F, 2.0944F, 0.0F);
		

		upperrail_3 = new ModelRenderer(this);
		upperrail_3.setRotationPoint(10.0F, -52.0F, 0.0F);
		upperrails3.addChild(upperrail_3);
		setRotationAngle(upperrail_3, 0.0F, 0.0F, 0.0873F);
		upperrail_3.setTextureOffset(180, 292).addBox(-5.85F, -2.5875F, -2.0F, 36.0F, 8.0F, 4.0F, 0.0F, false);
		upperrail_3.setTextureOffset(238, 228).addBox(-5.85F, 4.8125F, -2.0F, 64.0F, 2.0F, 4.0F, 0.0F, false);
		upperrail_3.setTextureOffset(258, 234).addBox(-5.85F, 6.8125F, -2.0F, 62.0F, 2.0F, 4.0F, 0.0F, false);
		upperrail_3.setTextureOffset(278, 286).addBox(-5.85F, 8.8125F, -2.0F, 60.0F, 2.0F, 4.0F, 0.0F, false);
		upperrail_3.setTextureOffset(699, 162).addBox(-4.85F, -8.5875F, -2.0F, 8.0F, 6.0F, 4.0F, 0.0F, false);
		upperrail_3.setTextureOffset(688, 57).addBox(3.15F, -9.5875F, -3.0F, 2.0F, 7.0F, 6.0F, 0.0F, false);

		upperrails4 = new ModelRenderer(this);
		upperrails4.setRotationPoint(0.0F, 0.0F, 0.0F);
		leftspine.addChild(upperrails4);
		setRotationAngle(upperrails4, 0.0F, 3.1416F, 0.0F);
		

		upperrail_4 = new ModelRenderer(this);
		upperrail_4.setRotationPoint(10.0F, -52.0F, 0.0F);
		upperrails4.addChild(upperrail_4);
		setRotationAngle(upperrail_4, 0.0F, 0.0F, 0.0873F);
		upperrail_4.setTextureOffset(697, 171).addBox(-4.85F, -8.5875F, -2.0F, 8.0F, 6.0F, 4.0F, 0.0F, false);
		upperrail_4.setTextureOffset(54, 267).addBox(-5.85F, -2.5875F, -2.0F, 36.0F, 8.0F, 4.0F, 0.0F, false);
		upperrail_4.setTextureOffset(238, 222).addBox(-5.85F, 4.8125F, -2.0F, 64.0F, 2.0F, 4.0F, 0.0F, false);
		upperrail_4.setTextureOffset(250, 106).addBox(-5.85F, 6.8125F, -2.0F, 62.0F, 2.0F, 4.0F, 0.0F, false);
		upperrail_4.setTextureOffset(278, 272).addBox(-5.85F, 8.8125F, -2.0F, 60.0F, 2.0F, 4.0F, 0.0F, false);
		upperrail_4.setTextureOffset(704, 57).addBox(3.15F, -9.5875F, -3.0F, 2.0F, 7.0F, 6.0F, 0.0F, false);

		upperrails5 = new ModelRenderer(this);
		upperrails5.setRotationPoint(0.0F, 0.0F, 0.0F);
		leftspine.addChild(upperrails5);
		setRotationAngle(upperrails5, 0.0F, -2.0944F, 0.0F);
		

		upperrail_5 = new ModelRenderer(this);
		upperrail_5.setRotationPoint(10.0F, -52.0F, 0.0F);
		upperrails5.addChild(upperrail_5);
		setRotationAngle(upperrail_5, 0.0F, 0.0F, 0.0873F);
		upperrail_5.setTextureOffset(690, 182).addBox(-4.85F, -8.5875F, -2.0F, 8.0F, 6.0F, 4.0F, 0.0F, false);
		upperrail_5.setTextureOffset(59, 301).addBox(-5.85F, -2.5875F, -2.0F, 36.0F, 8.0F, 4.0F, 0.0F, false);
		upperrail_5.setTextureOffset(238, 216).addBox(-5.85F, 4.8125F, -2.0F, 64.0F, 2.0F, 4.0F, 0.0F, false);
		upperrail_5.setTextureOffset(250, 100).addBox(-5.85F, 6.8125F, -2.0F, 62.0F, 2.0F, 4.0F, 0.0F, false);
		upperrail_5.setTextureOffset(278, 266).addBox(-5.85F, 8.8125F, -2.0F, 60.0F, 2.0F, 4.0F, 0.0F, false);
		upperrail_5.setTextureOffset(692, 48).addBox(3.15F, -9.5875F, -3.0F, 2.0F, 7.0F, 6.0F, 0.0F, false);

		upperrails6 = new ModelRenderer(this);
		upperrails6.setRotationPoint(0.0F, 0.0F, 0.0F);
		leftspine.addChild(upperrails6);
		setRotationAngle(upperrails6, 0.0F, -1.0472F, 0.0F);
		

		upperrail_6 = new ModelRenderer(this);
		upperrail_6.setRotationPoint(10.0F, -52.0F, 0.0F);
		upperrails6.addChild(upperrail_6);
		setRotationAngle(upperrail_6, 0.0F, 0.0F, 0.0873F);
		upperrail_6.setTextureOffset(704, 178).addBox(-4.85F, -8.5875F, -2.0F, 8.0F, 6.0F, 4.0F, 0.0F, false);
		upperrail_6.setTextureOffset(114, 354).addBox(-5.85F, -2.5875F, -2.0F, 36.0F, 8.0F, 4.0F, 0.0F, false);
		upperrail_6.setTextureOffset(114, 354).addBox(-5.85F, 4.8125F, -2.0F, 64.0F, 2.0F, 4.0F, 0.0F, false);
		upperrail_6.setTextureOffset(114, 354).addBox(-5.85F, 6.8125F, -2.0F, 62.0F, 2.0F, 4.0F, 0.0F, false);
		upperrail_6.setTextureOffset(114, 354).addBox(-5.85F, 8.8125F, -2.0F, 60.0F, 2.0F, 4.0F, 0.0F, false);
		upperrail_6.setTextureOffset(678, 45).addBox(3.15F, -9.5875F, -3.0F, 2.0F, 7.0F, 6.0F, 0.0F, false);

		rightspine = new ModelRenderer(this);
		rightspine.setRotationPoint(0.0F, 0.0F, 0.0F);
		hexspine.addChild(rightspine);
		setRotationAngle(rightspine, 0.0F, -1.0472F, 0.0F);
		rightspine.setTextureOffset(39, 398).addBox(-68.0F, -3.0F, -2.0F, 136.0F, 6.0F, 4.0F, 0.0F, false);
		rightspine.setTextureOffset(14, 214).addBox(-62.0F, -190.0F, -2.0F, 8.0F, 188.0F, 4.0F, 0.0F, false);
		rightspine.setTextureOffset(189, 218).addBox(54.0F, -190.0F, -2.0F, 8.0F, 188.0F, 4.0F, 0.0F, false);
		rightspine.setTextureOffset(470, 304).addBox(-65.0F, -218.0F, -2.0F, 4.0F, 24.0F, 4.0F, 0.0F, false);
		rightspine.setTextureOffset(560, 306).addBox(61.0F, -218.0F, -2.0F, 4.0F, 24.0F, 4.0F, 0.0F, false);
	}

	@Override
	public void setRotationAngles(Entity entity, float limbSwing, float limbSwingAmount, float ageInTicks, float netHeadYaw, float headPitch){
		//previously the render function, render code was moved to a method below
	}

	@Override
	public void render(MatrixStack matrixStack, IVertexBuilder buffer, int packedLight, int packedOverlay, float red, float green, float blue, float alpha){
		glow.render(matrixStack, buffer, packedLight, packedOverlay);
		door_rotate_y.render(matrixStack, buffer, packedLight, packedOverlay);
		boti.render(matrixStack, buffer, packedLight, packedOverlay);
		box.render(matrixStack, buffer, packedLight, packedOverlay);
	}
	
	public void render(MatrixStack matrixStack, IVertexBuilder buffer, int packedLight, int packedOverlay){
		glow.render(matrixStack, buffer, packedLight, packedOverlay);
		door_rotate_y.render(matrixStack, buffer, packedLight, packedOverlay);
		boti.render(matrixStack, buffer, packedLight, packedOverlay);
		box.render(matrixStack, buffer, packedLight, packedOverlay);
	}

	public void setRotationAngle(ModelRenderer modelRenderer, float x, float y, float z) {
		modelRenderer.rotateAngleX = x;
		modelRenderer.rotateAngleY = y;
		modelRenderer.rotateAngleZ = z;
	}

	@Override
	public void render(ExteriorTile tile, float scale, MatrixStack matrixStack, IVertexBuilder buffer, int packedLight, int packedOverlay, float alpha) {
		door_rotate_y.rotateAngleY = (float) Math.toRadians(tile.getOpen() == EnumDoorState.ONE ? 55 : (tile.getOpen() == EnumDoorState.BOTH ? 85 : 0));
        door_rotate_y.render(matrixStack, buffer, packedLight, packedOverlay,1,1,1, alpha);
        box.render(matrixStack, buffer, packedLight, packedOverlay,1,1,1, alpha);
        glow.setBright(tile.getLightLevel());
        glow.render(matrixStack, buffer, packedLight, packedOverlay,1,1,1, alpha);
        
        PortalInfo info = new PortalInfo();
        info.setPosition(tile.getPos());
        info.setTranslate(matrix -> {
        	ExteriorRenderer.applyTransforms(matrix, tile);
        });
        info.setTranslatePortal(matrix -> {
        	matrix.translate(0, -2.06, 0);
        });


        
	}
	
	public void renderBroken(MatrixStack matrix, IVertexBuilder vertex, int light, int overlay) {
		matrix.translate(0, 0.125, 0);
		matrix.scale(0.25F, 0.25F, 0.25F);
		this.box.render(matrix, vertex, light, overlay);
		this.door_rotate_y.rotateAngleY = 0;
		this.door_rotate_y.render(matrix, vertex, light, overlay);
		ClientWorld world = Minecraft.getInstance().world;
		float flickeringLight = 0;
		if(world.rand.nextBoolean()){
			flickeringLight = world.rand.nextFloat();
		}
		glow.setBright((float)Math.cos(flickeringLight / 0.3 * world.rand.nextFloat()));
		glow.render(matrix, vertex, light, overlay);
	}
}