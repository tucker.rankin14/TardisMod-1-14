package net.tardis.mod.client.models;

import com.mojang.blaze3d.matrix.MatrixStack;
import com.mojang.blaze3d.vertex.IVertexBuilder;

import net.minecraft.tileentity.TileEntity;
import net.tardis.mod.tileentities.ConsoleTile;

public interface TileModel<T extends TileEntity> {

     void render(T tileEntity, float scale, MatrixStack matrixStack, IVertexBuilder buffer, int packedLight, int packedOverlay, float red, float green, float blue, float alpha);
}
