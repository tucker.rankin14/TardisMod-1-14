package net.tardis.mod.client.models.exteriors;
// Made with Blockbench 3.7.4
// Exported for Minecraft version 1.15
// Paste this class into your mod and generate all required imports

import com.mojang.blaze3d.matrix.MatrixStack;
import com.mojang.blaze3d.vertex.IVertexBuilder;

import net.minecraft.client.renderer.model.ModelRenderer;
import net.minecraft.entity.Entity;
import net.minecraft.util.math.vector.Vector3d;
import net.tardis.mod.enums.EnumDoorState;
import net.tardis.mod.misc.IDoorType.EnumDoorType;
import net.tardis.mod.tileentities.exteriors.ExteriorTile;

public class TTCapsuleExteriorModel extends ExteriorModel {
	private final ModelRenderer right_door_rotate_90;
	private final ModelRenderer left_door_rotate_90;
	private final ModelRenderer base;
	private final ModelRenderer bone2;
	private final ModelRenderer bone3;
	private final ModelRenderer bone4;
	private final ModelRenderer walls;
	private final ModelRenderer bone22;
	private final ModelRenderer bone8;
	private final ModelRenderer bone7;
	private final ModelRenderer roof;
	private final ModelRenderer bone;
	private final ModelRenderer bone5;
	private final ModelRenderer bone6;
	private final ModelRenderer bb_main;

	public TTCapsuleExteriorModel() {
		textureWidth = 128;
		textureHeight = 128;

		right_door_rotate_90 = new ModelRenderer(this);
		right_door_rotate_90.setRotationPoint(-6.0F, 6.5F, -5.0F);
		right_door_rotate_90.setTextureOffset(14, 35).addBox(0.0F, -14.5F, -0.525F, 6.0F, 29.0F, 1.0F, 0.0F, false);

		left_door_rotate_90 = new ModelRenderer(this);
		left_door_rotate_90.setRotationPoint(6.0F, 6.5F, -5.0F);
		left_door_rotate_90.setTextureOffset(0, 30).addBox(-6.0F, -14.5F, -0.525F, 6.0F, 29.0F, 1.0F, 0.0F, false);

		base = new ModelRenderer(this);
		base.setRotationPoint(0.0F, 24.0F, 0.0F);
		base.setTextureOffset(26, 0).addBox(-3.0F, -3.0F, 0.0F, 6.0F, 3.0F, 9.0F, 0.0F, false);
		base.setTextureOffset(38, 25).addBox(-5.0F, -3.0F, 7.0F, 2.0F, 3.0F, 1.0F, 0.0F, false);
		base.setTextureOffset(58, 47).addBox(-7.0F, -3.0F, 3.0F, 4.0F, 3.0F, 4.0F, 0.0F, false);
		base.setTextureOffset(38, 21).addBox(3.0F, -3.0F, 7.0F, 2.0F, 3.0F, 1.0F, 0.0F, false);

		bone2 = new ModelRenderer(this);
		bone2.setRotationPoint(0.0F, 0.0F, 0.0F);
		base.addChild(bone2);
		setRotationAngle(bone2, 0.0F, -1.5708F, 0.0F);
		bone2.setTextureOffset(26, 12).addBox(-3.0F, -3.0F, 3.0F, 6.0F, 3.0F, 6.0F, 0.0F, false);
		bone2.setTextureOffset(38, 25).addBox(-5.0F, -3.0F, 7.0F, 2.0F, 3.0F, 1.0F, 0.0F, false);
		bone2.setTextureOffset(58, 47).addBox(-7.0F, -3.0F, 3.0F, 4.0F, 3.0F, 4.0F, 0.0F, false);
		bone2.setTextureOffset(38, 21).addBox(3.0F, -3.0F, 7.0F, 2.0F, 3.0F, 1.0F, 0.0F, false);

		bone3 = new ModelRenderer(this);
		bone3.setRotationPoint(0.0F, 0.0F, 0.0F);
		bone2.addChild(bone3);
		setRotationAngle(bone3, 0.0F, -1.5708F, 0.0F);
		bone3.setTextureOffset(26, 0).addBox(-3.0F, -3.0F, 0.0F, 6.0F, 3.0F, 9.0F, 0.0F, false);
		bone3.setTextureOffset(38, 25).addBox(-5.0F, -3.0F, 7.0F, 2.0F, 3.0F, 1.0F, 0.0F, false);
		bone3.setTextureOffset(58, 47).addBox(-7.0F, -3.0F, 3.0F, 4.0F, 3.0F, 4.0F, 0.0F, false);
		bone3.setTextureOffset(38, 21).addBox(3.0F, -3.0F, 7.0F, 2.0F, 3.0F, 1.0F, 0.0F, false);

		bone4 = new ModelRenderer(this);
		bone4.setRotationPoint(0.0F, 0.0F, 0.0F);
		bone3.addChild(bone4);
		setRotationAngle(bone4, 0.0F, -1.5708F, 0.0F);
		bone4.setTextureOffset(26, 12).addBox(-3.0F, -3.0F, 3.0F, 6.0F, 3.0F, 6.0F, 0.0F, false);
		bone4.setTextureOffset(38, 25).addBox(-5.0F, -3.0F, 7.0F, 2.0F, 3.0F, 1.0F, 0.0F, false);
		bone4.setTextureOffset(58, 47).addBox(-7.0F, -3.0F, 3.0F, 4.0F, 3.0F, 4.0F, 0.0F, false);
		bone4.setTextureOffset(38, 21).addBox(3.0F, -3.0F, 7.0F, 2.0F, 3.0F, 1.0F, 0.0F, false);

		walls = new ModelRenderer(this);
		walls.setRotationPoint(0.0F, 24.0F, 0.0F);
		

		bone22 = new ModelRenderer(this);
		bone22.setRotationPoint(3.0F, -3.0F, 9.0F);
		walls.addChild(bone22);
		bone22.setTextureOffset(40, 40).addBox(-5.5F, -29.0F, -1.5F, 5.0F, 29.0F, 1.0F, 0.0F, false);
		bone22.setTextureOffset(0, 60).addBox(-7.5F, -29.0F, -2.5F, 2.0F, 29.0F, 1.0F, 0.0F, false);
		bone22.setTextureOffset(52, 52).addBox(1.5F, -29.0F, -4.5F, 2.0F, 29.0F, 2.0F, 0.0F, false);
		bone22.setTextureOffset(52, 52).addBox(-9.5F, -29.0F, -4.5F, 2.0F, 29.0F, 2.0F, 0.0F, true);
		bone22.setTextureOffset(52, 12).addBox(-0.5F, -29.0F, -2.5F, 2.0F, 29.0F, 1.0F, 0.0F, false);

		bone8 = new ModelRenderer(this);
		bone8.setRotationPoint(-3.0F, 0.0F, -9.0F);
		bone22.addChild(bone8);
		setRotationAngle(bone8, 0.0F, -1.5708F, 0.0F);
		bone8.setTextureOffset(28, 35).addBox(-2.5F, -29.0F, 7.5F, 5.0F, 29.0F, 1.0F, 0.0F, false);
		bone8.setTextureOffset(0, 60).addBox(-4.5F, -29.0F, 6.5F, 2.0F, 29.0F, 1.0F, 0.0F, false);
		bone8.setTextureOffset(58, 0).addBox(-6.5F, -29.0F, 5.5F, 2.0F, 29.0F, 1.0F, 0.0F, false);
		bone8.setTextureOffset(52, 12).addBox(2.5F, -29.0F, 6.5F, 2.0F, 29.0F, 1.0F, 0.0F, false);

		bone7 = new ModelRenderer(this);
		bone7.setRotationPoint(-3.0F, 0.0F, -9.0F);
		bone22.addChild(bone7);
		setRotationAngle(bone7, 0.0F, 1.5708F, 0.0F);
		bone7.setTextureOffset(28, 35).addBox(-2.5F, -29.0F, 7.5F, 5.0F, 29.0F, 1.0F, 0.0F, true);
		bone7.setTextureOffset(0, 60).addBox(2.5F, -29.0F, 6.5F, 2.0F, 29.0F, 1.0F, 0.0F, true);
		bone7.setTextureOffset(58, 0).addBox(4.5F, -29.0F, 5.5F, 2.0F, 29.0F, 1.0F, 0.0F, true);
		bone7.setTextureOffset(52, 12).addBox(-4.5F, -29.0F, 6.5F, 2.0F, 29.0F, 1.0F, 0.0F, true);

		roof = new ModelRenderer(this);
		roof.setRotationPoint(0.0F, -8.0F, 0.0F);
		roof.setTextureOffset(17, 21).addBox(-3.0F, -5.0F, 0.0F, 6.0F, 5.0F, 9.0F, 0.0F, false);
		roof.setTextureOffset(26, 12).addBox(-5.0F, -5.0F, 7.0F, 2.0F, 5.0F, 1.0F, 0.0F, false);
		roof.setTextureOffset(60, 60).addBox(-7.0F, -5.0F, 3.0F, 4.0F, 5.0F, 4.0F, 0.0F, false);
		roof.setTextureOffset(26, 0).addBox(3.0F, -5.0F, 7.0F, 2.0F, 5.0F, 1.0F, 0.0F, false);

		bone = new ModelRenderer(this);
		bone.setRotationPoint(0.0F, 0.0F, 0.0F);
		roof.addChild(bone);
		setRotationAngle(bone, 0.0F, -1.5708F, 0.0F);
		bone.setTextureOffset(52, 36).addBox(-3.0F, -5.0F, 3.0F, 6.0F, 5.0F, 6.0F, 0.0F, false);
		bone.setTextureOffset(26, 12).addBox(-5.0F, -5.0F, 7.0F, 2.0F, 5.0F, 1.0F, 0.0F, false);
		bone.setTextureOffset(60, 60).addBox(-7.0F, -5.0F, 3.0F, 4.0F, 5.0F, 4.0F, 0.0F, false);
		bone.setTextureOffset(26, 0).addBox(3.0F, -5.0F, 7.0F, 2.0F, 5.0F, 1.0F, 0.0F, false);

		bone5 = new ModelRenderer(this);
		bone5.setRotationPoint(0.0F, 0.0F, 0.0F);
		bone.addChild(bone5);
		setRotationAngle(bone5, 0.0F, -1.5708F, 0.0F);
		bone5.setTextureOffset(17, 21).addBox(-3.0F, -5.0F, 0.0F, 6.0F, 5.0F, 9.0F, 0.0F, false);
		bone5.setTextureOffset(26, 12).addBox(-5.0F, -5.0F, 7.0F, 2.0F, 5.0F, 1.0F, 0.0F, false);
		bone5.setTextureOffset(60, 60).addBox(-7.0F, -5.0F, 3.0F, 4.0F, 5.0F, 4.0F, 0.0F, false);
		bone5.setTextureOffset(26, 0).addBox(3.0F, -5.0F, 7.0F, 2.0F, 5.0F, 1.0F, 0.0F, false);

		bone6 = new ModelRenderer(this);
		bone6.setRotationPoint(0.0F, 0.0F, 0.0F);
		bone5.addChild(bone6);
		setRotationAngle(bone6, 0.0F, -1.5708F, 0.0F);
		bone6.setTextureOffset(52, 36).addBox(-3.0F, -5.0F, 3.0F, 6.0F, 5.0F, 6.0F, 0.0F, false);
		bone6.setTextureOffset(26, 12).addBox(-5.0F, -5.0F, 7.0F, 2.0F, 5.0F, 1.0F, 0.0F, false);
		bone6.setTextureOffset(60, 60).addBox(-7.0F, -5.0F, 3.0F, 4.0F, 5.0F, 4.0F, 0.0F, false);
		bone6.setTextureOffset(26, 0).addBox(3.0F, -5.0F, 7.0F, 2.0F, 5.0F, 1.0F, 0.0F, false);

		bb_main = new ModelRenderer(this);
		bb_main.setRotationPoint(0.0F, 24.0F, 0.0F);
		bb_main.setTextureOffset(0, 0).addBox(-6.0F, -32.0F, -3.525F, 12.0F, 29.0F, 1.0F, 0.0F, false);
	}

	@Override
	public void setRotationAngles(Entity entity, float limbSwing, float limbSwingAmount, float ageInTicks, float netHeadYaw, float headPitch){
		//previously the render function, render code was moved to a method below
	}

	@Override
	public void render(MatrixStack matrixStack, IVertexBuilder buffer, int packedLight, int packedOverlay, float red, float green, float blue, float alpha){
		
	}

	public void setRotationAngle(ModelRenderer modelRenderer, float x, float y, float z) {
		modelRenderer.rotateAngleX = x;
		modelRenderer.rotateAngleY = y;
		modelRenderer.rotateAngleZ = z;
	}

	@Override
	public void render(ExteriorTile exterior, float scale, MatrixStack matrixStack, IVertexBuilder buffer, int packedLight, int packedOverlay, float alpha) {
		this.left_door_rotate_90.rotateAngleY = this.right_door_rotate_90.rotateAngleY = 0;
        float rot = -(float) Math.toRadians(EnumDoorType.TT_CAPSULE.getRotationForState(exterior.getOpen()));

        if (exterior.getOpen() == EnumDoorState.ONE) {
            this.right_door_rotate_90.rotateAngleY = -rot;
            this.left_door_rotate_90.rotateAngleY = 0;
        } else {
            this.left_door_rotate_90.rotateAngleY = rot;
            this.right_door_rotate_90.rotateAngleY = -rot;
        }

        right_door_rotate_90.render(matrixStack, buffer, packedLight, packedOverlay);
		left_door_rotate_90.render(matrixStack, buffer, packedLight, packedOverlay);
		base.render(matrixStack, buffer, packedLight, packedOverlay);
		walls.render(matrixStack, buffer, packedLight, packedOverlay);
		roof.render(matrixStack, buffer, packedLight, packedOverlay);
		bb_main.render(matrixStack, buffer, packedLight, packedOverlay);
	}
}