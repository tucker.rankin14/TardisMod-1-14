package net.tardis.mod.client.models.interiordoors;
// Made with Blockbench 3.7.4
// Exported for Minecraft version 1.15
// Paste this class into your mod and generate all required imports

import com.mojang.blaze3d.matrix.MatrixStack;
import com.mojang.blaze3d.vertex.IVertexBuilder;

import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.RenderType;
import net.minecraft.client.renderer.entity.model.EntityModel;
import net.minecraft.client.renderer.model.ModelRenderer;
import net.minecraft.entity.Entity;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.math.vector.Vector3f;
import net.tardis.mod.cap.Capabilities;
import net.tardis.mod.client.renderers.DoorRenderer;
import net.tardis.mod.client.renderers.boti.BOTIRenderer;
import net.tardis.mod.client.renderers.boti.PortalInfo;
import net.tardis.mod.client.renderers.exteriors.ClockExteriorRenderer;
import net.tardis.mod.entity.DoorEntity;
import net.tardis.mod.helper.WorldHelper;
import net.tardis.mod.misc.IDoorType.EnumDoorType;

public class GrandfatherClockInteriorModel extends EntityModel<Entity> implements IInteriorDoorRenderer {
	private final ModelRenderer boti;
	private final ModelRenderer door_rotate_y;
	private final ModelRenderer pendulum_rotate_z;
	private final ModelRenderer disk;
	private final ModelRenderer pendulum_backwall;
	private final ModelRenderer pully;
	private final ModelRenderer pully2;
	private final ModelRenderer door_frame;
	private final ModelRenderer door_latch;
	private final ModelRenderer frame;
	private final ModelRenderer top;
	private final ModelRenderer posts;
	private final ModelRenderer foot1;
	private final ModelRenderer foot2;
	private final ModelRenderer foot3;
	private final ModelRenderer foot4;
	private final ModelRenderer torso;
	private final ModelRenderer side_details;
	private final ModelRenderer side_details2;
	private final ModelRenderer back_details;
	private final ModelRenderer base;
	private final ModelRenderer clockworks;
	private final ModelRenderer small_cog_1;
	private final ModelRenderer small_cog_2;
	private final ModelRenderer large_cog_1;
	private final ModelRenderer med_cog_1;
	private final ModelRenderer center_gear;
	private final ModelRenderer bone;
	private final ModelRenderer bone2;
	private final ModelRenderer bone3;
	private final ModelRenderer bone4;
	private final ModelRenderer bone5;
	private final ModelRenderer bone6;
	private final ModelRenderer bone7;
	private final ModelRenderer bone8;
	private final ModelRenderer bone9;
	private final ModelRenderer center_frame;
	private final ModelRenderer bone10;
	private final ModelRenderer bone11;
	private final ModelRenderer bone12;
	private final ModelRenderer bone13;

	public GrandfatherClockInteriorModel() {
		textureWidth = 512;
		textureHeight = 512;

		boti = new ModelRenderer(this);
		boti.setRotationPoint(0.0F, 24.0F, -13.0F);
		boti.setTextureOffset(408, 195).addBox(-21.0F, -124.0F, 4.0F, 42.0F, 101.0F, 4.0F, 0.0F, false);

		door_rotate_y = new ModelRenderer(this);
		door_rotate_y.setRotationPoint(20.0F, -4.0F, -5.0F);
		

		pendulum_rotate_z = new ModelRenderer(this);
		pendulum_rotate_z.setRotationPoint(-20.0F, -88.0F, 5.0F);
		door_rotate_y.addChild(pendulum_rotate_z);
		pendulum_rotate_z.setTextureOffset(194, 185).addBox(-2.0F, 7.0F, 0.0F, 4.0F, 56.0F, 1.0F, 0.0F, false);

		disk = new ModelRenderer(this);
		disk.setRotationPoint(0.0F, 69.0F, 0.0F);
		pendulum_rotate_z.addChild(disk);
		setRotationAngle(disk, 0.0F, 0.0F, -0.7854F);
		disk.setTextureOffset(11, 184).addBox(-6.0F, -6.0F, -1.0F, 12.0F, 12.0F, 2.0F, 0.0F, false);

		pendulum_backwall = new ModelRenderer(this);
		pendulum_backwall.setRotationPoint(-20.0F, 28.0F, 5.0F);
		door_rotate_y.addChild(pendulum_backwall);
		pendulum_backwall.setTextureOffset(75, 190).addBox(-16.0F, -109.0F, 2.0F, 32.0F, 84.0F, 0.0F, 0.0F, false);

		pully = new ModelRenderer(this);
		pully.setRotationPoint(-4.0F, 0.0F, 5.0F);
		door_rotate_y.addChild(pully);
		pully.setTextureOffset(198, 195).addBox(-24.0F, -81.0F, 0.0F, 2.0F, 12.0F, 1.0F, 0.0F, false);
		pully.setTextureOffset(19, 192).addBox(-25.0F, -69.0F, -1.0F, 4.0F, 12.0F, 2.0F, 0.0F, false);

		pully2 = new ModelRenderer(this);
		pully2.setRotationPoint(-4.0F, 0.0F, 5.0F);
		door_rotate_y.addChild(pully2);
		pully2.setTextureOffset(198, 196).addBox(-10.0F, -81.0F, 0.0F, 2.0F, 24.0F, 1.0F, 0.0F, false);

		door_frame = new ModelRenderer(this);
		door_frame.setRotationPoint(-20.0F, 28.0F, 5.0F);
		door_rotate_y.addChild(door_frame);
		door_frame.setTextureOffset(149, 90).addBox(-16.0F, -105.0F, -4.0F, 4.0F, 72.0F, 3.0F, 0.0F, false);
		door_frame.setTextureOffset(143, 49).addBox(8.0F, -161.0F, -5.0F, 8.0F, 4.0F, 8.0F, 0.0F, false);
		door_frame.setTextureOffset(143, 49).addBox(-16.0F, -161.0F, -5.0F, 8.0F, 4.0F, 8.0F, 0.0F, false);
		door_frame.setTextureOffset(143, 49).addBox(-20.0F, -157.0F, -5.0F, 8.0F, 4.0F, 8.0F, 0.0F, false);
		door_frame.setTextureOffset(143, 49).addBox(12.0F, -157.0F, -5.0F, 8.0F, 4.0F, 8.0F, 0.0F, false);
		door_frame.setTextureOffset(143, 49).addBox(-16.0F, -165.0F, -5.0F, 32.0F, 4.0F, 8.0F, 0.0F, false);
		door_frame.setTextureOffset(143, 49).addBox(-16.0F, -121.0F, -5.0F, 32.0F, 4.0F, 8.0F, 0.0F, false);
		door_frame.setTextureOffset(143, 49).addBox(-16.0F, -113.0F, -5.0F, 32.0F, 4.0F, 8.0F, 0.0F, false);
		door_frame.setTextureOffset(107, 164).addBox(-16.0F, -29.0F, -5.0F, 32.0F, 4.0F, 8.0F, 0.0F, false);
		door_frame.setTextureOffset(101, 36).addBox(-20.0F, -153.0F, -5.0F, 4.0F, 128.0F, 8.0F, 0.0F, false);
		door_frame.setTextureOffset(120, 36).addBox(16.0F, -153.0F, -5.0F, 4.0F, 128.0F, 8.0F, 0.0F, false);
		door_frame.setTextureOffset(89, 77).addBox(-16.0F, -117.0F, -4.0F, 32.0F, 4.0F, 0.0F, 0.0F, false);
		door_frame.setTextureOffset(143, 49).addBox(-16.0F, -117.0F, 1.0F, 32.0F, 4.0F, 1.0F, 0.0F, false);
		door_frame.setTextureOffset(121, 103).addBox(-16.0F, -109.0F, -4.0F, 32.0F, 4.0F, 3.0F, 0.0F, false);
		door_frame.setTextureOffset(137, 84).addBox(12.0F, -105.0F, -4.0F, 4.0F, 72.0F, 3.0F, 0.0F, false);
		door_frame.setTextureOffset(111, 164).addBox(-16.0F, -33.0F, -4.0F, 32.0F, 4.0F, 3.0F, 0.0F, false);

		door_latch = new ModelRenderer(this);
		door_latch.setRotationPoint(-20.0F, 28.0F, 5.0F);
		door_rotate_y.addChild(door_latch);
		door_latch.setTextureOffset(11, 183).addBox(-19.0F, -97.0F, -7.0F, 2.0F, 8.0F, 12.0F, 0.0F, false);

		frame = new ModelRenderer(this);
		frame.setRotationPoint(0.0F, 24.0F, 0.0F);
		

		top = new ModelRenderer(this);
		top.setRotationPoint(0.0F, 0.0F, 0.0F);
		frame.addChild(top);
		top.setTextureOffset(80, 71).addBox(16.0F, -165.0F, -8.0F, 12.0F, 8.0F, 15.0F, 0.0F, false);
		top.setTextureOffset(33, 60).addBox(-28.0F, -165.0F, -8.0F, 12.0F, 8.0F, 15.0F, 0.0F, false);
		top.setTextureOffset(34, 49).addBox(-28.0F, -169.0F, -9.0F, 56.0F, 4.0F, 16.0F, 0.0F, false);

		posts = new ModelRenderer(this);
		posts.setRotationPoint(0.0F, 0.0F, 0.0F);
		frame.addChild(posts);
		

		foot1 = new ModelRenderer(this);
		foot1.setRotationPoint(0.0F, 0.0F, 0.0F);
		posts.addChild(foot1);
		

		foot2 = new ModelRenderer(this);
		foot2.setRotationPoint(0.0F, 0.0F, 0.0F);
		posts.addChild(foot2);
		

		foot3 = new ModelRenderer(this);
		foot3.setRotationPoint(0.0F, 0.0F, 0.0F);
		posts.addChild(foot3);
		

		foot4 = new ModelRenderer(this);
		foot4.setRotationPoint(0.0F, 0.0F, 0.0F);
		posts.addChild(foot4);
		

		torso = new ModelRenderer(this);
		torso.setRotationPoint(0.0F, 0.0F, 0.0F);
		frame.addChild(torso);
		torso.setTextureOffset(90, 15).addBox(-20.0F, -165.0F, 1.0F, 40.0F, 47.0F, 1.0F, 0.0F, false);
		torso.setTextureOffset(96, 14).addBox(-24.0F, -157.0F, -4.0F, 4.0F, 136.0F, 10.0F, 0.0F, false);
		torso.setTextureOffset(80, 10).addBox(20.0F, -157.0F, -4.0F, 4.0F, 136.0F, 10.0F, 0.0F, false);

		side_details = new ModelRenderer(this);
		side_details.setRotationPoint(0.0F, 0.0F, 0.0F);
		torso.addChild(side_details);
		

		side_details2 = new ModelRenderer(this);
		side_details2.setRotationPoint(0.0F, 0.0F, 0.0F);
		torso.addChild(side_details2);
		

		back_details = new ModelRenderer(this);
		back_details.setRotationPoint(0.0F, 0.0F, 0.0F);
		torso.addChild(back_details);
		

		base = new ModelRenderer(this);
		base.setRotationPoint(0.0F, 0.0F, 0.0F);
		frame.addChild(base);
		base.setTextureOffset(31, 129).addBox(-25.0F, -25.0F, -7.0F, 50.0F, 17.0F, 13.0F, 0.0F, false);
		base.setTextureOffset(67, 228).addBox(-22.0F, -22.0F, 5.25F, 44.0F, 11.0F, 1.0F, 0.0F, false);
		base.setTextureOffset(31, 129).addBox(-20.0F, -120.0F, -1.0F, 40.0F, 2.0F, 7.0F, 0.0F, false);

		clockworks = new ModelRenderer(this);
		clockworks.setRotationPoint(0.0F, -142.0F, 0.0F);
		frame.addChild(clockworks);
		

		small_cog_1 = new ModelRenderer(this);
		small_cog_1.setRotationPoint(-7.0156F, 6.0F, 4.625F);
		clockworks.addChild(small_cog_1);
		small_cog_1.setTextureOffset(11, 183).addBox(-0.9844F, -1.0F, -1.625F, 2.0F, 2.0F, 3.0F, 0.0F, false);
		small_cog_1.setTextureOffset(188, 209).addBox(-2.0156F, -2.0F, -0.375F, 4.0F, 4.0F, 1.0F, 0.0F, false);

		small_cog_2 = new ModelRenderer(this);
		small_cog_2.setRotationPoint(8.9844F, 2.5F, 3.625F);
		clockworks.addChild(small_cog_2);
		small_cog_2.setTextureOffset(11, 183).addBox(-0.9844F, -1.0F, -1.625F, 2.0F, 2.0F, 3.0F, 0.0F, false);
		small_cog_2.setTextureOffset(188, 209).addBox(-2.0156F, -2.0F, -0.375F, 4.0F, 4.0F, 1.0F, 0.0F, false);

		large_cog_1 = new ModelRenderer(this);
		large_cog_1.setRotationPoint(-5.8869F, -6.4203F, 6.125F);
		clockworks.addChild(large_cog_1);
		setRotationAngle(large_cog_1, 0.0F, 0.0F, 0.7854F);
		large_cog_1.setTextureOffset(11, 183).addBox(-0.9844F, -1.0F, -1.125F, 2.0F, 2.0F, 3.0F, 0.0F, false);
		large_cog_1.setTextureOffset(188, 209).addBox(-4.0156F, -4.0F, -0.875F, 8.0F, 8.0F, 1.0F, 0.0F, false);

		med_cog_1 = new ModelRenderer(this);
		med_cog_1.setRotationPoint(5.9061F, 6.2922F, 5.125F);
		clockworks.addChild(med_cog_1);
		setRotationAngle(med_cog_1, 0.0F, 0.0F, 1.9199F);
		med_cog_1.setTextureOffset(11, 183).addBox(-0.9844F, -1.0F, -1.125F, 2.0F, 2.0F, 3.0F, 0.0F, false);
		med_cog_1.setTextureOffset(188, 209).addBox(-3.0156F, -3.0F, -0.875F, 6.0F, 6.0F, 1.0F, 0.0F, false);

		center_gear = new ModelRenderer(this);
		center_gear.setRotationPoint(0.0F, 106.5F, 0.625F);
		clockworks.addChild(center_gear);
		center_gear.setTextureOffset(11, 183).addBox(-1.0F, -107.5F, -0.625F, 2.0F, 2.0F, 5.0F, 0.0F, false);

		bone = new ModelRenderer(this);
		bone.setRotationPoint(0.0F, 35.5F, -0.625F);
		center_gear.addChild(bone);
		bone.setTextureOffset(188, 209).addBox(-4.0F, -136.0F, 2.0F, 8.0F, 2.0F, 1.0F, 0.0F, false);
		bone.setTextureOffset(188, 209).addBox(-4.0F, -150.0F, 2.0F, 8.0F, 2.0F, 1.0F, 0.0F, false);

		bone2 = new ModelRenderer(this);
		bone2.setRotationPoint(0.0F, 0.0F, 0.0F);
		bone.addChild(bone2);
		bone2.setTextureOffset(188, 209).addBox(-1.0938F, -150.75F, 2.5F, 2.0F, 3.0F, 1.0F, 0.0F, false);
		bone2.setTextureOffset(188, 209).addBox(-2.0313F, -144.0F, 3.25F, 4.0F, 4.0F, 1.0F, 0.0F, false);
		bone2.setTextureOffset(188, 209).addBox(-1.0938F, -141.25F, 2.5F, 2.0F, 8.0F, 1.0F, 0.0F, false);

		bone3 = new ModelRenderer(this);
		bone3.setRotationPoint(-0.0938F, -142.0F, 3.0F);
		bone.addChild(bone3);
		setRotationAngle(bone3, 0.0F, 0.0F, 0.5236F);
		bone3.setTextureOffset(188, 209).addBox(-1.0F, -9.25F, -0.5F, 2.0F, 3.0F, 1.0F, 0.0F, false);
		bone3.setTextureOffset(188, 209).addBox(-1.0F, 6.25F, -0.5F, 2.0F, 3.0F, 1.0F, 0.0F, false);

		bone4 = new ModelRenderer(this);
		bone4.setRotationPoint(0.0F, -106.5F, 0.625F);
		center_gear.addChild(bone4);
		setRotationAngle(bone4, 0.0F, 0.0F, 1.0472F);
		bone4.setTextureOffset(188, 209).addBox(-4.0F, 6.0F, 0.75F, 8.0F, 2.0F, 1.0F, 0.0F, false);
		bone4.setTextureOffset(188, 209).addBox(-4.0F, -8.0F, 0.75F, 8.0F, 2.0F, 1.0F, 0.0F, false);

		bone5 = new ModelRenderer(this);
		bone5.setRotationPoint(0.0F, 142.0F, -1.25F);
		bone4.addChild(bone5);
		bone5.setTextureOffset(188, 209).addBox(-1.0938F, -150.75F, 2.5F, 2.0F, 9.0F, 1.0F, 0.0F, false);
		bone5.setTextureOffset(188, 209).addBox(-1.0938F, -136.25F, 2.5F, 2.0F, 3.0F, 1.0F, 0.0F, false);

		bone6 = new ModelRenderer(this);
		bone6.setRotationPoint(-0.0938F, 0.0F, 1.75F);
		bone4.addChild(bone6);
		setRotationAngle(bone6, 0.0F, 0.0F, 0.5236F);
		bone6.setTextureOffset(188, 209).addBox(-1.0F, -9.25F, -0.5F, 2.0F, 3.0F, 1.0F, 0.0F, false);
		bone6.setTextureOffset(188, 209).addBox(-1.0F, 6.25F, -0.5F, 2.0F, 3.0F, 1.0F, 0.0F, false);

		bone7 = new ModelRenderer(this);
		bone7.setRotationPoint(0.0F, -106.5F, 0.625F);
		center_gear.addChild(bone7);
		setRotationAngle(bone7, 0.0F, 0.0F, 2.0944F);
		bone7.setTextureOffset(188, 209).addBox(-4.0F, 6.0F, 0.75F, 8.0F, 2.0F, 1.0F, 0.0F, false);
		bone7.setTextureOffset(188, 209).addBox(-4.0F, -8.0F, 0.75F, 8.0F, 2.0F, 1.0F, 0.0F, false);

		bone8 = new ModelRenderer(this);
		bone8.setRotationPoint(0.0F, 142.0F, -1.25F);
		bone7.addChild(bone8);
		bone8.setTextureOffset(188, 209).addBox(-1.0938F, -150.75F, 2.5F, 2.0F, 3.0F, 1.0F, 0.0F, false);
		bone8.setTextureOffset(188, 209).addBox(-1.0938F, -142.25F, 2.5F, 2.0F, 9.0F, 1.0F, 0.0F, false);

		bone9 = new ModelRenderer(this);
		bone9.setRotationPoint(-0.0938F, 0.0F, 1.75F);
		bone7.addChild(bone9);
		setRotationAngle(bone9, 0.0F, 0.0F, 0.5236F);
		bone9.setTextureOffset(188, 209).addBox(-1.0F, -9.25F, -0.5F, 2.0F, 3.0F, 1.0F, 0.0F, false);
		bone9.setTextureOffset(188, 209).addBox(-1.0F, 6.25F, -0.5F, 2.0F, 3.0F, 1.0F, 0.0F, false);

		center_frame = new ModelRenderer(this);
		center_frame.setRotationPoint(0.0F, 0.0F, 0.0F);
		clockworks.addChild(center_frame);
		

		bone10 = new ModelRenderer(this);
		bone10.setRotationPoint(0.0F, 142.0F, 0.0F);
		center_frame.addChild(bone10);
		bone10.setTextureOffset(11, 183).addBox(-1.0F, -143.0F, 0.0F, 2.0F, 2.0F, 5.0F, 0.0F, false);

		bone11 = new ModelRenderer(this);
		bone11.setRotationPoint(0.0F, 0.0F, 0.0F);
		bone10.addChild(bone11);
		bone11.setTextureOffset(188, 209).addBox(-2.0313F, -144.0F, 3.25F, 4.0F, 4.0F, 1.0F, 0.0F, false);
		bone11.setTextureOffset(188, 209).addBox(-1.0938F, -141.0F, 2.5F, 2.0F, 11.0F, 1.0F, 0.0F, false);
		bone11.setTextureOffset(188, 209).addBox(-1.0938F, -154.0F, 2.5F, 2.0F, 11.0F, 1.0F, 0.0F, false);
		bone11.setTextureOffset(188, 209).addBox(-7.0F, -130.25F, 2.0F, 14.0F, 1.0F, 3.0F, 0.0F, false);
		bone11.setTextureOffset(188, 209).addBox(-8.0F, -130.75F, 2.5F, 2.0F, 2.0F, 3.0F, 0.0F, false);
		bone11.setTextureOffset(188, 209).addBox(6.0F, -155.0F, 2.5F, 2.0F, 2.0F, 3.0F, 0.0F, false);
		bone11.setTextureOffset(188, 209).addBox(-7.0F, -154.5F, 2.0F, 14.0F, 1.0F, 3.0F, 0.0F, false);

		bone12 = new ModelRenderer(this);
		bone12.setRotationPoint(-0.0313F, -141.9286F, 3.5357F);
		bone10.addChild(bone12);
		setRotationAngle(bone12, 0.0F, 0.0F, 1.0472F);
		bone12.setTextureOffset(188, 209).addBox(-2.0F, -2.0714F, -0.5357F, 4.0F, 4.0F, 1.0F, 0.0F, false);
		bone12.setTextureOffset(188, 209).addBox(-1.0625F, 0.9286F, -1.0357F, 2.0F, 11.0F, 1.0F, 0.0F, false);
		bone12.setTextureOffset(188, 209).addBox(-1.0625F, -12.0714F, -1.0357F, 2.0F, 11.0F, 1.0F, 0.0F, false);
		bone12.setTextureOffset(188, 209).addBox(-6.9688F, 11.6786F, -1.5357F, 14.0F, 1.0F, 3.0F, 0.0F, false);
		bone12.setTextureOffset(188, 209).addBox(-7.9688F, 11.1786F, -1.0357F, 2.0F, 2.0F, 3.0F, 0.0F, false);
		bone12.setTextureOffset(188, 209).addBox(6.0313F, -13.0714F, -1.0357F, 2.0F, 2.0F, 3.0F, 0.0F, false);
		bone12.setTextureOffset(188, 209).addBox(-6.9688F, -12.5714F, -1.5357F, 14.0F, 1.0F, 3.0F, 0.0F, false);

		bone13 = new ModelRenderer(this);
		bone13.setRotationPoint(-0.0313F, -141.9286F, 3.5357F);
		bone10.addChild(bone13);
		setRotationAngle(bone13, 0.0F, 0.0F, 2.0944F);
		bone13.setTextureOffset(188, 209).addBox(-2.0F, -2.0714F, -0.0357F, 4.0F, 4.0F, 1.0F, 0.0F, false);
		bone13.setTextureOffset(188, 209).addBox(-1.0625F, 0.9286F, -1.0357F, 2.0F, 11.0F, 1.0F, 0.0F, false);
		bone13.setTextureOffset(188, 209).addBox(-1.0625F, -12.0714F, -1.0357F, 2.0F, 11.0F, 1.0F, 0.0F, false);
		bone13.setTextureOffset(188, 209).addBox(-6.9688F, 11.6786F, -1.5357F, 14.0F, 1.0F, 3.0F, 0.0F, false);
		bone13.setTextureOffset(188, 209).addBox(-7.9688F, 11.1786F, -1.0357F, 2.0F, 2.0F, 3.0F, 0.0F, false);
		bone13.setTextureOffset(188, 209).addBox(6.0313F, -13.0714F, -1.0357F, 2.0F, 2.0F, 3.0F, 0.0F, false);
		bone13.setTextureOffset(188, 209).addBox(-6.9688F, -12.5714F, -1.5357F, 14.0F, 1.0F, 3.0F, 0.0F, false);
	}

    @Override
    public void setRotationAngles(Entity entity, float limbSwing, float limbSwingAmount, float ageInTicks, float netHeadYaw, float headPitch){
        //previously the render function, render code was moved to a method below
    }

    @Override
    public void render(MatrixStack matrixStack, IVertexBuilder buffer, int packedLight, int packedOverlay, float red, float green, float blue, float alpha){
        
    }

    public void setRotationAngle(ModelRenderer modelRenderer, float x, float y, float z) {
        modelRenderer.rotateAngleX = x;
        modelRenderer.rotateAngleY = y;
        modelRenderer.rotateAngleZ = z;
    }

    @Override
    public void render(DoorEntity door, MatrixStack matrixStack, IVertexBuilder buffer, int packedLight, int packedOverlay) {
        matrixStack.push();
        
        matrixStack.translate(0, 1.25, -0.5);
        matrixStack.scale(0.25F, 0.25F, 0.25F);
        this.door_rotate_y.rotateAngleY = (float) Math.toRadians(EnumDoorType.CLOCK.getRotationForState(door.getOpenState()));
        
        //boti.render(matrixStack, buffer, packedLight, packedOverlay);
        door_rotate_y.render(matrixStack, buffer, packedLight, packedOverlay);
        frame.render(matrixStack, buffer, packedLight, packedOverlay);
        
        matrixStack.pop();
        
        Minecraft.getInstance().world.getCapability(Capabilities.TARDIS_DATA).ifPresent(data -> {
        	PortalInfo info = new PortalInfo();
            info.setPosition(door.getPositionVec());
            info.setWorldShell(data.getBotiWorld());
            
            //Translations
            info.setTranslate(matrix -> {
            	DoorRenderer.applyTranslations(matrix, door.rotationYaw - 180, door.getHorizontalFacing());
            	matrix.translate(0, -0.25, -0.5);
            	matrix.rotate(Vector3f.ZN.rotationDegrees(180));
            });
            
            info.setTranslatePortal(matrix -> {
            	matrix.translate(0, -1.6, 0);
            	matrix.rotate(Vector3f.YP.rotationDegrees(WorldHelper.getAngleFromFacing(data.getBotiWorld().getPortalDirection())));
            	matrix.translate(-0.5, 0, -0.5);
            });
            
            info.setRenderPortal((matrix, buf) -> {
            	matrix.push();
            	matrix.scale(0.25F, 0.25F, 0.25F);
            	this.boti.render(matrix, buf.getBuffer(RenderType.getEntityTranslucent(getTexture())), packedLight, packedOverlay);
            	matrix.pop();
            });
            
            info.setRenderDoor((matrix, buf) -> {
            	matrix.push();
            	matrix.rotate(Vector3f.ZP.rotationDegrees(180));
            	matrix.translate(0, 1.5, 0);
            	matrix.scale(0.25F, 0.25F, 0.25F);
            	this.door_rotate_y.render(matrix, buf.getBuffer(RenderType.getEntityTranslucent(getTexture())), packedLight, packedOverlay);
            	matrix.pop();
            });
            
            
            BOTIRenderer.addPortal(info);
        });
        
    }

    @Override
    public ResourceLocation getTexture() {
        return ClockExteriorRenderer.TEXTURE;
    }
}