package net.tardis.mod.client.models.interiordoors;

import com.mojang.blaze3d.matrix.MatrixStack;
import com.mojang.blaze3d.vertex.IVertexBuilder;

import net.minecraft.util.ResourceLocation;
import net.tardis.mod.entity.DoorEntity;

public interface IInteriorDoorRenderer {

    void render(DoorEntity door, MatrixStack matrixStack, IVertexBuilder buffer, int packedLight, int packedOverlay);

    ResourceLocation getTexture();
}
