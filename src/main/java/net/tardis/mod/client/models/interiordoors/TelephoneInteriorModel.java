package net.tardis.mod.client.models.interiordoors;
// Made with Blockbench 3.7.4
// Exported for Minecraft version 1.15
// Paste this class into your mod and generate all required imports

import com.mojang.blaze3d.matrix.MatrixStack;
import com.mojang.blaze3d.vertex.IVertexBuilder;

import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.entity.model.EntityModel;
import net.minecraft.client.renderer.model.ModelRenderer;
import net.minecraft.entity.Entity;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.math.vector.Vector3f;
import net.tardis.mod.Tardis;
import net.tardis.mod.entity.DoorEntity;
import net.tardis.mod.helper.TardisHelper;
import net.tardis.mod.misc.IDoorType.EnumDoorType;
import net.tardis.mod.tileentities.ConsoleTile;

public class TelephoneInteriorModel extends EntityModel<Entity> implements IInteriorDoorRenderer{
    
    public static final ResourceLocation TEXTURE = new ResourceLocation(Tardis.MODID, "textures/exteriors/interior/telephone_red.png");
    
    private final ModelRenderer BOTI;
	private final ModelRenderer door;
	private final ModelRenderer bone2;
	private final ModelRenderer bone;
	private final ModelRenderer bone3;
	private final ModelRenderer bone4;
	private final ModelRenderer bb_main;

	public TelephoneInteriorModel() {
		textureWidth = 128;
		textureHeight = 128;

		BOTI = new ModelRenderer(this);
		BOTI.setRotationPoint(-3.0F, 57.0F, -2.0F);
		BOTI.setTextureOffset(32, 32).addBox(-4.0F, -66.0F, 6.5F, 14.0F, 33.0F, 1.0F, 0.0F, false);

		door = new ModelRenderer(this);
		door.setRotationPoint(-7.0F, 7.5F, 6.0F);
		door.setTextureOffset(0, 39).addBox(0.0F, -16.5F, 0.0F, 14.0F, 33.0F, 1.0F, 0.0F, false);
		door.setTextureOffset(32, 15).addBox(0.0F, -10.5F, -1.0F, 1.0F, 4.0F, 1.0F, 0.0F, false);
		door.setTextureOffset(32, 15).addBox(0.0F, 6.5F, -1.0F, 1.0F, 4.0F, 1.0F, 0.0F, false);
		door.setTextureOffset(32, 0).addBox(0.0F, 10.5F, -0.5F, 14.0F, 6.0F, 1.0F, 0.0F, false);
		door.setTextureOffset(32, 11).addBox(11.0F, -4.0F, -1.0F, 2.0F, 3.0F, 1.0F, 0.0F, false);

		bone2 = new ModelRenderer(this);
		bone2.setRotationPoint(0.0F, 24.0F, 0.0F);
		bone2.setTextureOffset(62, 62).addBox(7.0F, -39.0F, 6.5F, 2.0F, 39.0F, 2.0F, 0.0F, false);
		bone2.setTextureOffset(59, 59).addBox(-9.0F, 0.025F, 5.5F, 18.0F, 0.0F, 3.0F, 0.0F, false);
		bone2.setTextureOffset(50, 66).addBox(7.0F, -35.0F, 5.5F, 1.0F, 35.0F, 1.0F, 0.0F, false);

		bone = new ModelRenderer(this);
		bone.setRotationPoint(8.0F, -20.0F, -9.0F);
		bone2.addChild(bone);
		setRotationAngle(bone, 0.0F, -0.7854F, 0.0F);
		bone.setTextureOffset(42, 66).addBox(10.253F, -19.0F, 10.253F, 1.0F, 39.0F, 1.0F, 0.0F, false);
		bone.setTextureOffset(42, 66).addBox(10.6673F, -19.0F, 10.253F, 1.0F, 39.0F, 1.0F, 0.0F, false);

		bone3 = new ModelRenderer(this);
		bone3.setRotationPoint(0.0F, 24.0F, 0.0F);
		bone3.setTextureOffset(62, 62).addBox(-9.0F, -39.0F, 6.5F, 2.0F, 39.0F, 2.0F, 0.0F, false);
		bone3.setTextureOffset(46, 66).addBox(-8.0F, -35.0F, 5.5F, 1.0F, 35.0F, 1.0F, 0.0F, false);

		bone4 = new ModelRenderer(this);
		bone4.setRotationPoint(-8.0F, -20.0F, -9.0F);
		bone3.addChild(bone4);
		setRotationAngle(bone4, 0.0F, 0.7854F, 0.0F);
		bone4.setTextureOffset(34, 66).addBox(-11.253F, -19.0F, 10.253F, 1.0F, 39.0F, 1.0F, 0.0F, false);
		bone4.setTextureOffset(34, 66).addBox(-11.6673F, -19.0F, 10.253F, 1.0F, 39.0F, 1.0F, 0.0F, false);

		bb_main = new ModelRenderer(this);
		bb_main.setRotationPoint(0.0F, 24.0F, 0.0F);
		bb_main.setTextureOffset(62, 48).addBox(-7.0F, -35.0F, 5.5F, 14.0F, 2.0F, 2.0F, 0.0F, false);
		bb_main.setTextureOffset(62, 41).addBox(-8.0F, -39.0F, 5.5F, 16.0F, 4.0F, 3.0F, 0.0F, false);
		bb_main.setTextureOffset(32, 7).addBox(-6.0F, -38.25F, 5.0F, 12.0F, 3.0F, 1.0F, 0.0F, false);
	}

    @Override
    public void setRotationAngles(Entity entity, float limbSwing, float limbSwingAmount, float ageInTicks, float netHeadYaw, float headPitch){
        //previously the render function, render code was moved to a method below
    }

    @Override
    public void render(MatrixStack matrixStack, IVertexBuilder buffer, int packedLight, int packedOverlay, float red, float green, float blue, float alpha){
        
    }

    public void setRotationAngle(ModelRenderer modelRenderer, float x, float y, float z) {
        modelRenderer.rotateAngleX = x;
        modelRenderer.rotateAngleY = y;
        modelRenderer.rotateAngleZ = z;
    }

    @Override
    public void render(DoorEntity door, MatrixStack matrixStack, IVertexBuilder buffer, int packedLight,
            int packedOverlay) {
        matrixStack.push();
        matrixStack.translate(0, 0, -0.95);
        this.door.rotateAngleY = (float) Math.toRadians(EnumDoorType.TELEPHONE.getRotationForState(door.getOpenState()));
        
        BOTI.render(matrixStack, buffer, packedLight, packedOverlay);
		this.door.render(matrixStack, buffer, packedLight, packedOverlay);
		bone2.render(matrixStack, buffer, packedLight, packedOverlay);
		bone3.render(matrixStack, buffer, packedLight, packedOverlay);
		bb_main.render(matrixStack, buffer, packedLight, packedOverlay);
        
        matrixStack.pop();
    }

    @Override
    public ResourceLocation getTexture() {
    	ConsoleTile tile = TardisHelper.getConsoleInWorld(Minecraft.getInstance().world).orElse(null);
        if (tile != null) {
          int index = tile.getExteriorManager().getExteriorVariant();
          if (tile.getExteriorType().getVariants() != null && index < tile.getExteriorType().getVariants().length) {
//              ResourceLocation original = tile.getExterior().getVariants()[index].getTexture();
//              String originalString = original.toString();
//              String newString = originalString.replace(".png", "_interior.png");
//              ResourceLocation finalLocation = new ResourceLocation(newString);
        	  return tile.getExteriorType().getVariants()[index].getInteriorDoorTexture();
          }
        }
        return TEXTURE;
    }
}