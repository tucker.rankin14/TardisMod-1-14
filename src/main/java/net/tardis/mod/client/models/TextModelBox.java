package net.tardis.mod.client.models;

import java.util.function.Supplier;

import com.mojang.blaze3d.platform.GlStateManager;

import net.minecraft.client.renderer.BufferBuilder;
import net.minecraft.client.renderer.model.ModelRenderer;
import net.minecraft.util.text.TextFormatting;
import net.tardis.mod.misc.WorldText;

public class TextModelBox extends ModelRenderer.ModelBox{
	
	Supplier<String> text;
	WorldText textRenderer = new WorldText(1, 1, 1, TextFormatting.DARK_PURPLE);

	public TextModelBox(ModelRenderer renderer, Supplier<String> text, WorldText textRender, int texOffX, int texOffY, float x, float y, float z, float width, float height, float depth, float deltaX, float deltaY, float deltaZ, boolean mirorIn, float texWidth, float texHeight) {
		super(texOffX, texOffY, x, y, z, width, height, depth, deltaX, deltaY, deltaZ, mirorIn, texWidth, texHeight);
		this.text = text;
		this.textRenderer = textRender;
	}

//	@Override
//	public void render(BufferBuilder renderer, float scale) {
//		super.render(renderer, scale);
//		GlStateManager.pushMatrix();
//		GlStateManager.translated(this.posX1, this.posY1, this.posZ1);
//		if(text != null && text.get() != null)
//			textRenderer.renderMonitor(text.get());
//		GlStateManager.popMatrix();
//	}

}
