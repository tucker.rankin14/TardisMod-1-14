package net.tardis.mod.client.renderers.tiles;

import com.mojang.blaze3d.matrix.MatrixStack;
import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.IRenderTypeBuffer;
import net.minecraft.client.renderer.tileentity.TileEntityRenderer;
import net.minecraft.client.renderer.tileentity.TileEntityRendererDispatcher;
import net.minecraft.state.properties.BlockStateProperties;
import net.minecraft.util.math.vector.Vector3f;
import net.minecraft.util.text.TextFormatting;
import net.tardis.mod.helper.Helper;
import net.tardis.mod.helper.TardisHelper;
import net.tardis.mod.helper.WorldHelper;
import net.tardis.mod.misc.WorldText;
import net.tardis.mod.tileentities.MonitorDynamicTile;

public class MonitorDynamicRenderer extends TileEntityRenderer<MonitorDynamicTile> {


	public MonitorDynamicRenderer(TileEntityRendererDispatcher rendererDispatcherIn) {
		super(rendererDispatcherIn);
	}

	@Override
	public void render(MonitorDynamicTile tile, float partialTicks, MatrixStack matrixStackIn, IRenderTypeBuffer bufferIn, int combinedLightIn, int combinedOverlayIn) {
		if(!tile.isCorener())
			return;

		TardisHelper.getConsoleInWorld(Minecraft.getInstance().world).ifPresent(console -> {
			matrixStackIn.push();
			matrixStackIn.translate(0.5, 1, 0.5);
			matrixStackIn.rotate(Vector3f.YP.rotationDegrees(360 - WorldHelper.getAngleFromFacing(tile.getBlockState().get(BlockStateProperties.HORIZONTAL_FACING)) - 180));
			int width = tile.getWidth();
			matrixStackIn.translate(-width + 0.5 + 0.03125, -0.03125, -0.31);
			matrixStackIn.rotate(Vector3f.XP.rotationDegrees(180));
			WorldText text = new WorldText(width - 0.0625F, tile.getHeight() - 0.0625F, tile.getHeight() / (100.0F), TextFormatting.WHITE.getColor());
			text.renderText(matrixStackIn, bufferIn, combinedLightIn, Helper.getConsoleText(console));
			matrixStackIn.pop();
		});
	}

	@Override
	public boolean isGlobalRenderer(MonitorDynamicTile te) {
		return te.isCorener();
	}

}
