package net.tardis.mod.client.renderers.entity.transport;

import com.mojang.blaze3d.matrix.MatrixStack;
import com.mojang.blaze3d.platform.GlStateManager;

import net.minecraft.client.renderer.IRenderTypeBuffer;
import net.minecraft.client.renderer.entity.EntityRenderer;
import net.minecraft.client.renderer.entity.EntityRendererManager;
import net.minecraft.client.renderer.tileentity.TileEntityRendererDispatcher;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.math.vector.Vector3f;
import net.tardis.mod.entity.TardisEntity;

public class TardisEntityRenderer extends EntityRenderer<TardisEntity>{
	
	public TardisEntityRenderer(EntityRendererManager renderManager) {
		super(renderManager);
	}
	
	

	@Override
	public void render(TardisEntity entity, float entityYaw, float partialTicks, MatrixStack matrixStackIn,
	        IRenderTypeBuffer bufferIn, int packedLightIn) {
		matrixStackIn.push();
		matrixStackIn.rotate(Vector3f.YP.rotationDegrees(-entity.rotationYaw - 180));
		if(entity.hasNoGravity())
			matrixStackIn.translate(0, Math.sin(entity.ticksExisted * 0.05) * 0.1, 0);
		if(entity.getExteriorTile() != null)
			TileEntityRendererDispatcher.instance.renderTileEntity(entity.getExteriorTile(), partialTicks, matrixStackIn, bufferIn);
		
		matrixStackIn.pop();
	}

	@Override
	public ResourceLocation getEntityTexture(TardisEntity entity) {
		return null;
	}

}
