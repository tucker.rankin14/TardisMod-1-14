package net.tardis.mod.client.renderers.entity;

import com.mojang.blaze3d.matrix.MatrixStack;

import net.minecraft.client.renderer.IRenderTypeBuffer;
import net.minecraft.client.renderer.RenderType;
import net.minecraft.client.renderer.entity.EntityRendererManager;
import net.minecraft.client.renderer.entity.LivingRenderer;
import net.minecraft.client.renderer.texture.OverlayTexture;
import net.minecraft.util.ResourceLocation;
import net.tardis.mod.Tardis;
import net.tardis.mod.client.models.entity.drones.SecDroneModel;
import net.tardis.mod.entity.SecDroidEntity;

public class SecDroidRenderer extends LivingRenderer<SecDroidEntity, SecDroneModel>{

	public static final ResourceLocation TEXTURE = new ResourceLocation(Tardis.MODID, "textures/entity/sec_drone.png");
	public static final SecDroneModel MODEL = new SecDroneModel();
	
	public SecDroidRenderer(EntityRendererManager rendererManager) {
		super(rendererManager, MODEL, 0.5F);
	}
	
	

	@Override
	public void render(SecDroidEntity entityIn, float entityYaw, float partialTicks, MatrixStack matrixStackIn,
	        IRenderTypeBuffer bufferIn, int packedLightIn) {
		
		MODEL.render(entityIn, matrixStackIn, bufferIn.getBuffer(RenderType.getEntityCutout(TEXTURE)), packedLightIn, OverlayTexture.NO_OVERLAY);
	}


	@Override
	public ResourceLocation getEntityTexture(SecDroidEntity entity) {
		return TEXTURE;
	}

}