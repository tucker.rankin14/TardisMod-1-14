package net.tardis.mod.client.guis.minigame;

import java.util.List;
import java.util.Random;

import javax.annotation.Nullable;

import org.lwjgl.opengl.GL11;


import com.google.common.collect.Lists;
import com.mojang.blaze3d.platform.GlStateManager;
import com.google.common.collect.Lists;
import com.mojang.blaze3d.matrix.MatrixStack;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.screen.Screen;
import net.minecraft.client.renderer.BufferBuilder;
import net.minecraft.client.renderer.Tessellator;
import net.minecraft.client.renderer.vertex.DefaultVertexFormats;
import net.minecraft.util.Direction;
import net.minecraft.util.ResourceLocation;

import net.minecraft.util.math.vector.Vector3i;
import net.minecraft.util.text.StringTextComponent;
import net.tardis.mod.client.guis.minigame.wires.Terminal;
import net.tardis.mod.client.guis.minigame.wires.Wire;
import net.tardis.mod.client.guis.minigame.wires.Wire.Type;
import net.tardis.mod.helper.Helper;
import net.tardis.mod.network.Network;
import net.tardis.mod.network.packets.FailEngineMessage;
import net.tardis.mod.sounds.TSounds;
import org.lwjgl.opengl.GL11;

import java.awt.*;
import java.util.List;
import java.util.Random;

public class WireGameScreen extends Screen{

	private static final ResourceLocation TEXTURE = Helper.createRL("textures/gui/minigame/wires.png");
	private static final StringTextComponent TITLE = new StringTextComponent("WIRE");
	private List<Wire> connections = Lists.newArrayList();
	private List<Terminal> terminals = Lists.newArrayList();
	private int slotNumber = 0;
	private Direction panel;
	private boolean completed = false;
	
	public WireGameScreen(int slot, Direction panel) {
		super(TITLE);
		this.slotNumber = slot;
		this.panel = panel;
	}

	@Override
	public void render(MatrixStack matrixStack, int mouseX, int mouseY, float partialTicks) {
		this.renderBackground(matrixStack);
		
		super.render(matrixStack, mouseX, mouseY, partialTicks);
		
		Minecraft.getInstance().getTextureManager().bindTexture(TEXTURE);
		
		this.blit(matrixStack, width / 2 - 232 / 2, height / 2 - 202 / 2, 0, 0, 232, 202);
		
		//Update Wires
		for(Wire wire : this.connections) {
			wire.update(mouseX, mouseY);
		}
		
		BufferBuilder bb = Tessellator.getInstance().getBuffer();
		
		GlStateManager.disableTexture();
		//Wires
		bb.begin(GL11.GL_QUADS, DefaultVertexFormats.POSITION_COLOR);
		for(Wire wire : this.connections) {
			wire.render(bb);
		}
		Tessellator.getInstance().draw();
		GlStateManager.enableTexture();
		
		//Wires Post
		bb.begin(GL11.GL_QUADS, DefaultVertexFormats.POSITION_TEX);
		for(Wire wire : this.connections) {
			wire.renderPost(bb);
		}
		for(Terminal term: this.terminals) {
			term.render(bb);
		}
		Tessellator.getInstance().draw();
		
	}
	
	@Override
	protected void init() {
		super.init();
		this.connections.clear();
		this.terminals.clear();
		
		this.connections.add(new Wire(Type.RED, width / 2 - 80, height / 2 - 79));
		this.connections.add(new Wire(Type.GREEN, width / 2 - 105, height / 2 - 39));
		this.connections.add(new Wire(Type.BLACK, width / 2 - 105, height / 2 + 25));
		this.connections.add(new Wire(Type.BLUE, width / 2 - 80, height / 2 + 68));
		
		List<Vector3i> termPos = Lists.newArrayList(
				new Vector3i(width / 2 + 63, height / 2 - 79, 0),
				new Vector3i(width / 2 + 88, height / 2 - 39, 0),
				new Vector3i(width / 2 + 88, height / 2 + 25, 0),
				new Vector3i(width / 2 + 63, height / 2 + 68, 0)
		);
		
		Random rand = Minecraft.getInstance().world.rand;
		
		for(Type type : Wire.Type.values()) {
			Vector3i pos = termPos.get(rand.nextInt(termPos.size()));
			this.terminals.add(new Terminal(type, pos.getX(), pos.getY()));
			termPos.remove(pos);
		}
	}
	
	@Override
	public void tick() {
		super.tick();
		
		boolean finished = true;
		for(Wire wire : this.connections) {
			if(!wire.isComplete()) {
				finished = false;
				break;
			}
		}
		
		//If all are finished
		if(finished) {
			this.minecraft.player.playSound(TSounds.REACHED_DESTINATION.get(), 1F, 1F);
			this.completed = true;
			Minecraft.getInstance().displayGuiScreen(null);
		}
		
		
	}

	@Override
	public void closeScreen() {
		if(!this.completed) {
			this.fail();
		}
		
	}

	@Override
	public boolean isPauseScreen() {
		return false;
	}

	public void fail() {
		//TODO: Fail
		Minecraft.getInstance().displayGuiScreen((Screen)null);
		this.minecraft.player.playSound(TSounds.SONIC_FAIL.get(), 1F, 1F);
		Network.sendToServer(new FailEngineMessage(this.panel, this.slotNumber));
	}

	@Override
	public boolean mouseClicked(double mouseX, double mouseY, int button) {
		for(Terminal term : this.terminals) {
			if(!term.onClick((int)mouseX, (int)mouseY, this.getSelectedWire()))
				this.fail();
		}
		
		for(Wire w : this.connections) {
			w.onClick((int)mouseX, (int)mouseY);
		}
		return super.mouseClicked(mouseX, mouseY, button);

	}
	
	@Nullable
	public Wire getSelectedWire() {
		for(Wire wire : this.connections) {
			if(wire.isSelected())
				return wire;
		}
		return null;
	}
	
}
