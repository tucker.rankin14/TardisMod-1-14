package net.tardis.mod.client.guis.monitors;


import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import com.mojang.blaze3d.matrix.MatrixStack;

import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.texture.DynamicTexture;
import net.minecraft.client.renderer.texture.NativeImage;
import net.minecraft.client.resources.DefaultPlayerSkin;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.text.TranslationTextComponent;
import net.tardis.mod.Tardis;
import net.tardis.mod.ars.ConsoleRoom;
import net.tardis.mod.client.guis.widgets.TextButton;
import net.tardis.mod.constants.Constants;
import net.tardis.mod.helper.TardisHelper;
import net.tardis.mod.network.Network;
import net.tardis.mod.network.packets.ChangeInteriorMessage;

public class InteriorChangeScreen extends MonitorScreen {

    private static final HashMap<ConsoleRoom, ResourceLocation> PREVIEW_TEXTURES = new HashMap<>();
    private List<ConsoleRoom> rooms = new ArrayList<>();
    private int index = 0;

    public InteriorChangeScreen(IMonitorGui mon, String menu) {
        super(mon, menu);
    }

    //TODO This could be moved elsewhere, wasn't sure where to put it
    public static ResourceLocation urlToTexture(URL url) {
        URLConnection uc = null;
        NativeImage image = null;
        try {
            uc = url.openConnection();
            uc.connect();
            uc = url.openConnection();
            uc.addRequestProperty("User-Agent", "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/73.0.3683.75 Safari/537.36");
            Tardis.LOGGER.warn("Downloading Image from: {}", url.toString());
            image = NativeImage.read(uc.getInputStream());
            return Minecraft.getInstance().getTextureManager().getDynamicTextureLocation("interior_", new DynamicTexture(image));
        } catch (IOException e) {
            e.printStackTrace();
        }
        return DefaultPlayerSkin.getDefaultSkinLegacy();
    }

    @Override
    protected void init() {
        super.init();

        this.addButton(new TextButton(this.parent.getMinX(),
                this.parent.getMinY(),
                Constants.Translations.GUI_PREV.getString(), (button) -> modIndex(-1)));

        this.addButton(new TextButton(this.parent.getMinX(),
                this.parent.getMinY() - (int) (this.minecraft.fontRenderer.FONT_HEIGHT * 1.25),
                Constants.Translations.GUI_SELECT.getString(), (button) -> {
            confirmAction();
        }));

        this.addButton(new TextButton(this.parent.getMinX(),
                this.parent.getMinY() - (int) ((this.minecraft.fontRenderer.FONT_HEIGHT * 1.25) * 2),
                Constants.Translations.GUI_NEXT.getString(), (button) -> modIndex(1)));

        this.rooms.clear();

        TardisHelper.getConsoleInWorld(Minecraft.getInstance().world).ifPresent(tile -> {
            rooms.addAll(tile.getUnlockManager().getUnlockedConsoleRooms());
            if (!rooms.contains(tile.getConsoleRoom()))
                rooms.add(tile.getConsoleRoom());
            int temp = 0;
            for (ConsoleRoom r : rooms) {

                if (r == tile.getConsoleRoom()) {
                    this.index = temp;
                    break;
                }
                ++temp;
            }
        });
        //Called here to update the room image
        modIndex(0);
    }

    @Override
    public void render(MatrixStack matrixStack, int mouseX, int mouseY, float partialTicks) {
        super.render(matrixStack, mouseX, mouseY, partialTicks);

        ConsoleRoom room = this.getRoomFromIndex();

        if (room != null) {
            //We use the translation key as a workaround to render our datapack names because we construct a translationtextcomponent in datapacks, but drawCentredString will account for text formatting codes
            this.drawCenteredString(matrixStack, this.font, room.isDataPack() ? room.getDisplayName().getKey() : room.getDisplayName().getString(), this.parent.getMinX() + ((this.parent.getMaxX() - this.parent.getMinX()) / 2), this.parent.getMaxY() + 90, 0xFFFFFF);

            if (room.isUsingRemoteImage().get()) {
                this.minecraft.getTextureManager().bindTexture(getTextureForRoom(room));
            } else {
                this.minecraft.getTextureManager().bindTexture(room.getTexture());
            }
            float asp = 1.77777F;
            int centerX = this.parent.getMinX() + ((this.parent.getMaxX() - this.parent.getMinX()) / 2);

            int width = 150;
            int height = (int) (width / asp);
            blit(matrixStack, centerX - width / 2, this.parent.getMaxY(), 0, 0, width, height, width, height);
        }
    }

    private ConsoleRoom getRoomFromIndex() {
        if (index < 0 || index >= this.rooms.size()) {
        	return ConsoleRoom.ARCHITECT;
        }
        return rooms.get(index);
    }

    private void modIndex(int i) {
        if (index + i >= this.rooms.size()) {
            index = 0;
        } else if (index + i < 0) {
            index = this.rooms.size() - 1;
        } else index += i;

    }

    private void confirmAction() {
        this.getMinecraft().displayGuiScreen(new MonitorConfirmScreen(parent, menu, (shouldChange) -> {
            if (shouldChange && this.getRoomFromIndex() != null) {
                Network.sendToServer(new ChangeInteriorMessage(this.getRoomFromIndex().getRegistryName(), false));
                this.minecraft.displayGuiScreen(null);
            } else {
                this.minecraft.displayGuiScreen(new InteriorChangeScreen(parent, menu));
            }
        }, new TranslationTextComponent("gui.tardis.interior.change.warning_message"), new TranslationTextComponent("gui.tardis.interior.change.confirm_message"), Constants.Translations.GUI_CONFIRM, Constants.Translations.GUI_CANCEL));
    }

    public ResourceLocation getTextureForRoom(ConsoleRoom consoleRoom) {
        if (PREVIEW_TEXTURES.containsKey(consoleRoom)) {
            return PREVIEW_TEXTURES.get(consoleRoom);
        }

        //Make this happen on another thread so we don't make the game have a seizure
        //This is only called ONCE per image, as if they already exist, it won't be called again
        // (At least, until the user reboots the game)
        Thread thread = new Thread(() -> {
            try {
                PREVIEW_TEXTURES.put(consoleRoom, urlToTexture(new URL(consoleRoom.getImageUrl().get())));
            } catch (MalformedURLException e) {
                e.printStackTrace();
            }
        });

        thread.run();
        return PREVIEW_TEXTURES.get(consoleRoom);
    }

}
