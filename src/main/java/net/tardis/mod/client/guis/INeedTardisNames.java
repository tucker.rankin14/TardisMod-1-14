package net.tardis.mod.client.guis;

import java.util.Map;

import net.minecraft.util.ResourceLocation;

public interface INeedTardisNames {

	void setNamesFromServer(Map<ResourceLocation, String> nameMap);
}
