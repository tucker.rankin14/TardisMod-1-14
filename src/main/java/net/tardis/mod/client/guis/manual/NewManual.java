package net.tardis.mod.client.guis.manual;

import com.google.common.collect.Lists;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.mojang.blaze3d.matrix.MatrixStack;
import com.mojang.datafixers.util.Pair;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.screen.Screen;
import net.minecraft.client.gui.widget.button.ChangePageButton;
import net.minecraft.resources.IResource;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.text.ITextComponent;
import net.minecraft.util.text.StringTextComponent;
import net.tardis.mod.Tardis;
import net.tardis.mod.client.ClientHelper;

import java.io.IOException;
import java.io.InputStreamReader;
import java.util.List;

public class NewManual extends Screen {

    public static final ResourceLocation TEXTURE = new ResourceLocation(Tardis.MODID, "textures/gui/manual.png");
    private List<Chapter> chapters = Lists.newArrayList();

    private int pageIndex = 0;
    private int chapterIndex = 0;

    protected NewManual(ITextComponent titleIn) {
        super(titleIn);
        this.read();
    }

    public NewManual(){
        this(new StringTextComponent("Manual"));
    }

    @Override
    protected void init() {
        super.init();

        this.buttons.clear();

        this.addButton(new ChangePageButton(width / 2 + 85, height / 2 + 45, true, button -> {
                this.turnPage(true);
        }, true));

        this.addButton(new ChangePageButton(width / 2 - 110, height / 2 + 45, false, button -> {
            this.turnPage(false);
        }, true));

    }

    @Override
    public void render(MatrixStack matrixStack, int mouseX, int mouseY, float partialTicks) {
        this.renderBackground(matrixStack);
        Minecraft.getInstance().getTextureManager().bindTexture(TEXTURE);
        this.blit(matrixStack, (width - 256) / 2, (height - 187) / 2, 0, 0, 256, 187);

        super.render(matrixStack, mouseX, mouseY, partialTicks);

        int x = width / 2 - 110, y = height / 2 - 70;

        //Render Pages
        Pair<Page, Page> pages = this.getPages();
        if(pages.getFirst() != null) {
            pages.getFirst().render(matrixStack, this.font, x, y, this.width, this.height);
        }
        if(pages.getSecond() != null){
            getPages().getSecond().render(matrixStack, this.font, x + 120, y, this.width, this.height);
        }

    }

    public void turnPage(boolean forward){

        //if can change page in same chapter
        Chapter currentChapter = this.getChapter();
        if(currentChapter == null){
            this.chapterIndex = 0;
            return;
        }

        if(forward){
            if(this.pageIndex + 2 < currentChapter.getPages().size()){
                this.pageIndex += 2;
            }
            //If outside the chapter
            else{
                //Try to get next chapter
                if(this.chapterIndex + 1 < this.chapters.size()){
                    this.chapterIndex += 1;
                    this.pageIndex = 0;
                }
            }
        }
        //If turning back
        else {
            //If still in chapter
            if(this.pageIndex - 2 >= 0)
                this.pageIndex -= 2;
            //if out of chapter
            else{
                //try to get the previous chapter
                if(this.chapterIndex - 1 >= 0){
                    this.chapterIndex -= 1;
                    this.pageIndex = this.getChapter().getPages().size() - 1;
                }
            }
        }

    }

    public Chapter getChapter(){
        if(this.chapterIndex < this.chapters.size())
            return this.chapters.get(this.chapterIndex);
        return null;
    }

    public Pair<Page, Page> getPages(){
        Chapter chapter = this.getChapter();
        if(chapter != null && this.pageIndex < chapter.getPages().size()){

            Page page2 = null;

            if(this.pageIndex + 1 < chapter.getPages().size())
                page2 = chapter.getPages().get(this.pageIndex + 1);

            return new Pair(this.getChapter().getPages().get(this.pageIndex), page2);
        }
        return new Pair(null, null);
    }

    private void read(){
        try {
            IResource resource = Minecraft.getInstance().getResourceManager().getResource(new ResourceLocation(Tardis.MODID, "manual/manual.json"));

            JsonObject root = new JsonParser().parse(new InputStreamReader(resource.getInputStream())).getAsJsonObject();

            //Pull all chapters
            this.chapters.clear();

            for(JsonElement ele : root.get("chapters").getAsJsonArray()){

                ResourceLocation chapterLoc = this.contructChapterRL(new ResourceLocation(ele.getAsString()));

                this.chapters.add(Chapter.read(chapterLoc, ClientHelper.getResourceAsJson(chapterLoc)));
            }

        }
        catch(IOException e){
            e.printStackTrace();
        }
    }

    private ResourceLocation contructChapterRL(ResourceLocation loc){
        return new ResourceLocation(loc.getNamespace(), "manual/chapters/" + loc.getPath() + ".json");
    }
}