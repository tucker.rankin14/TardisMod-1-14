package net.tardis.mod.client.guis.monitors;

import com.google.common.collect.Lists;
import com.mojang.blaze3d.matrix.MatrixStack;
import it.unimi.dsi.fastutil.booleans.BooleanConsumer;
import net.minecraft.util.IReorderingProcessor;
import net.minecraft.util.text.CharacterManager;
import net.minecraft.util.text.ITextComponent;
import net.minecraft.util.text.ITextProperties;
import net.minecraft.util.text.StringTextComponent;
import net.minecraft.util.text.Style;
import net.minecraft.util.text.TranslationTextComponent;

import java.util.Collection;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;


/**
 * A Basic Confirmation GUI that can be used to make user confirm their decision
 *
 * @implSpec Create a method in your parent gui that will open this GUI, then call the method in one of your buttons
 */
public class MonitorConfirmScreen extends MonitorScreen {


    private final List<ITextProperties> listLines = Lists.newArrayList();
    protected TranslationTextComponent message1;
    protected TranslationTextComponent message2;
    protected TranslationTextComponent confirmButtonText;
    protected TranslationTextComponent cancelButtonText;
    protected BooleanConsumer callbackFunction;

    public MonitorConfirmScreen(IMonitorGui monitor, String type) {
        super(monitor, type);
    }

    public MonitorConfirmScreen(IMonitorGui monitor, String type, BooleanConsumer callbackFunction, TranslationTextComponent message1, TranslationTextComponent message2, TranslationTextComponent confirmButtonText, TranslationTextComponent cancelButtonText) {
        super(monitor, type);
        this.callbackFunction = callbackFunction;
        this.message1 = message1;
        this.message2 = message2;
        this.confirmButtonText = confirmButtonText;
        this.cancelButtonText = cancelButtonText;
    }

    @Override
    protected void init() {
        super.init();
        this.buttons.clear();
        this.listLines.clear();
        this.addSubmenu(this.confirmButtonText, (action) -> {
            this.callbackFunction.accept(true); //Confirm button
        });
        this.addSubmenu(this.cancelButtonText, (action) -> {
            this.callbackFunction.accept(false); //Cancel button
        });
        
        
        List<ITextProperties> list1 = this.minecraft.fontRenderer.getCharacterManager().func_238362_b_(this.message1, (this.parent.getMaxX() - this.parent.getMinX()), Style.EMPTY);
        List<ITextProperties> list2 = this.minecraft.fontRenderer.getCharacterManager().func_238362_b_(this.message2, (this.parent.getMaxX() - this.parent.getMinX()), Style.EMPTY);
        this.listLines.addAll(list1);
        this.listLines.addAll(list2);
    }

    @Override
    public void render(MatrixStack matrixStack, int mouseX, int mouseY, float p_render_3_) {
        super.render(matrixStack, mouseX, mouseY, p_render_3_);
        AtomicInteger addToDrawHeight = new AtomicInteger(20);
        this.listLines.forEach(message -> {
        	StringTextComponent mes = new StringTextComponent(message.getString());
            drawCenteredString(matrixStack, this.font,
            		mes,
                    this.parent.getMinX() + 100,
                    this.parent.getMaxY()
                            + addToDrawHeight.getAndAdd(this.font.FONT_HEIGHT), //Returns the previous value and adds a value to the old value
                    0xFFFFFF);

        });
    }

}
