package net.tardis.mod.client.guis.monitors;

import java.util.ArrayList;
import java.util.List;

import net.minecraft.client.Minecraft;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.text.StringTextComponent;
import net.minecraft.util.text.TranslationTextComponent;
import net.tardis.mod.client.animation.ExteriorAnimationEntry;
import net.tardis.mod.client.guis.widgets.TextButton;
import net.tardis.mod.constants.Constants;
import net.tardis.mod.helper.TardisHelper;
import net.tardis.mod.misc.TexVariant;
import net.tardis.mod.network.Network;
import net.tardis.mod.network.packets.ChangeExtAnimationMessage;
import net.tardis.mod.network.packets.ChangeExtVarMessage;
import net.tardis.mod.registries.ExteriorAnimationRegistry;
import net.tardis.mod.registries.TardisRegistries;
import net.tardis.mod.tileentities.ConsoleTile;

public class ExteriorPropMonitorScreen extends MonitorScreen {

    public static final TranslationTextComponent land_code = new TranslationTextComponent(Constants.Strings.GUI_PROTOCOL_TITLE + "exterior_properties.land_code");
    public static final TranslationTextComponent ext_var = new TranslationTextComponent(Constants.Strings.GUI_PROTOCOL_TITLE + "exterior_properties.exterior_var");
    TexVariant[] variants;
    private List<ExteriorAnimationEntry> anims = new ArrayList<ExteriorAnimationEntry>();
    private int texVarIndex = 0;
    private TextButton anim;
    private TextButton varButton;
    private int index = 0;

    public ExteriorPropMonitorScreen(IMonitorGui mon) {
        super(mon, "exterior_prop");
    }

    @Override
    protected void init() {
        super.init();

        ConsoleTile console = null;
        TileEntity te = this.minecraft.world.getTileEntity(TardisHelper.TARDIS_POS);
        if (te instanceof ConsoleTile)
            console = (ConsoleTile) te;
        else return;

        variants = console.getExteriorType().getVariants();
        this.texVarIndex = console.getExteriorManager().getExteriorVariant();

        anims.clear();
        anims.addAll(ExteriorAnimationRegistry.EXTERIOR_ANIMATION_REGISTRY.get().getValues());

        this.addButton(anim = (TextButton) this.createButton(this.parent.getMinX(), this.parent.getMinY(),
                new TranslationTextComponent("gui.exterior.prop.anim"),
                but -> mod(1)));

        if (this.variants != null && this.variants.length > 0)
            this.addButton(this.varButton = (TextButton) this.createButton(this.parent.getMinX(), this.parent.getMinY(), variants[0].getTranslation(), but -> this.modTexVar(1)));

        this.addButton(this.createButton(this.parent.getMinX(), this.parent.getMinY(), land_code, but -> Minecraft.getInstance().displayGuiScreen(new LandCodeScreen(this.parent))));

        modTexVar(0);

        for (int i = 0; i < this.anims.size(); ++i) {
            if (this.anims.get(i).getRegistryName().equals(tile.getExteriorManager().getExteriorAnimation())) {
                this.index = i;
                this.anim.setMessage(new StringTextComponent("> " + this.anims.get(i).getDisplayName().getString()));
                break;
            }
        }

    }

    public void mod(int i) {
        if (index + i >= anims.size())
            index = 0;
        else if (index + i < 0)
            index = anims.size() - 1;
        else index += i;

        ExteriorAnimationEntry type = this.anims.get(this.index);

        anim.setMessage(new StringTextComponent("> " + type.getDisplayName().getString()));
        tile.getExteriorManager().setExteriorAnimation(type.getRegistryName());
        Network.sendToServer(new ChangeExtAnimationMessage(type.getRegistryName()));
    }

    public void modTexVar(int i) {
        if (this.variants == null)
            return;

        if (this.texVarIndex + i >= this.variants.length)
            this.texVarIndex = 0;
        else if (this.texVarIndex + i < 0)
            this.texVarIndex = this.variants.length - 1;
        else this.texVarIndex += i;

        TexVariant tex = this.variants[this.texVarIndex];
        String varText = ext_var.getString() + tex.getTranslation().getString();
        this.varButton.setMessage(new StringTextComponent(varText));
        this.varButton.setWidth(this.font.getStringWidth(varText));

        if (i != 0)
            Network.sendToServer(new ChangeExtVarMessage(this.texVarIndex));

    }

    @Override
    public int getUsedHeight() {
        return 0;
    }

}
