package net.tardis.mod.world.dimensions;


import java.util.ArrayList;
import java.util.List;
import java.util.OptionalLong;
import java.util.function.BiFunction;

import com.mojang.serialization.Lifecycle;

import net.minecraft.client.world.ClientWorld;
import net.minecraft.server.MinecraftServer;
import net.minecraft.tags.BlockTags;
import net.minecraft.util.RegistryKey;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.registry.DynamicRegistries;
import net.minecraft.util.registry.MutableRegistry;
import net.minecraft.util.registry.Registry;
import net.minecraft.world.Dimension;
import net.minecraft.world.DimensionType;
import net.minecraft.world.World;
import net.minecraft.world.biome.Biome;
import net.minecraft.world.biome.FuzzedBiomeMagnifier;
import net.minecraft.world.gen.DimensionSettings;
import net.minecraft.world.server.ServerWorld;
import net.minecraftforge.event.world.WorldEvent;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.Mod.EventBusSubscriber.Bus;
import net.tardis.mod.Tardis;
import net.tardis.mod.helper.DimensionHelper;
//import net.tardis.mod.world.data.TardisWorldData;
import net.tardis.mod.world.data.TardisWorldData;

/** Main class for creating and registering Dimension related objects
 * <br> These can be replaced by Json datapacks if needed
 * */
@Mod.EventBusSubscriber(modid = Tardis.MODID, bus = Bus.MOD)
public class TDimensions {
    
	//List of Tardis interior dimensions to keep track off, and reregister old ones if they are present.
	public static List<RegistryKey<World>> TARDIS_DIMENSIONS = new ArrayList<RegistryKey<World>>();
	
	public static final RegistryKey<World> VORTEX_DIM = RegistryKey.getOrCreateKey(Registry.WORLD_KEY, new ResourceLocation(Tardis.MODID, "vortex"));
	public static final RegistryKey<World> MOON_DIM = RegistryKey.getOrCreateKey(Registry.WORLD_KEY, new ResourceLocation(Tardis.MODID, "moon"));
	public static final RegistryKey<World> SPACE_DIM = RegistryKey.getOrCreateKey(Registry.WORLD_KEY, new ResourceLocation(Tardis.MODID, "space"));
	
	//DimensionRenderInfo (SkyProperty) Resource Locations
	public static final ResourceLocation TARDIS_SKY_PROPERTY_KEY = new ResourceLocation(Tardis.MODID, "tardis_sky_property");
	public static final ResourceLocation VORTEX_SKY_PROPERTY_KEY = new ResourceLocation(Tardis.MODID, "vortex_sky_property");
	public static final ResourceLocation MOON_SKY_PROPERTY_KEY = new ResourceLocation(Tardis.MODID, "moon_sky_property");
	public static final ResourceLocation SPACE_SKY_PROPERTY_KEY = new ResourceLocation(Tardis.MODID, "space_sky_property");
    
	//NoiseSettings (Bad Name by MCP) - Defines things like blocks used in chunk gen, what structures to generate
    public static RegistryKey<DimensionSettings> TARDIS_NOISE_SETTINGS;
    public static RegistryKey<DimensionSettings> MOON_NOISE_SETTINGS;
    public static RegistryKey<DimensionSettings> VORTEX_NOISE_SETTINGS;
    public static RegistryKey<DimensionSettings> SPACE_NOISE_SETTINGS;
   

	public static class DimensionTypes{
		//This is the DimensionType we use for the Tardis Dimension
		//It's statically initialised because we don't want users to be able to modify it via datapacks
		public static final RegistryKey<DimensionType> TARDIS_TYPE = RegistryKey.getOrCreateKey(Registry.DIMENSION_TYPE_KEY, Tardis.TARDIS_LOC);
	    
	    //Currently we statically init the Moon, Vortex and Space dimension types for dev purposes
	    //If we want users to be able to customise them, we need to make jsons, keep the RegistryKey, and comment out the actual DimensionType instance
	    public static final RegistryKey<DimensionType> MOON_TYPE = RegistryKey.getOrCreateKey(Registry.DIMENSION_TYPE_KEY, new ResourceLocation(Tardis.MODID,"moon"));
	    
	    public static final RegistryKey<DimensionType> VORTEX_TYPE = RegistryKey.getOrCreateKey(Registry.DIMENSION_TYPE_KEY, new ResourceLocation(Tardis.MODID, "vortex"));
	    
	   //Currently we statically init the Moon, Vortex and Space dimension types for dev purposes
	   //If we want users to be able to customise them, we need to make jsons, keep the RegistryKey, and comment out the actual DimensionType instance
	    public static final RegistryKey<DimensionType> SPACE_TYPE = RegistryKey.getOrCreateKey(Registry.DIMENSION_TYPE_KEY, new ResourceLocation(Tardis.MODID,"space"));
	}
	
	/** ===Registry Methods Start=== */

    /**
     * Register DimensionSettings (Called Noise Settings by MCP) in FMLCommonSetup Event
     */
    public static void registerNoiseSettings() {
    	MOON_NOISE_SETTINGS = RegistryKey.getOrCreateKey(Registry.NOISE_SETTINGS_KEY, new ResourceLocation(Tardis.MODID, "moon"));
    	VORTEX_NOISE_SETTINGS = RegistryKey.getOrCreateKey(Registry.NOISE_SETTINGS_KEY, new ResourceLocation(Tardis.MODID, "vortex"));
    	SPACE_NOISE_SETTINGS = RegistryKey.getOrCreateKey(Registry.NOISE_SETTINGS_KEY, new ResourceLocation(Tardis.MODID, "space"));
    	TARDIS_NOISE_SETTINGS = RegistryKey.getOrCreateKey(Registry.NOISE_SETTINGS_KEY, Tardis.TARDIS_LOC);
    }
    
    /**
     * Register ChunkGenerator types so we can use them in our dimensions.
     * <br> Call in FMLCommonSetup, so that when FMLServerStarting occurs, these will be registered
     */
    public static void registerChunkGenerators() {
        Registry.register(Registry.CHUNK_GENERATOR_CODEC, Tardis.TARDIS_LOC, TardisChunkGenerator.providerCodec);
        Registry.register(Registry.CHUNK_GENERATOR_CODEC, new ResourceLocation(Tardis.MODID, "vortex"), VortexChunkGenerator.providerCodec);
        Registry.register(Registry.CHUNK_GENERATOR_CODEC, new ResourceLocation(Tardis.MODID, "space"), SpaceChunkGenerator.providerCodec);
    }
    
    /** ===Registry Methods End=== */
    
     
    /** ====Builder Methods Start==== */
    
    
    /**
     * Creates an instance of a Tardis Dimension. 
     * Required as Dimension is a final Class
     *
     * @param server
     * @param dimensionKey
     * @return
     */
    public static Dimension tardisDimensionBuilder(MinecraftServer server, RegistryKey<Dimension> dimensionKey) {
        long seed = server.getWorld(World.OVERWORLD).getSeed() + dimensionKey.getLocation().hashCode();
        DynamicRegistries registries = server.getDynamicRegistries(); // get dynamic registries
        Registry<DimensionSettings> noiseRegistry = registries.getRegistry(Registry.NOISE_SETTINGS_KEY);
        Registry<Biome> biomeRegistry = registries.getRegistry(Registry.BIOME_KEY);
        return new Dimension(
                () -> registries.getRegistry(Registry.DIMENSION_TYPE_KEY).getOrThrow(DimensionTypes.TARDIS_TYPE), //use getOrThrow, do not use getOptionalValue as it's physical client side only
                new TardisChunkGenerator(server));
    }
    
    /** ====Builder Methods End==== */
    
    
    
    /* Helper Functions*/
    
    /**
     * Register or Get a ServerWorld (Dimension instance) for STATICALLY initialised dimensions
     * <br> DO NOT use this for dynamically created dimensions like Tardis dimensions
     * @param server
     * @return
     */
    public static ServerWorld registerOrGetDimensionStatic(String name, MinecraftServer server, BiFunction<MinecraftServer, RegistryKey<Dimension>, Dimension> dimension) {
    	ResourceLocation loc = new ResourceLocation(Tardis.MODID, name);
    	RegistryKey<World> worldKey = RegistryKey.getOrCreateKey(Registry.WORLD_KEY, loc);
    	ServerWorld newWorld = DimensionHelper.getOrCreateWorldStatic(server, worldKey, dimension);
    	return newWorld;
    }
    
    /**
     * Register or Get a ServerWorld (Dimension instance) for STATICALLY initialised dimensions
     * <br> DO NOT use this for dynamically created dimensions like Tardis dimensions
     * @param worldKey
     * @param server
     * @param dimension
     * @return
     */
    public static ServerWorld registerOrGetDimensionStatic(RegistryKey<World> worldKey, MinecraftServer server, BiFunction<MinecraftServer, RegistryKey<Dimension>, Dimension> dimension) {
    	ServerWorld newWorld = DimensionHelper.getOrCreateWorldStatic(server, worldKey, dimension);
    	return newWorld;
    }
    
    /**
     * Dyanamically Register or get a Tardis interior dimension
     *
     * @param name
     * @param server
     * @return
     */
    public static ServerWorld registerOrGetTardisDim(String name, MinecraftServer server) {
        ResourceLocation loc = new ResourceLocation(Tardis.MODID, name);
        RegistryKey<World> worldKey = RegistryKey.getOrCreateKey(Registry.WORLD_KEY, loc);
        ServerWorld newWorld = DimensionHelper.getOrCreateWorld(server, worldKey, TDimensions::tardisDimensionBuilder);
        if (server.getWorld(worldKey) != null)
            return newWorld;
        TARDIS_DIMENSIONS.add(worldKey);
        if (Events.DATA != null)
            Events.DATA.markDirty();
        return newWorld;
    }


    /// Takes previously created TARDISs and updates them to the new system.
    public static void registerOldTardises(MinecraftServer server) {
        for (RegistryKey<World> world : TARDIS_DIMENSIONS) {
            if (server.getWorld(world) == null) {
                DimensionHelper.getOrCreateWorldStatic(server, world, TDimensions::tardisDimensionBuilder);
                if (!TDimensions.TARDIS_DIMENSIONS.contains(world))
                    TDimensions.TARDIS_DIMENSIONS.add(world);
            }
        }
    }


    @Mod.EventBusSubscriber(modid = Tardis.MODID)
    public static class Events {

        static TardisWorldData DATA;

        @SubscribeEvent
        public static void saveTardisDimensions(WorldEvent.Save event) {
            if (!(event.getWorld() instanceof ServerWorld))
                return;
            else {
                ServerWorld serverWorld = (ServerWorld) event.getWorld();
                if (serverWorld.getDimensionKey() == World.OVERWORLD) {
                    ((ServerWorld) event.getWorld()).getSavedData().set(DATA);
                }
            }
        }

        @SubscribeEvent
        public static void load(WorldEvent.Load event) {
            if (!(event.getWorld() instanceof ServerWorld))
                return;
            else {
                ServerWorld serverWorld = (ServerWorld) event.getWorld();
                if (serverWorld.getDimensionKey() == World.OVERWORLD) {
                    DATA = ((ServerWorld) event.getWorld()).getSavedData().getOrCreate(TardisWorldData::new, "tardis");
                    registerOldTardises(serverWorld.getServer());
                }
            }
        }

        @SubscribeEvent
        public static void unload(WorldEvent.Unload event) {
            if (!(event.getWorld() instanceof ServerWorld))
                return;
            ServerWorld serverWorld = (ServerWorld) event.getWorld();
            if (serverWorld.getDimensionKey() == World.OVERWORLD) {
                TDimensions.TARDIS_DIMENSIONS.clear();
            }
        }
    }
}