package net.tardis.mod.compat.jei;

import java.util.ArrayList;
import java.util.List;

import com.google.common.collect.Lists;

import mezz.jei.api.constants.VanillaTypes;
import mezz.jei.api.gui.IRecipeLayout;
import mezz.jei.api.gui.drawable.IDrawable;
import mezz.jei.api.helpers.IGuiHelper;
import mezz.jei.api.ingredients.IIngredients;
import mezz.jei.api.recipe.category.IRecipeCategory;
import net.minecraft.item.ItemStack;
import net.minecraft.item.Items;
import net.minecraft.util.ResourceLocation;
import net.tardis.mod.Tardis;
import net.tardis.mod.blocks.TBlocks;
import net.tardis.mod.items.TItems;
import net.tardis.mod.recipe.AlembicRecipe;

public class AlembicRecipeCategory implements IRecipeCategory<AlembicRecipe> {

	public static final ResourceLocation NAME = new ResourceLocation(Tardis.MODID, "alembic");
	private IDrawable background;
	private IDrawable icon;
	
	public AlembicRecipeCategory(IGuiHelper help){
		background = help.createDrawable(new ResourceLocation(Tardis.MODID, "textures/gui/containers/alembic.png"),
				4, 3, 169, 67);
		icon = help.createDrawableIngredient(new ItemStack(TBlocks.alembic.get()));
	}
	
	@Override
	public ResourceLocation getUid() {
		return NAME;
	}

	@Override
	public Class<AlembicRecipe> getRecipeClass() {
		return AlembicRecipe.class;
	}

	@Override
	public String getTitle() {
		return "Alembic";
	}

	@Override
	public IDrawable getBackground() {
		return this.background;
	}

	@Override
	public IDrawable getIcon() {
		return this.icon;
	}

	@Override
	public void setIngredients(AlembicRecipe recipe, IIngredients ingre) {
		ingre.setInputIngredients(Lists.newArrayList(recipe.getrequiredBurnableIngredient()));
		if(!recipe.getResult().isEmpty())
			ingre.setOutput(VanillaTypes.ITEM, recipe.getResult());
		else ingre.setOutput(VanillaTypes.ITEM, new ItemStack(TItems.MERCURY_BOTTLE.get()));
	}

	@Override
	public void setRecipe(IRecipeLayout recipeLayout, AlembicRecipe recipe, IIngredients ingre) {
		recipeLayout.getItemStacks().init(0, false, 20, 7);
		recipeLayout.getItemStacks().set(0, new ItemStack(Items.WATER_BUCKET));
		
		recipeLayout.getItemStacks().init(1, false, 20, 43);
		recipeLayout.getItemStacks().set(1, new ItemStack(Items.BUCKET));
		
		recipeLayout.getItemStacks().init(2, true, 57, 7);
		List<ItemStack> resultStacks = Lists.newArrayList(recipe.getrequiredBurnableIngredient().getMatchingStacks());
		List<ItemStack> displayStacks = new ArrayList<>();
		for (ItemStack stack : resultStacks) {
			stack.setCount(recipe.getRequiredIngredientCount());
			displayStacks.add(stack);
		}
		recipeLayout.getItemStacks().set(2, displayStacks);
		
		recipeLayout.getItemStacks().init(3, false, 57, 43);
		recipeLayout.getItemStacks().set(3, new ItemStack(Items.COAL));
		
		recipeLayout.getItemStacks().init(4, false, 135, 43);
		recipeLayout.getItemStacks().set(4, recipe.getResult().isEmpty() ? new ItemStack(TItems.MERCURY_BOTTLE.get()) : recipe.getResult());
	}

}
