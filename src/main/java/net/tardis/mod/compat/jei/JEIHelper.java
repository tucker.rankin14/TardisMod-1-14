package net.tardis.mod.compat.jei;

import mezz.jei.api.gui.IRecipeLayout;
import net.minecraft.item.ItemStack;

public class JEIHelper {
	
	public static void addInputSlot(IRecipeLayout layout, int id, int x, int y, ItemStack stack) {
		if(!stack.isEmpty()) {
			layout.getItemStacks().init(id, true, x, y);
			layout.getItemStacks().set(id, stack);
		}
	}

}
