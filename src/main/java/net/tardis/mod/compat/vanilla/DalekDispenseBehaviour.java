package net.tardis.mod.compat.vanilla;

import net.minecraft.block.DispenserBlock;
import net.minecraft.dispenser.DefaultDispenseItemBehavior;
import net.minecraft.dispenser.IBlockSource;
import net.minecraft.item.ItemStack;
import net.minecraft.tags.FluidTags;
import net.minecraft.util.Direction;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;

/**
 * Created by Swirtzly
 * on 19/04/2020 @ 12:57
 */

//TODO THINK ABOUT REMOVAL/FIX
public class DalekDispenseBehaviour extends DefaultDispenseItemBehavior {

    private final ResourceLocation resourceLocation;
    private final DefaultDispenseItemBehavior dispenseItemBehaviour = new DefaultDispenseItemBehavior();

    public DalekDispenseBehaviour(ResourceLocation dalekType) {
        this.resourceLocation = dalekType;
    }

    public static void init() {
    }

    public ItemStack dispenseStack(IBlockSource source, ItemStack stack) {
        Direction direction = source.getBlockState().get(DispenserBlock.FACING);
        World world = source.getWorld();
        double d0 = source.getX() + (double) ((float) direction.getXOffset() * 1.125F);
        double d1 = source.getY() + (double) ((float) direction.getYOffset() * 1.125F);
        double d2 = source.getZ() + (double) ((float) direction.getZOffset() * 1.125F);
        BlockPos blockpos = source.getBlockPos().offset(direction);
        double d3 = 0;
        if (world.getFluidState(blockpos).isTagged(FluidTags.WATER)) {
            d3 = 1.0D;
        }
//        DalekEntity dalekEntity = new DalekEntity(world);
//        dalekEntity.setDalekType(resourceLocation);
//        dalekEntity.rotationYaw = direction.getHorizontalAngle();
//        dalekEntity.setPosition(d0, d1 + d3, d2);
//        world.addEntity(dalekEntity);
        stack.shrink(1);
        return stack;
    }
}
