package net.tardis.mod.commands.subcommands;

import java.util.Optional;

import com.mojang.brigadier.context.CommandContext;
import com.mojang.brigadier.exceptions.CommandSyntaxException;

import net.minecraft.command.CommandSource;
import net.minecraft.entity.player.ServerPlayerEntity;
import net.tardis.mod.Tardis;
import net.tardis.mod.helper.TardisHelper;
import net.tardis.mod.tileentities.ConsoleTile;
/** Base class for commands that need a ConsoleTile context.
 * <p> If you need the command to have different subcommands or different behaviours, make your own private static methods and call each method where needed
 * <br> See SetupCommand or UnlockCommand for the examples of the former.*/
public abstract class ConsoleTileCommand extends TCommand{

	@Override
	public int run(CommandContext<CommandSource> context) throws CommandSyntaxException {
		try {
			context.getSource().assertIsEntity();
			ServerPlayerEntity executor = context.getSource().asPlayer();
			Optional<ConsoleTile> tile = TardisHelper.getConsoleInWorld(executor.getServerWorld());
			tile.ifPresent(console -> runCommand(executor, console));
			
			if(!tile.isPresent())
				context.getSource().sendErrorMessage(MUST_BE_IN_TARDIS);
			
		}
		catch(CommandSyntaxException exeception) {
			Tardis.LOGGER.catching(exeception);
		}
		return 0;
	}

	public abstract void runCommand(ServerPlayerEntity executor, ConsoleTile tile);
	

}
