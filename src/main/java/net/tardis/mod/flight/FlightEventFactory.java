package net.tardis.mod.flight;

import java.util.ArrayList;
import java.util.function.Supplier;

import net.minecraft.util.ResourceLocation;
import net.minecraftforge.registries.ForgeRegistryEntry;
import net.tardis.mod.tileentities.ConsoleTile;
/** The factory wrapper class to allow {@linkplain FlightEvent}(s) to be registered
 * <br> This is the unique reference of a specific {@linkplain FlightEvent}*/
public class FlightEventFactory extends ForgeRegistryEntry<FlightEventFactory>{

	private IFlightEventFactory<FlightEvent> factory;
	private Supplier<ArrayList<ResourceLocation>> sequence;
	private boolean isNormal = true;
	
	public FlightEventFactory(IFlightEventFactory<FlightEvent> event, Supplier<ArrayList<ResourceLocation>> sequence) {
		this.factory = event;
		this.sequence = sequence;
	}
	
	public FlightEventFactory(IFlightEventFactory<FlightEvent> event, Supplier<ArrayList<ResourceLocation>> sequence, boolean normal) {
		this(event, sequence);
		this.isNormal = normal;
	}
	
	public FlightEvent create(ConsoleTile tile) {
		FlightEvent event = this.factory.create(this, this.sequence.get());
//		event.setRegistryName(getRegistryName());
		event.calcTime(tile);
		return event;
	}
	
	public boolean isNormal() {
		return this.isNormal;
	}
	
	public void setNormal(boolean normal) {
		this.isNormal = normal;
	}
	
	public interface IFlightEventFactory<T extends FlightEvent> {

        T create(FlightEventFactory entry, ArrayList<ResourceLocation> resources);
    }

}
