package net.tardis.mod.flight;

import java.util.List;

import net.minecraft.util.ResourceLocation;
import net.tardis.mod.tileentities.ConsoleTile;

public class ResidualArtronEvent extends FlightEvent{

	public ResidualArtronEvent(FlightEventFactory entry, List<ResourceLocation> controls) {
		super(entry, controls);
	}

	@Override
	public boolean onComplete(ConsoleTile tile) {
		boolean success = tile.getFlightEvent() == null || tile.getFlightEvent().getControls().isEmpty();
		if(success) {
			tile.setArtron(tile.getArtron() + (float)((10 + ConsoleTile.rand.nextDouble() * 10)));
			tile.getEmotionHandler().addLoyalty(tile.getPilot(), 1);
		}
		return success;
	}
}
