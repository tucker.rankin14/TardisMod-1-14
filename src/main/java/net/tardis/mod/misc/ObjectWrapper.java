package net.tardis.mod.misc;

public class ObjectWrapper<T> {

    T val;

    public ObjectWrapper(T val) {
        this.val = val;
    }

    public static <X> ObjectWrapper<X> create(X defaultVal) {
        return new ObjectWrapper<X>(defaultVal);
    }

    public T getValue() {
        return val;
    }

    public void setValue(T val) {
        this.val = val;
    }
}
