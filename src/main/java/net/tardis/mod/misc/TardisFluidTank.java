package net.tardis.mod.misc;

import net.minecraftforge.fluids.FluidStack;
import net.minecraftforge.fluids.IFluidTank;
import net.minecraftforge.fluids.capability.IFluidHandler;
import net.minecraftforge.fluids.capability.templates.FluidTank;

import java.util.function.Supplier;

public class TardisFluidTank implements IFluidHandler, IFluidTank {

    Supplier<FluidTank> delegate;

    public TardisFluidTank(Supplier<FluidTank> tank) {
        this.delegate = tank;
    }

    @Override
    public FluidStack getFluid() {
        return this.delegate.get().getFluid();
    }

    @Override
    public int getFluidAmount() {
        return this.delegate.get().getFluidAmount();
    }

    @Override
    public int getCapacity() {
        return this.delegate.get().getCapacity();
    }

    @Override
    public boolean isFluidValid(FluidStack stack) {
        return true;
    }

    @Override
    public int fill(FluidStack resource, FluidAction action) {
        return this.delegate.get().fill(resource, action);
    }

    @Override
    public FluidStack drain(int maxDrain, FluidAction action) {
        return this.delegate.get().drain(maxDrain, action);
    }

    @Override
    public FluidStack drain(FluidStack resource, FluidAction action) {
        return this.drain(resource, action);
    }

    @Override
    public int getTanks() {
        return 1;
    }

    @Override
    public FluidStack getFluidInTank(int tank) {
        return this.delegate.get().getFluid();
    }

    @Override
    public int getTankCapacity(int tank) {
        return this.delegate.get().getCapacity();
    }

    @Override
    public boolean isFluidValid(int tank, FluidStack stack) {
        return true;
    }

}
