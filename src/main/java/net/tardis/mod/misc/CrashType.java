package net.tardis.mod.misc;

public enum CrashType {

    DEFAULT(50, 10.0F, true);

    private float componentDamage;
    private int inaccuracy;
    private boolean makeExteriorEffects;

    CrashType(int inaccuracy, float damage, boolean exteriorEffects){
        this.inaccuracy = inaccuracy;
        this.componentDamage = damage;
        this.makeExteriorEffects = exteriorEffects;
    }

    public float getComponentDamage(){
        return this.componentDamage;
    }

    public int getInaccuracy() {
        return this.inaccuracy;
    }

    public boolean shouldDoExteriorEffects(){
        return this.makeExteriorEffects;
    }
}
