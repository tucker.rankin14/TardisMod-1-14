package net.tardis.mod.misc;
/*
The MIT License (MIT)
Copyright (c) 2020 Joseph Bettendorff a.k.a. "Commoble"
Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:
The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.
THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
 */

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.function.Consumer;
import java.util.function.Function;

import javax.annotation.Nonnull;

import org.apache.commons.io.IOUtils;
import org.apache.logging.log4j.Logger;

import com.google.common.collect.Maps;
import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;
import com.mojang.serialization.Codec;
import com.mojang.serialization.JsonOps;

import net.minecraft.client.resources.ReloadListener;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.entity.player.ServerPlayerEntity;
import net.minecraft.profiler.IProfiler;
import net.minecraft.resources.IResource;
import net.minecraft.resources.IResourceManager;
import net.minecraft.util.JSONUtils;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.event.entity.player.PlayerEvent;
import net.minecraftforge.fml.LogicalSide;
import net.minecraftforge.fml.LogicalSidedProvider;
import net.minecraftforge.fml.network.PacketDistributor;
import net.minecraftforge.fml.network.simple.SimpleChannel;

/**
 * Generic Data loader for Json -> Object.
 * Merges multiple datapacks togethor
 * @param <RAW> The type of the objects that the codec is parsing jsons as
 * @param <FINE> The type of the object we get after merging the parsed objects. Can be the same as RAW
 * <br>References: 
 * <br>https://github.com/Commoble/databuddy/blob/1.16.3/src/main/java/commoble/databuddy/data/MergeableCodecDataManager.java
 * <br>Minecraft JsonListener
 */
public class MergeableCodecJsonDataListener<RAW, FINE> extends ReloadListener<Map<ResourceLocation, FINE>>{
	
	
	protected static final String JSON_EXTENSION = ".json";
	protected static final int JSON_EXTENSION_LENGTH = JSON_EXTENSION.length();
	protected static final Gson STANDARD_GSON = new Gson();
	
	private final String folderName;
	private final Logger logger;
	private final Codec<RAW> codec;
	private final Function<List<RAW>, FINE> merger;
	private final Gson gson;
	
	private Optional<Runnable> syncDataOnReload = Optional.empty();
	
	@Nonnull
	/** Mutable, non-null map containing whatever data was loaded last time server datapacks were loaded **/ 
	public Map<ResourceLocation, FINE> data = new HashMap<>();
	
	/**
	 * @param folderName - the name of the folder we wish to look for json files under
	 * @param codec - codec used 
	 * @param logger
	 * @param gson
	 */
	public MergeableCodecJsonDataListener(final String folderName, final Logger logger, Codec<RAW> codec, final Function<List<RAW>, FINE> merger, final Gson gson) {
		this.folderName = folderName;
		this.logger = logger;
		this.codec = codec;
		this.merger = merger;
		this.gson = gson;
	}
	
	public MergeableCodecJsonDataListener(final String folderName, final Logger logger, Codec<RAW> codec, final Function<List<RAW>, FINE> merger) {
		this(folderName, logger, codec, merger, STANDARD_GSON);
	}
	
	@Override
	protected Map<ResourceLocation, FINE> prepare(IResourceManager resourceManager, IProfiler profiler) {
		final Map<ResourceLocation, List<RAW>> map = Maps.newHashMap();
		for(ResourceLocation resourceLocation : resourceManager.getAllResourceLocations(this.folderName, MergeableCodecJsonDataListener::isStringJsonFile)){
			final String namespace = resourceLocation.getNamespace();
			final String filePath = resourceLocation.getPath();
			final String dataPath = filePath.substring(this.folderName.length() + 1, filePath.length() - JSON_EXTENSION_LENGTH); 
			
			//Registry Name for the object made up of the json file name and namespace
			//We will be using this to register our Java object with this ID
			final ResourceLocation jsonObjectID = new ResourceLocation(namespace, dataPath);
			//List of all json objects with the same resource location, possible from multiple datapacks 
			final List<RAW> unmergedRaws = new ArrayList<>();
			
			try {
				for (IResource resource : resourceManager.getAllResources(resourceLocation)) {
					//Follow vanilla logic
					try
					(
							final InputStream inputstream = resource.getInputStream();
				            final Reader reader = new BufferedReader(new InputStreamReader(inputstream, StandardCharsets.UTF_8));
					)
					{
						// read the json file and save the parsed object for later
						// this json element may return null
						final JsonElement jsonElement = JSONUtils.fromJson(this.gson, reader, JsonElement.class);
						this.codec.parse(JsonOps.INSTANCE, jsonElement)
							// resultOrPartial either returns a non-empty optional or calls the consumer given
							//This is similar to manually handling isLeft and isRight
						    .resultOrPartial(MergeableCodecJsonDataListener::throwJsonParseException)
							.ifPresent(unmergedRaws::add);
					}
					catch(RuntimeException | IOException exception){
						this.logger.error("Data loader for {} could not read data {} from file {} in data pack {}", this.folderName, jsonObjectID, resourceLocation, resource.getPackName(), exception); 
					}
					finally {
						IOUtils.closeQuietly(resource);
					}
					map.put(jsonObjectID, unmergedRaws);
				}
			}
			catch (IOException exception) {
				this.logger.error("Data loader for {} could not read data {} from file {}", this.folderName, jsonObjectID, resourceLocation, exception);
			}
		}
		return MergeableCodecJsonDataListener.mapValues(map, this.merger::apply);
	}

	@Override
	protected void apply(final Map<ResourceLocation, FINE> processedData, final IResourceManager resourceManager, final IProfiler profiler) {
		this.logger.info("Begin loading of data for data loader: {}", this.folderName);
		this.data = processedData;
		this.logger.info("Data loader for {} loaded {} finalised jsons", this.folderName, this.data.size());
		
		// hacky server test until we can find a better way to do this
		boolean isServer = true;
		try {
			LogicalSidedProvider.INSTANCE.get(LogicalSide.SERVER);
		}
		catch (NullPointerException e){
			isServer = false;
		}
		if (isServer){
			// if we're on the server and we are configured to send syncing packets, send syncing packets
			this.syncDataOnReload.ifPresent(Runnable::run);
		}
	}
	
	static <Key, In, Out> Map<Key, Out> mapValues(final Map<Key,In> inputs, final Function<In, Out> mapper) {
		final Map<Key,Out> newMap = new HashMap<>();
		
		inputs.forEach((key, input) -> newMap.put(key, mapper.apply(input)));
		
		return newMap;
	}
	
	public <PACKET> MergeableCodecJsonDataListener<RAW, FINE> subscribeAsSyncable(final SimpleChannel channel,
			final Function<Map<ResourceLocation, FINE>, PACKET> packetFactory){
			MinecraftForge.EVENT_BUS.addListener(this.getLoginListener(channel, packetFactory));
			this.syncDataOnReload = Optional.of(() -> channel.send(PacketDistributor.ALL.noArg(), packetFactory.apply(this.data)));
			return this;
	}
	
	private <PACKET> Consumer<PlayerEvent.PlayerLoggedInEvent> getLoginListener(final SimpleChannel channel,
			final Function<Map<ResourceLocation, FINE>, PACKET> packetFactory){
			return event -> {
				PlayerEntity player = event.getPlayer();
			if (player instanceof ServerPlayerEntity){
				channel.send(PacketDistributor.PLAYER.with(() -> (ServerPlayerEntity)player), packetFactory.apply(this.data));
			}
		};
	}
	
	private static boolean isStringJsonFile(final String filename) {
		return filename.endsWith(JSON_EXTENSION);
	}
	
	private static void throwJsonParseException(final String codecParseFailure) {
		throw new JsonParseException(codecParseFailure);
	}

}
