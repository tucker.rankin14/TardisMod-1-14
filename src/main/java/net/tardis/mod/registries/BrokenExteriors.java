package net.tardis.mod.registries;

import net.minecraft.util.ResourceLocation;
import net.tardis.mod.Tardis;
import net.tardis.mod.ars.ConsoleRoom;
import net.tardis.mod.blocks.ConsoleBlock;
import net.tardis.mod.blocks.TBlocks;
import net.tardis.mod.misc.BrokenExteriorType;

public enum BrokenExteriors {

    WATER(new BrokenExteriorType(() -> ExteriorRegistry.TRUNK.get(), () -> ConsoleRegistry.NEMO.get(), () -> ConsoleRoom.ABANDONED_PANAMAX, () -> new ConsoleRoom[]{ConsoleRoom.PANAMAX})),
    STEAM(new BrokenExteriorType(() -> ExteriorRegistry.STEAMPUNK.get(), () -> ConsoleRegistry.STEAM.get(), () -> ConsoleRoom.ABANDONED_STEAM, () -> new ConsoleRoom[]{ConsoleRoom.STEAM}));

    private BrokenExteriorType type;

    BrokenExteriors(BrokenExteriorType type) {
        this.type = type;
    }

    public BrokenExteriorType getType() {
        return this.type;
    }
}
