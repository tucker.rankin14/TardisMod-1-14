package net.tardis.mod.registries;

import java.util.function.Supplier;

import net.minecraftforge.fml.RegistryObject;
import net.minecraftforge.registries.DeferredRegister;
import net.minecraftforge.registries.IForgeRegistry;
import net.minecraftforge.registries.RegistryBuilder;
import net.tardis.mod.Tardis;
import net.tardis.mod.ars.ConsoleRoom;
import net.tardis.mod.schematics.ExteriorUnlockSchematic;
import net.tardis.mod.schematics.InteriorUnlockSchematic;
import net.tardis.mod.schematics.Schematic;

public class SchematicRegistry {
	
    public static final DeferredRegister<Schematic> SCHEMATICS = DeferredRegister.create(Schematic.class, Tardis.MODID);
	
	public static Supplier<IForgeRegistry<Schematic>> SCHEMATIC_REGISTRY = SCHEMATICS.makeRegistry("schematic", () -> new RegistryBuilder<Schematic>().setMaxID(Integer.MAX_VALUE - 1));
	
	public static final RegistryObject<Schematic> POLICE_BOX_EXTERIOR = SCHEMATICS.register("police_box_exterior", () -> new ExteriorUnlockSchematic(() -> ExteriorRegistry.POLICE_BOX.get()));
	public static final RegistryObject<Schematic> POLICE_BOX_MODERN_EXTERIOR = SCHEMATICS.register("modern_police_box_exterior", () -> new ExteriorUnlockSchematic(() -> ExteriorRegistry.MODERN_POLICE_BOX.get()));
	//Interior Schematics
	public static final RegistryObject<Schematic> TOYOTA_INTERIOR = SCHEMATICS.register("toyota_interior", () -> new InteriorUnlockSchematic(() -> ConsoleRoom.TOYOTA));
	public static final RegistryObject<Schematic> CORAL_INTERIOR = SCHEMATICS.register("coral_interior", () -> new InteriorUnlockSchematic(() -> ConsoleRoom.CORAL));
	public static final RegistryObject<Schematic> NAUTILUS_INTERIOR = SCHEMATICS.register("nautilus_interior", () -> new InteriorUnlockSchematic(() -> ConsoleRoom.NAUTILUS));
	public static final RegistryObject<Schematic> JADE_INTERIOR = SCHEMATICS.register("jade_interior", () -> new InteriorUnlockSchematic(() -> ConsoleRoom.JADE));
	public static final RegistryObject<Schematic> STEAM_INTERIOR = SCHEMATICS.register("steam_interior", () -> new InteriorUnlockSchematic(() -> ConsoleRoom.STEAM));
	public static final RegistryObject<Schematic> ENVOY_INTERIOR = SCHEMATICS.register("envoy_interior", () -> new InteriorUnlockSchematic(() -> ConsoleRoom.ENVOY));
	public static final RegistryObject<Schematic> ALABASTER_INTERIOR = SCHEMATICS.register("alabaster_interior", () -> new InteriorUnlockSchematic(() -> ConsoleRoom.ALABASTER));
	public static final RegistryObject<Schematic> IMPERIAL_INTERIOR = SCHEMATICS.register("imperial_interior", () -> new InteriorUnlockSchematic(() -> ConsoleRoom.IMPERIAL));
	public static final RegistryObject<Schematic> OMEGA_INTERIOR = SCHEMATICS.register("omega_interior", () -> new InteriorUnlockSchematic(() -> ConsoleRoom.OMEGA));
	

}
