package net.tardis.mod.helper;

import java.util.Optional;

import javax.annotation.Nullable;

import net.minecraft.util.RegistryKey;
import net.minecraft.util.registry.Registry;
import net.minecraft.world.World;

public class NBTHelper {

	@Nullable
	public static RegistryKey<World> getRegistryKey(World world, String string) {
		Optional<RegistryKey<World>> optional = world.func_241828_r().getRegistry(Registry.WORLD_KEY).getOptionalKey(world);
		if(optional.isPresent())
			return optional.get();
		return null;
	}

}
