package net.tardis.mod.helper;

import java.util.Optional;
import java.util.UUID;

import com.mojang.authlib.GameProfile;

import net.minecraft.util.math.BlockPos;
import net.minecraft.util.text.StringTextComponent;
import net.minecraft.util.text.TextComponent;
import net.minecraft.util.text.TextFormatting;
import net.minecraft.util.text.event.ClickEvent;
import net.minecraft.util.text.event.HoverEvent;
import net.minecraft.world.server.ServerWorld;
import net.tardis.mod.cap.Capabilities;
import net.tardis.mod.cap.ITardisWorldData;
import net.tardis.mod.tileentities.ConsoleTile;

public class TextHelper {

    public static TextComponent createTextComponentWithTip(String text, String tooltipText) {
        //Always surround tool tip items with brackets
        TextComponent textComponent = new StringTextComponent("[" + text + "]");
        textComponent.modifyStyle(style -> {
            return style.setFormatting(TextFormatting.GREEN)//color tool tip items green
                    .setHoverEvent(new HoverEvent(HoverEvent.Action.SHOW_TEXT, new StringTextComponent(tooltipText)))
                    .setClickEvent(new ClickEvent(ClickEvent.Action.COPY_TO_CLIPBOARD, tooltipText));
        });
        return textComponent;
    }
    
    /** Creates a Text Component but doesn't allow copying of the tooltip text. Instead, will copy the text in the text component*/
    public static TextComponent createTextWithoutTooltipCopying(String text, String tooltipText) {
        //Always surround tool tip items with brackets
        TextComponent textComponent = new StringTextComponent("[" + text + "]");
        textComponent.modifyStyle(style -> {
            return style.setFormatting(TextFormatting.GREEN)//color tool tip items green
                    .setHoverEvent(new HoverEvent(HoverEvent.Action.SHOW_TEXT, new StringTextComponent(tooltipText)))
                    .setClickEvent(new ClickEvent(ClickEvent.Action.COPY_TO_CLIPBOARD, text));
        });
        return textComponent;
    }
    
    public static TextComponent createTextWithoutTooltip(String text) {
        //Always surround tool tip items with brackets
        TextComponent textComponent = new StringTextComponent("[" + text + "]");
        textComponent.modifyStyle(style -> {
            return style.setFormatting(TextFormatting.GREEN)//color tool tip items green
                    .setClickEvent(new ClickEvent(ClickEvent.Action.COPY_TO_CLIPBOARD, text));
        });
        return textComponent;
    }

    /**
     * For getting tardis tool tip when you already have the data there
     */
    public static TextComponent getTardisTextObject(ServerWorld world, ITardisWorldData data) {
        return createTextComponentWithTip(data.getTARDISName(), world.getDimensionKey().getLocation().toString());
    }

    /**
     * Gets a Tardis UUID based dimension id and also shows tooltip which displays the Tardis display name
     * <br> If clicked, allows user to copy the dimension id
     * @param world
     * @param data
     * @return
     */
    public static TextComponent getTardisDimObject(ServerWorld world, ITardisWorldData data) {
        return createTextWithoutTooltipCopying(world.getDimensionKey().getLocation().toString(), data.getTARDISName());
    }

    /**
     * For getting tardis tool tip when you only have the world related to it.
     */
    public static TextComponent getTardisTextObject(ServerWorld world) {
        Optional<ConsoleTile> console = TardisHelper.getConsoleInWorld(world);

        final TextComponent[] tardisTextObject = new TextComponent[1];

        console.ifPresent(tile -> tile.getWorld().getCapability(Capabilities.TARDIS_DATA)
                .ifPresent(data -> tardisTextObject[0] = getTardisTextObject(world, data)));

        return tardisTextObject != null // if we were able to retrieve it
                ? tardisTextObject[0] // then use it
                : new StringTextComponent(world.getDimensionKey().getLocation().toString());
    }
    
    /** Gets the Tardis UUID based dimension id and its corresponding name in a tooltip if we only have a world context*/
    public static TextComponent getTardisDimObject(ServerWorld world) {
        Optional<ConsoleTile> console = TardisHelper.getConsoleInWorld(world);

        final TextComponent[] tardisTextObject = new TextComponent[1];

        console.ifPresent(tile -> tile.getWorld().getCapability(Capabilities.TARDIS_DATA)
                .ifPresent(data -> tardisTextObject[0] = getTardisDimObject(world, data)));

        return tardisTextObject[0] != null // if we were able to retrieve it
                ? tardisTextObject[0] // then use it
                : new StringTextComponent(world.getDimensionKey().getLocation().toString());
    }


    /**
     * For getting player tool tip. If player has been online since server start, then will show their display name.
     */
    public static TextComponent getPlayerTextObject(ServerWorld world, UUID id) {
        GameProfile profileByUUID = world.getServer().getPlayerProfileCache().getProfileByUUID(id);
        String playerName = profileByUUID != null ? profileByUUID.getName() : "OFFLINE Player";
        return createTextComponentWithTip(playerName, id.toString());
    }
    
    public static TextComponent getBlockPosObject(BlockPos pos) {
    	String formattedPos = WorldHelper.formatBlockPosDebug(pos);
    	return createTextWithoutTooltip(formattedPos);
    }
}
