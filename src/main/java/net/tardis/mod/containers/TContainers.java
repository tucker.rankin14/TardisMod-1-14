package net.tardis.mod.containers;

import net.minecraft.inventory.container.Container;
import net.minecraft.inventory.container.ContainerType;
import net.minecraftforge.fml.RegistryObject;
import net.minecraftforge.fml.network.IContainerFactory;
import net.minecraftforge.registries.DeferredRegister;
import net.minecraftforge.registries.ForgeRegistries;
import net.tardis.mod.Tardis;


public class TContainers {
    
    public static final DeferredRegister<ContainerType<?>> CONTAINERS = DeferredRegister.create(ForgeRegistries.CONTAINERS, Tardis.MODID);
    
    public static final RegistryObject<ContainerType<QuantiscopeSonicContainer>> QUANTISCOPE = CONTAINERS.register("quantiscope", () -> registerContainer(QuantiscopeSonicContainer::new));
    public static final RegistryObject<ContainerType<QuantiscopeWeldContainer>> QUANTISCOPE_WELD = CONTAINERS.register("quantiscope_weld", () -> registerContainer(QuantiscopeWeldContainer::new));
    public static final RegistryObject<ContainerType<AlembicContainer>> ALEMBIC = CONTAINERS.register("alembic", () -> registerContainer(AlembicContainer::new));
    public static final RegistryObject<ContainerType<ReclamationContainer>> RECLAMATION_UNIT = CONTAINERS.register("reclaimation_unit", () -> registerContainer(ReclamationContainer::new));
    public static final RegistryObject<ContainerType<ShipComputerContainer>> SHIP_COMPUTER = CONTAINERS.register("ship_computer", () -> registerContainer(ShipComputerContainer::new));
    public static final RegistryObject<ContainerType<VMContainer>> VORTEX_M_BATTERY = CONTAINERS.register("vm_battery", () -> registerContainer(VMContainer::new));
    public static final RegistryObject<ContainerType<EngineContainer>> ENGINE = CONTAINERS.register("engine", () -> registerContainer(EngineContainer::new));
    public static final RegistryObject<ContainerType<WaypointBankContainer>> WAYPOINT = CONTAINERS.register("waypoint", () -> registerContainer(WaypointBankContainer::new));
    public static final RegistryObject<ContainerType<SpectrometerContainer>> SPECTROMETER = CONTAINERS.register("spectrometer", () -> registerContainer(SpectrometerContainer::new));
    
    
//    
//    public static <T extends Container> ContainerType<T> register(IContainerFactory<T> fact, String name){
//        ContainerType<T> type = new ContainerType<T>(fact);
//        type.setRegistryName(new ResourceLocation(Tardis.MODID, name));
//        return type;
//    }
    
    public static <T extends Container> ContainerType<T> registerContainer(IContainerFactory<T> fact){
        ContainerType<T> type = new ContainerType<T>(fact);
        return type;
    }
    

}
