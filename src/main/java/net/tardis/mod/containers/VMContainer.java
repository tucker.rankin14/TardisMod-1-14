package net.tardis.mod.containers;

import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.entity.player.PlayerInventory;
import net.minecraft.inventory.container.Container;
import net.minecraft.inventory.container.ContainerType;
import net.minecraft.inventory.container.Slot;
import net.minecraft.item.ItemStack;
import net.minecraft.network.PacketBuffer;
import net.tardis.mod.artron.IArtronBattery;
import net.tardis.mod.cap.Capabilities;
import net.tardis.mod.containers.slot.SlotItemHandlerFiltered;

/**
 * Created by 50ap5ud5
 * on 22 Apr 2020 @ 8:12:47 pm
 */
public class VMContainer extends Container implements IVortexUtil{
	
	private ItemStack vortex;
	
    public VMContainer(ContainerType<?> type, int id) {
	    super(type, id);
    }
    /** Client Only constructor */
	public VMContainer(int id, PlayerInventory playerInventoryIn, PacketBuffer buf) {
		 this(id, playerInventoryIn, buf.readItemStack());
	}
	
	/** Server Only constructor */
	public VMContainer(int id, PlayerInventory playerInventoryIn, ItemStack stack) {
		super(TContainers.VORTEX_M_BATTERY.get(),id);
		
		this.vortex = stack;
		
	     int i = (this.getNumRows() - 4) * 18;
	      
	     stack.getCapability(Capabilities.VORTEX_MANIP).ifPresent(cap -> {
	    	 //The actual Container Slots
		     for(int j = 0; j < this.getNumRows(); ++j) {
		        for(int k = 0; k < 3; ++k) {
		           this.addSlot(new SlotItemHandlerFiltered(cap.getItemHandler(), k + j * 3, 8 + k * 18 + 54, 58 + j * 18, item -> item.getItem() instanceof IArtronBattery));
		        }
		     }
	     });
	   
	      
	      //Player inventory
	     for(int l = 0; l < 3; ++l) {
	        for(int j1 = 0; j1 < 9; ++j1) {
	           this.addSlot(new Slot(playerInventoryIn, j1 + l * 9 + 9, 8 + j1 * 18, 143 + l * 18 + i));
	        }
	     }
	      
	     //Player Hotbar
	     for(int i1 = 0; i1 < 9; ++i1) {
	        this.addSlot(new Slot(playerInventoryIn, i1, 8 + i1 * 18, 201 + i));
	     }
	}
	
	@Override
	public boolean canInteractWith(PlayerEntity playerIn) {
		return true;
	}
	
	@Override
	public ItemStack transferStackInSlot(PlayerEntity playerIn, int index) {
		ItemStack itemstack = ItemStack.EMPTY;
		Slot slot = this.inventorySlots.get(index);
		if (slot != null && slot.getHasStack()) {
			ItemStack itemstack1 = slot.getStack();
			itemstack = itemstack1.copy();
			if (index < this.getNumRows() * 3) {
				if (!this.mergeItemStack(itemstack1, this.getNumRows() * 3, this.inventorySlots.size(), true)) {
					return ItemStack.EMPTY;
				}
			} else if (!this.mergeItemStack(itemstack1, 0, this.getNumRows() * 3, false)) {
				return ItemStack.EMPTY;
			}
			
			if (itemstack1.isEmpty()) {
				slot.putStack(ItemStack.EMPTY);
			} else {
				slot.onSlotChanged();
			}
		}

		return itemstack;
	}
	
	public ItemStack getStack() {
		return this.vortex;
	}
	
	@Override
	public int getNumRows() {
		return 1;
	}

}
