package net.tardis.mod.containers;

import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.entity.player.PlayerInventory;
import net.minecraft.inventory.container.Container;
import net.minecraft.inventory.container.ContainerType;
import net.minecraft.inventory.container.Slot;
import net.minecraft.item.ItemStack;
import net.minecraft.network.PacketBuffer;
import net.minecraft.tileentity.TileEntity;
import net.minecraftforge.items.SlotItemHandler;
import net.tardis.mod.helper.TInventoryHelper;
import net.tardis.mod.tileentities.machines.NeutronicSpectrometerTile;

import javax.annotation.Nullable;

public class SpectrometerContainer extends Container {

    private NeutronicSpectrometerTile tile;

    protected SpectrometerContainer(@Nullable ContainerType<?> type, int id) {
        super(type, id);
    }

    public SpectrometerContainer(int id){
        this(TContainers.SPECTROMETER.get(), id);
    }

    /** Server Only constructor */
    public SpectrometerContainer(int id, PlayerInventory inv, NeutronicSpectrometerTile tile){
        this(id);
        init(inv, tile);
    }

    /** Client Only constructor */
    public SpectrometerContainer(int id, PlayerInventory inv, PacketBuffer buf){
        this(id);

        TileEntity te = inv.player.world.getTileEntity(buf.readBlockPos());

        if(te instanceof NeutronicSpectrometerTile)
            init(inv, (NeutronicSpectrometerTile)te);

    }

    public void init(PlayerInventory inv, NeutronicSpectrometerTile tile){
        this.tile = tile;


        this.addSlot(new SlotItemHandler(tile.getInventory(), 0, 41, 27));
        this.addSlot(new SlotItemHandler(tile.getInventory(), 1, 147, 47));

        for(Slot s : TInventoryHelper.createPlayerInv(inv, 0, -2)){
            this.addSlot(s);
        }

    }

    @Override
    public void updateProgressBar(int id, int data) {
        super.updateProgressBar(id, data);
    }

    @Override
    public ItemStack transferStackInSlot(PlayerEntity playerIn, int index) {
        return ItemStack.EMPTY;
    }

    @Override
    public boolean canInteractWith(PlayerEntity playerIn) {
        return true;
    }

    public float getProgress() {
        return tile == null ? 0 : tile.getProgress();
    }

    public float getDownloadProgress() {
        return tile == null ? 0 : tile.getDownloadProgress();
    }

    public boolean hasSchematics(){
        return tile == null  ? false : tile.hasSchematics();
    }
}
