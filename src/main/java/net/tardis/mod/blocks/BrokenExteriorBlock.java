package net.tardis.mod.blocks;

import java.util.Random;

import net.minecraft.block.Block;
import net.minecraft.block.BlockState;
import net.minecraft.block.material.Material;
import net.minecraft.client.particle.ParticleManager;
import net.minecraft.entity.Entity;
import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.item.*;
import net.minecraft.particles.ParticleTypes;
import net.minecraft.state.StateContainer.Builder;
import net.minecraft.state.properties.BlockStateProperties;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.ActionResultType;
import net.minecraft.util.Direction;
import net.minecraft.util.Hand;
import net.minecraft.util.Rotation;
import net.minecraft.util.SoundCategory;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.BlockRayTraceResult;
import net.minecraft.util.math.RayTraceResult;
import net.minecraft.world.World;
import net.minecraft.world.server.ServerWorld;
import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.api.distmarker.OnlyIn;
import net.tardis.mod.helper.WorldHelper;
import net.tardis.mod.itemgroups.TItemGroups;
import net.tardis.mod.misc.IDontBreak;
import net.tardis.mod.sounds.TSounds;
import net.tardis.mod.tileentities.BrokenExteriorTile;
import net.tardis.mod.world.dimensions.TDimensions;

public class BrokenExteriorBlock extends TileBlock implements IDontBreak {

    public BlockItem ITEM = new BlockItemBrokenExterior(this);

    public BrokenExteriorBlock() {
        super(Properties.create(Material.BARRIER).hardnessAndResistance(9999F));
        this.setDefaultState(this.stateContainer.getBaseState().with(BlockStateProperties.WATERLOGGED, false));
    }

    @Override
    public ActionResultType onBlockActivated(BlockState state, World worldIn, BlockPos pos, PlayerEntity player, Hand handIn, BlockRayTraceResult hit) {

        if (worldIn.isRemote)
            return ActionResultType.CONSUME;

        ItemStack stack = player.getHeldItem(handIn);
        TileEntity te = worldIn.getTileEntity(pos);
        
        if (te instanceof BrokenExteriorTile) {
            //is the player trying to force entry?
            //check for empty hand
            if(stack.isEmpty()) {
                //then run the check for whether that player is holding the leads of any animals.
                if (LeadItem.bindPlayerMobs(player, worldIn, pos) == ActionResultType.SUCCESS){
                    //if we successfully leashed the animal(s) to the tardis, start angry forced tame.
                    ((BrokenExteriorTile) te).startForceTame(player);
                }
            }

        	if(((BrokenExteriorTile) te).onPlayerRightClick(player, stack)) {
        		//Consume item
        		if(!player.isCreative())
        			player.getHeldItem(handIn).shrink(1);
        	}
        }

        return ActionResultType.SUCCESS;
    }

    @Override
    @OnlyIn(Dist.CLIENT)
    public void animateTick(BlockState stateIn, World worldIn, BlockPos pos, Random rand) {
        if (rand.nextDouble() < 0.05) {
            for (int i = 0; i < 18; ++i) {
                double angle = Math.toRadians(i * 20);
                double x = Math.sin(angle);
                double z = Math.cos(angle);


                worldIn.playSound(pos.getX(), pos.getY(), pos.getZ(), TSounds.ELECTRIC_SPARK.get(), SoundCategory.BLOCKS, 0.05F, 1F, false);
                worldIn.addParticle(ParticleTypes.LAVA, pos.getX() + 0.5 + x, pos.getY() + rand.nextDouble(), pos.getZ() + 0.5 + z, 0, 0, 0);
            }
        }

    }

    @Override
    protected void fillStateContainer(Builder<Block, BlockState> builder) {
        builder.add(BlockStateProperties.HORIZONTAL_FACING, BlockStateProperties.WATERLOGGED);
    }

    @Override
    public BlockState getStateForPlacement(BlockItemUseContext context) {
        if (context.getPlayer() != null)
            return this.getDefaultState().with(BlockStateProperties.HORIZONTAL_FACING, context.getPlayer().getHorizontalFacing().getOpposite());
        return this.getDefaultState();
    }


    //This have been added to "increase" immersion, by removing running particles and breaking particles etc
    @Override
    public boolean addLandingEffects(BlockState state1, ServerWorld worldserver, BlockPos pos, BlockState state2, LivingEntity entity, int numberOfParticles) {
        return false;
    }

    @Override
    public boolean addRunningEffects(BlockState state, World world, BlockPos pos, Entity entity) {
        return false;
    }

    @Override
    public boolean addHitEffects(BlockState state, World worldObj, RayTraceResult target, ParticleManager manager) {
        return false;
    }

    @Override
    public boolean addDestroyEffects(BlockState state, World world, BlockPos pos, ParticleManager manager) {
        return false;
    }

	@Override
	public void onBlockAdded(BlockState state, World worldIn, BlockPos pos, BlockState oldState, boolean isMoving) {
		super.onBlockAdded(state, worldIn, pos, oldState, isMoving);
		TileEntity te = worldIn.getTileEntity(pos);
		if(te instanceof BrokenExteriorTile) {
			((BrokenExteriorTile)te).randomizeType();
		}
	}

	@Override
	public BlockState rotate(BlockState state, Rotation rot) {
		return state.with(BlockStateProperties.HORIZONTAL_FACING, rot.rotate(state.get(BlockStateProperties.HORIZONTAL_FACING)));
	}


	public static class BlockItemBrokenExterior extends BlockItem {

        public BlockItemBrokenExterior(Block blockIn) {
            super(blockIn, new Item.Properties().group(TItemGroups.MAIN));
        }

        @Override
        protected boolean canPlace(BlockItemUseContext cont, BlockState state) {
            return cont.getFace() == Direction.UP && cont.getWorld().getBlockState(cont.getPos().up()).isReplaceable(cont) && !WorldHelper.areDimensionTypesSame(cont.getWorld(), TDimensions.DimensionTypes.TARDIS_TYPE);
        }

        @Override
        protected boolean placeBlock(BlockItemUseContext context, BlockState state) {
            return context.getWorld().setBlockState(context.getPos().up(), state);
        }

    }


}
