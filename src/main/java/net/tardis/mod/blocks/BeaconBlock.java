package net.tardis.mod.blocks;

import net.minecraft.block.Block;
import net.minecraft.block.BlockState;
import net.minecraft.block.Blocks;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.fluid.FluidState;
import net.minecraft.fluid.Fluids;
import net.minecraft.item.BlockItemUseContext;
import net.minecraft.particles.ParticleTypes;
import net.minecraft.state.StateContainer.Builder;
import net.minecraft.state.properties.BlockStateProperties;
import net.minecraft.tags.FluidTags;
import net.minecraft.util.*;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.BlockRayTraceResult;
import net.minecraft.util.math.shapes.ISelectionContext;
import net.minecraft.util.math.shapes.VoxelShape;
import net.minecraft.util.text.TranslationTextComponent;
import net.minecraft.world.IBlockReader;
import net.minecraft.world.IWorld;
import net.minecraft.world.World;
import net.tardis.mod.Tardis;
import net.tardis.mod.constants.Constants;
import net.tardis.mod.helper.WorldHelper;
import net.tardis.mod.helper.PlayerHelper;
import net.tardis.mod.helper.TardisHelper;
import net.tardis.mod.misc.SpaceTimeCoord;
import net.tardis.mod.properties.Prop;
import net.tardis.mod.subsystem.AntennaSubsystem;
import net.tardis.mod.tileentities.console.misc.DistressSignal;

public class BeaconBlock extends Block {

    public BeaconBlock() {
        super(Prop.Blocks.BASIC_TECH.get().notSolid());
        this.setDefaultState(this.getDefaultState().with(BlockStateProperties.WATERLOGGED, false));
    }

    @Override
    public ActionResultType onBlockActivated(BlockState state, World worldIn, BlockPos pos, PlayerEntity player, Hand handIn, BlockRayTraceResult hit) {
        
            if (!worldIn.isRemote && handIn == player.getActiveHand()) {
            	if (WorldHelper.canTravelToDimension(player.world)) {
	            	player.getServer().getWorlds().forEach(serverWorld -> {
	            		if (serverWorld.getDimensionKey().getLocation().getPath().contentEquals(Tardis.MODID)) { //TODO: Verify this works
	                        TardisHelper.getConsoleInWorld(serverWorld).ifPresent(tile -> {
	                            tile.getSubsystem(AntennaSubsystem.class).ifPresent(ant -> {
	                                if (ant.canBeUsed()) {
	                                    tile.addDistressSignal(new DistressSignal(player.getDisplayName().getString() + " sent signal", new SpaceTimeCoord(player.world.getDimensionKey(), pos)));
	                                }
	                            });
	                        });
	                    }
	            	});
	            	PlayerHelper.sendMessageToPlayer(player, new TranslationTextComponent("message.tardis.beacon_sent"), true);
	                worldIn.setBlockState(pos, Blocks.AIR.getDefaultState());
	                worldIn.playSound(null, pos, SoundEvents.ENTITY_FIREWORK_ROCKET_LAUNCH, SoundCategory.BLOCKS, 1F, 1F);
                }
            	else player.sendStatusMessage(Constants.Translations.CANT_USE_IN_DIM, true);
            }
            if (worldIn.isRemote) {
                worldIn.setBlockState(pos, Blocks.AIR.getDefaultState());
                for (int y = 0; y < 120; ++y) {
                    worldIn.addParticle(ParticleTypes.CLOUD, pos.getX() + 0.5, pos.getY() + (y / 2.0), pos.getZ() + 0.5, 0, 0.05, 0);
                }
            }
        return ActionResultType.SUCCESS;
    }

    @Override
    public VoxelShape getShape(BlockState state, IBlockReader worldIn, BlockPos pos, ISelectionContext context) {
        return Block.makeCuboidShape(6.02, 0, 6.02, 9.98, 3.96, 9.98);
    }

    @Override
    public BlockState getStateForPlacement(BlockItemUseContext context) {
        FluidState fluid = context.getWorld().getFluidState(context.getPos());
        return this.getDefaultState().with(BlockStateProperties.HORIZONTAL_FACING, context.getPlayer().getHorizontalFacing().getOpposite()).with(BlockStateProperties.WATERLOGGED, fluid.getFluidState().isTagged(FluidTags.WATER));
    }

    @Override
    protected void fillStateContainer(Builder<Block, BlockState> builder) {
        builder.add(BlockStateProperties.HORIZONTAL_FACING);
        builder.add(BlockStateProperties.WATERLOGGED);
    }

    @Override
    public FluidState getFluidState(BlockState state) {
        return state.get(BlockStateProperties.WATERLOGGED) ? Fluids.WATER.getStillFluidState(false) : Fluids.EMPTY.getDefaultState();
    }

    @Override
    public BlockState updatePostPlacement(BlockState stateIn, Direction facing, BlockState facingState, IWorld worldIn, BlockPos currentPos, BlockPos facingPos) {
        BlockState state = stateIn;
        if (state.get(BlockStateProperties.WATERLOGGED)) {
            worldIn.getPendingFluidTicks().scheduleTick(currentPos, Fluids.WATER, Fluids.WATER.getTickRate(worldIn));
        }
        return state;
    }
}
