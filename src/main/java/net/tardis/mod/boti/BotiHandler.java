package net.tardis.mod.boti;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import net.minecraft.block.BlockState;
import net.minecraft.entity.Entity;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.math.AxisAlignedBB;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;
import net.tardis.mod.boti.stores.AbstractEntityStorage;
import net.tardis.mod.boti.stores.BlockStore;
import net.tardis.mod.boti.stores.EntityStorage;
import net.tardis.mod.boti.stores.TileStore;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Author: Spectre0987
 * A class-agnostic object for handleing BOTI stuff
 */
public class BotiHandler {

    private List<TileStore> tileStores = Lists.newArrayList();
    private AxisAlignedBB area;
    IBotiEnabled parent;

    public BotiHandler(IBotiEnabled parent){
        this.parent = parent;
    }

    public BotiHandler(IBotiEnabled parent, AxisAlignedBB area){
        this(parent);
        this.setArea(area);
    }

    public BotiHandler(IBotiEnabled parent, int diameter){
        this(parent);
        this.setDiameter(diameter);
    }

    public void setArea(AxisAlignedBB area){
        this.area = area;
    }

    /**
     * Set a square area to gather blocks from
     * @param diameter
     */
    public void setDiameter(int diameter){
        this.setArea(AxisAlignedBB.withSizeAtOrigin(diameter, diameter, diameter));
    }

    /**
     * For things to be centered, like scanner mode
     * @param world
     * @param center
     * @return
     */
    public boolean updateBoti(World world, BlockPos center){
        return this.updateBoti(world, center, BlockPos.ZERO);
    }

    /**
     *
     * @param otherWorld
     * @param center -
     * @param offset - For things like doors, so we can ignore blocks behind them
     * @return
     */
    public boolean updateBoti(World otherWorld, BlockPos center, BlockPos offset){
        //Must be on server
        if(!otherWorld.isRemote){
            //Setup World Shell if missing
            if(this.parent.getBotiWorld() == null){
                this.parent.setBotiWorld(new WorldShell(center, otherWorld));
            }

            if(!this.parent.getBotiWorld().getOffset().equals(center))
                this.parent.getBotiWorld().setOffset(center);

            //Gather blocks
            Map<BlockPos, BlockStore> newBlocks = this.gatherBlocks(otherWorld, center, area.offset(offset));
            this.parent.getBotiWorld().getMap().putAll(newBlocks);

            //Cull blocks out of range
            this.parent.getBotiWorld().cullOutOfRangeBlocks();

            return true;
        }
        return false;
    }

    /**
     * Gets all blocks and tiles in the area
     * @param world
     * @param pos
     * @param area
     * @return
     */
    public Map<BlockPos, BlockStore> gatherBlocks(World world, BlockPos pos, AxisAlignedBB area){

        //Make sure this is not a Mutable BlockPos, that will break this function
        pos = pos.toImmutable();

        Map<BlockPos, BlockStore> map = new HashMap<BlockPos, BlockStore>();

        //Clear pending tile messages
        this.tileStores.clear();

        for(int x = (int)Math.floor(area.minX); x < area.maxX; ++x){
            for(int y = (int)Math.floor(area.minY); y < area.maxY; ++y){
                for(int z = (int)Math.floor(area.minZ); z < area.maxZ; ++z){
                    BlockPos newPos = pos.add(x, y, z);
                    BlockState state = world.getBlockState(newPos);
                    if(!state.isAir()){
                        map.put(newPos, new BlockStore(world, newPos));
                    }

                    //Gather Tiles
                    TileEntity tile = world.getTileEntity(newPos);
                    if(tile != null) {
                        this.tileStores.add(new TileStore(newPos, tile));
                    }
                }
            }
        }

        return map;
    }

    /**
     * Returns the difference in block stores between the two maps
     * @param original
     * @param newList
     * @return
     */
    public Map<BlockPos, BlockStore> getDiff(Map<BlockPos, BlockStore> original, Map<BlockPos, BlockStore> newList){
        Map<BlockPos, BlockStore> diff = Maps.newHashMap();

        for(Map.Entry<BlockPos, BlockStore> entry : newList.entrySet()){
            //If position is new
            if(!original.containsKey(entry.getKey())){
                diff.put(entry.getKey(), entry.getValue());
            }
            // if it existed, but the block is different now
            else if(!original.get(entry.getKey()).equals(entry.getValue())){
                diff.put(entry.getKey(), entry.getValue());
            }
            //If neither, do not add
        }

        return diff;
    }

    public List<AbstractEntityStorage> gatherEntities(World world, BlockPos center, BlockPos offset){
        List<AbstractEntityStorage> list = Lists.newArrayList();

        //Normal Entities
        for(Entity e : world.getEntitiesWithinAABB(Entity.class, this.area.offset(center).offset(offset))){
            //If this has an entity type
            if(e.getType() != null){

                //If this is a player (Unfinished)
                if(e instanceof PlayerEntity){

                }
                else list.add(new EntityStorage(e));
            }
        }

        return list;
    }

    public List<TileStore> getTileStores(){
        return this.tileStores;
    }

}
