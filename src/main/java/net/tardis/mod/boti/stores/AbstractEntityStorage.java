package net.tardis.mod.boti.stores;

import net.minecraft.network.PacketBuffer;

public abstract class AbstractEntityStorage {

    public AbstractEntityStorage(PacketBuffer buf) {
        this.decode(buf);
    }

    public AbstractEntityStorage() {
    }

    public abstract void encode(PacketBuffer buf);

    public abstract void decode(PacketBuffer buf);
}
