package net.tardis.mod.particles.client;

import net.minecraft.client.particle.IAnimatedSprite;
import net.minecraft.client.particle.IParticleFactory;
import net.minecraft.client.particle.IParticleRenderType;
import net.minecraft.client.particle.Particle;
import net.minecraft.client.particle.SpriteTexturedParticle;
import net.minecraft.client.world.ClientWorld;
import net.minecraft.particles.BasicParticleType;
import net.tardis.mod.constants.Constants;

public class BubbleParticle extends SpriteTexturedParticle{

	private IAnimatedSprite animatedSprite;

	protected BubbleParticle(ClientWorld world, double x, double y, double z, double motionX, double motionY, double motionZ, IAnimatedSprite sprite) {
		super(world, x, y, z, motionX, motionY, motionZ);
		this.animatedSprite = sprite;
		this.selectSpriteWithAge(animatedSprite);
		this.particleScale = 0.75F;
		this.canCollide = false;
		this.maxAge = Constants.BUBBLE_PARTICLE_MAX_AGE;
		this.motionX = 0;
		this.motionY = 0;
		this.motionZ = 0;
		this.particleGravity = 0;
	}
	
	@Override
	public IParticleRenderType getRenderType() {
		return IParticleRenderType.PARTICLE_SHEET_TRANSLUCENT;
	}
	
	@Override
	public void tick() {
		super.tick();
		if(animatedSprite != null)
			this.selectSpriteWithAge(this.animatedSprite);
	}



	public static class Factory implements IParticleFactory<BasicParticleType>{
		
		IAnimatedSprite sprite;
		
		public Factory(IAnimatedSprite sprite) {
			this.sprite = sprite;
		}
		
		@Override
		public Particle makeParticle(BasicParticleType typeIn, ClientWorld worldIn, double x, double y, double z, double xSpeed, double ySpeed, double zSpeed) {
			BubbleParticle bubble = new BubbleParticle(worldIn, x, y, z, xSpeed, ySpeed, zSpeed, this.sprite);
			return bubble;
		}
		
	}
}
