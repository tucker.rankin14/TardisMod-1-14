package net.tardis.mod.entity.ai;

import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.BlockRayTraceResult;
import net.minecraft.util.math.RayTraceResult;
import net.minecraft.world.Explosion;
import net.minecraft.world.GameRules;
import net.tardis.mod.entity.projectiles.LaserEntity;
import net.tardis.mod.sounds.TSounds;

public class WeaponTypes {

    public static IWeapon DALEK_SPECIAL = new IWeapon() {
        @Override
        public void onHit(LaserEntity laserEntity, RayTraceResult result) {
            if (result.getType() == RayTraceResult.Type.BLOCK) {
                BlockRayTraceResult blockResult = (BlockRayTraceResult) result;
                BlockPos pos = blockResult.getPos();
                laserEntity.playSound(TSounds.DALEK_SW_HIT_EXPLODE.get(), 1, 1);
                GameRules.BooleanValue shouldGrief = laserEntity.world.getGameRules().get(GameRules.MOB_GRIEFING);
                laserEntity.world.createExplosion(laserEntity.getThrower(), laserEntity.getSource(), null, (double) pos.getX() + 0.5D, (double) pos.getY() + 0.5D, (double) pos.getZ() + 0.5D, 3.0F, true, shouldGrief.get() ? Explosion.Mode.DESTROY : Explosion.Mode.NONE);
            }
        }

        @Override
        public float damage() {
            return 10;
        }
    };


    public interface IWeapon {
        void onHit(LaserEntity laserEntity, RayTraceResult result);

        float damage();
    }
}
