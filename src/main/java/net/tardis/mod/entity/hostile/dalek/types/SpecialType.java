package net.tardis.mod.entity.hostile.dalek.types;

import net.minecraft.util.SoundEvent;
import net.tardis.mod.entity.ai.WeaponTypes;
import net.tardis.mod.entity.hostile.dalek.DalekEntity;
import net.tardis.mod.sounds.TSounds;

public class SpecialType extends DalekType {

    private SoundEvent[] specialFireSounds,specialAmbientSounds;

    @Override
    public WeaponTypes.IWeapon getWeapon() {
        return WeaponTypes.DALEK_SPECIAL;
    }

    @Override
    public SoundEvent getAmbientSound(DalekEntity dalekEntity) {
        if (specialAmbientSounds == null) {
            specialAmbientSounds = new SoundEvent[]{TSounds.DALEK_SW_AIM.get(), TSounds.DALEK_HOVER.get()};
        }
        return specialAmbientSounds[(int) (System.currentTimeMillis() % specialAmbientSounds.length)];
    }

    @Override
    public SoundEvent getFireSound(DalekEntity dalekEntity) {
        if (specialFireSounds == null) {
            specialFireSounds = new SoundEvent[]{TSounds.DALEK_SW_FIRE.get(), TSounds.DALEK_EXTERMINATE.get()};
        }
        return specialFireSounds[(int) (System.currentTimeMillis() % specialFireSounds.length)];
    }
}
