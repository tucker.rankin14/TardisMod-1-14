package net.tardis.mod.network.packets.console;

import net.minecraft.network.PacketBuffer;
import net.minecraftforge.fml.network.NetworkEvent;
import net.tardis.mod.network.packets.ConsoleUpdateMessage;
import net.tardis.mod.tileentities.ConsoleTile;

import java.util.function.Supplier;

public interface ConsoleData {

	void applyToConsole(ConsoleTile tile, Supplier<NetworkEvent.Context> context);
	
	void serialize(PacketBuffer buf);
	void deserialize(PacketBuffer buf);

	ConsoleUpdateMessage.DataTypes getDataType();
}
