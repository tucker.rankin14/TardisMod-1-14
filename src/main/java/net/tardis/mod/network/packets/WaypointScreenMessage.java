package net.tardis.mod.network.packets;

import java.util.List;
import java.util.function.Supplier;

import com.google.common.collect.Lists;

import net.minecraft.item.ItemStack;
import net.minecraft.network.PacketBuffer;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.math.BlockPos;
import net.minecraftforge.fml.network.NetworkEvent;
import net.tardis.mod.items.DataCrystalItem;
import net.tardis.mod.items.TItems;
import net.tardis.mod.misc.SpaceTimeCoord;
import net.tardis.mod.tileentities.WaypointBankTile;

public class WaypointScreenMessage {

	public Type type;
	public BlockPos pos;
	public List<Integer> ids = Lists.newArrayList();
	
	public WaypointScreenMessage(Type type, BlockPos pos, List<Integer> list) {
		this.type = type;
		this.pos = pos;
		
		this.ids.clear();
		this.ids.addAll(list);
	}
	
	public static void encode(WaypointScreenMessage mes, PacketBuffer buf) {
		buf.writeInt(mes.type.ordinal());
		buf.writeBlockPos(mes.pos);
		buf.writeInt(mes.ids.size());
		for(Integer i : mes.ids)
			buf.writeInt(i);
	}
	
	public static WaypointScreenMessage decode(PacketBuffer buf) {
		Type type = Type.values()[buf.readInt()];
		BlockPos pos = buf.readBlockPos();
		int size = buf.readInt();
		List<Integer> list = Lists.newArrayList();
		for(int i = 0; i < size; ++i) {
			list.add(buf.readInt());
		}
		return new WaypointScreenMessage(type, pos, list);
	}
	
	public static void handle(WaypointScreenMessage mes, Supplier<NetworkEvent.Context> context) {
		
		context.get().enqueueWork(() -> {
			TileEntity te = context.get().getSender().world.getTileEntity(mes.pos);	
			if(te instanceof WaypointBankTile) {
				WaypointBankTile waypoint = (WaypointBankTile)te;
				
				if(mes.type == Type.COPY_TO_CRYSTAL) {
					if(waypoint.getItemHandler().getStackInSlot(0).getItem() == TItems.DATA_CRYSTAL.get()) {
						for(Integer index : mes.ids) {
							SpaceTimeCoord coord = waypoint.getWaypoints().get(index).toImmutable();
							DataCrystalItem.addWaypoint(waypoint.getItemHandler().getStackInSlot(0), coord);
						}
					}
				}
				else if(mes.type == Type.COPY_TO_BANK) {
					ItemStack stack = waypoint.getItemHandler().getStackInSlot(0);
					if(stack.getItem() == TItems.DATA_CRYSTAL.get()) {
						List<SpaceTimeCoord> coords = DataCrystalItem.getStoredWaypoints(stack);
						for(Integer index : mes.ids) {
							SpaceTimeCoord coord = coords.get(index).toImmutable();
							waypoint.addWaypoint(coord);
						}
					}
				}
				else if(mes.type == Type.DELETE_FROM_BANK) {
					for(Integer id : mes.ids) {
						waypoint.deleteWaypoint(id);
						waypoint.update();
					}
				}
				else if(mes.type == Type.DELETE_FROM_CRYSTAL) {
					ItemStack stack = waypoint.getItemHandler().getStackInSlot(0);
					if(stack.getItem() == TItems.DATA_CRYSTAL.get()) {
						List<SpaceTimeCoord> coords = DataCrystalItem.getStoredWaypoints(stack);
						for(Integer id : mes.ids) {
							if(id < coords.size())
								coords.remove((int)id);
						}
						DataCrystalItem.setStoredWaypoints(stack, coords);
					}
				}
				
			}
		});
		context.get().setPacketHandled(true);
		
	}
	
	public static enum Type{
		COPY_TO_CRYSTAL,
		COPY_TO_BANK,
		DELETE_FROM_CRYSTAL,
		DELETE_FROM_BANK
	}
}
