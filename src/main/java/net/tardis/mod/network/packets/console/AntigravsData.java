package net.tardis.mod.network.packets.console;

import java.util.function.Supplier;

import net.minecraft.network.PacketBuffer;
import net.minecraftforge.fml.network.NetworkEvent.Context;
import net.tardis.mod.network.packets.ConsoleUpdateMessage;
import net.tardis.mod.tileentities.ConsoleTile;

public class AntigravsData implements ConsoleData{

	boolean active = false;
	
	public AntigravsData(boolean gravs) {
		this.active = gravs;
	}
	
	@Override
	public void applyToConsole(ConsoleTile tile, Supplier<Context> context) {
		tile.setAntiGrav(this.active);
	}

	@Override
	public void serialize(PacketBuffer buff) {
		buff.writeBoolean(this.active);
	}

	@Override
	public void deserialize(PacketBuffer buff) {
		this.active = buff.readBoolean();
	}

	@Override
	public ConsoleUpdateMessage.DataTypes getDataType() {
		return ConsoleUpdateMessage.DataTypes.ANTIGRAVS;
	}

}
