package net.tardis.mod.network.packets;

import java.util.function.Supplier;

import net.minecraft.network.PacketBuffer;
import net.minecraft.util.math.BlockPos;
import net.minecraftforge.fml.network.NetworkEvent;
import net.tardis.mod.boti.WorldShell;
import net.tardis.mod.client.ClientPacketHandler;
import net.tardis.mod.constants.Constants;

public class BOTIMessage {

	BlockPos pos = null;
	WorldShell shell = null;
	
	public BOTIMessage(WorldShell shell) {
		this.shell = shell;
	}
	
	public BOTIMessage(WorldShell shell, BlockPos pos) {
		this(shell);
		this.pos = pos;
	}
	
	public static void encode(BOTIMessage mes, PacketBuffer buf) {
		
		buf.writeBoolean(mes.pos != null);
		if(mes.pos != null)
			buf.writeBlockPos(mes.pos);
		mes.shell.writeToBuffer(buf);
	}
	
	public static BOTIMessage decode(PacketBuffer buf) {

		boolean hasPos = buf.readBoolean();
		if(hasPos) {
			BlockPos pos = buf.readBlockPos();
			return new BOTIMessage(WorldShell.readFromBuffer(buf), pos);
		}
		
		return new BOTIMessage(WorldShell.readFromBuffer(buf));
	}
	
	public static void handle(BOTIMessage mes, Supplier<NetworkEvent.Context> cont) {
		cont.get().enqueueWork(() -> {
			ClientPacketHandler.handleBOTIMessage(mes.pos, mes.shell);
		});
		cont.get().setPacketHandled(true);
	}
	
	
	
}
