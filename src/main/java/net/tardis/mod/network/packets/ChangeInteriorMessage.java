package net.tardis.mod.network.packets;

import java.util.function.Supplier;

import net.minecraft.network.PacketBuffer;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.text.TranslationTextComponent;
import net.minecraftforge.fml.network.NetworkEvent;
import net.tardis.mod.ars.ConsoleRoom;
import net.tardis.mod.config.TConfig;
import net.tardis.mod.constants.Constants;
import net.tardis.mod.helper.TardisHelper;
import net.tardis.mod.helper.WorldHelper;
import net.tardis.mod.tileentities.ConsoleTile;
import net.tardis.mod.world.dimensions.TDimensions;

public class ChangeInteriorMessage {
	
	public ResourceLocation room;
	public boolean isCancelling;
	
	public ChangeInteriorMessage(ResourceLocation room, boolean isCancelling) {
		this.room = room;
		this.isCancelling = isCancelling;
	}
	
	public static void encode(ChangeInteriorMessage mes, PacketBuffer buffer) {
		buffer.writeResourceLocation(mes.room);
		buffer.writeBoolean(mes.isCancelling);
	}
	
	public static ChangeInteriorMessage decode(PacketBuffer buffer) {
		return new ChangeInteriorMessage(buffer.readResourceLocation(), buffer.readBoolean());
	}
	
	public static void handle(ChangeInteriorMessage mes, Supplier<NetworkEvent.Context> context) {
		context.get().enqueueWork(() -> {
			if(WorldHelper.areDimensionTypesSame(context.get().getSender().world, TDimensions.DimensionTypes.TARDIS_TYPE)) {
				TileEntity te = context.get().getSender().world.getTileEntity(TardisHelper.TARDIS_POS);
				if(te instanceof ConsoleTile) {
					ConsoleTile console = (ConsoleTile)te;
					if(console.getInteriorManager().canChangeInteriorAgain() || context.get().getSender().isCreative()) {
						ConsoleRoom room = ConsoleRoom.getRegistry().get(mes.room);
						if(room != null) {
							int fuelUsage = TConfig.SERVER.interiorChangeArtronUse.get();
							if(console.getArtron() > fuelUsage) {
								if(console.canDoAdminFunction(context.get().getSender())) {
									console.setupInteriorChangeProperties(room, mes.isCancelling, context.get().getSender().abilities.isCreativeMode);
								}
								else context.get().getSender().sendStatusMessage(Constants.Translations.NOT_ADMIN, true);
							}
							else context.get().getSender().sendStatusMessage(new TranslationTextComponent(Constants.Translations.NOT_ENOUGH_ARTRON, fuelUsage), true);
						}
					}
					else context.get().getSender().sendStatusMessage(new TranslationTextComponent("message.tardis.interior_cooldown", console.getInteriorManager().getInteriorChangeTime() / 20), true);
				}
			}
		});
		context.get().setPacketHandled(true);
	}

}
