package net.tardis.mod.network.packets.console;

import java.util.function.Supplier;

import net.minecraft.network.PacketBuffer;
import net.minecraftforge.fml.RegistryObject;
import net.minecraftforge.fml.network.NetworkEvent.Context;
import net.tardis.mod.network.packets.ConsoleUpdateMessage;
import net.tardis.mod.registries.SoundSchemeRegistry;
import net.tardis.mod.sounds.AbstractSoundScheme;
import net.tardis.mod.tileentities.ConsoleTile;

public class SoundSchemeData implements ConsoleData{

	private AbstractSoundScheme key;
	
	public SoundSchemeData(AbstractSoundScheme key) {
		this.key = key;
	}
	
	@Override
	public void applyToConsole(ConsoleTile tile, Supplier<Context> context) {
		tile.setSoundScheme(this.key);
	}

	@Override
	public void serialize(PacketBuffer buff) {
		buff.writeRegistryId(this.key);
	}

	@Override
	public void deserialize(PacketBuffer buff) {
		this.key = buff.readRegistryIdSafe(AbstractSoundScheme.class);
	}

	@Override
	public ConsoleUpdateMessage.DataTypes getDataType() {
		return ConsoleUpdateMessage.DataTypes.SOUND_SCHEME;
	}

}
