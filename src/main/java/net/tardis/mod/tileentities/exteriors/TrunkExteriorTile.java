package net.tardis.mod.tileentities.exteriors;

import net.minecraft.tileentity.TileEntityType;
import net.minecraft.util.math.AxisAlignedBB;
import net.tardis.mod.texturevariants.TextureVariants;
import net.tardis.mod.tileentities.TTiles;

public class TrunkExteriorTile extends ExteriorTile{

    public static final AxisAlignedBB RENDER = new AxisAlignedBB(-1, -1, -1, 2, 2, 2);
	
	public TrunkExteriorTile(TileEntityType<?> tileEntityTypeIn) {
		super(tileEntityTypeIn);
		this.setVariants(TextureVariants.TRUNK);
	}
	
	public TrunkExteriorTile() {
		this(TTiles.EXTERIOR_TRUNK.get());
	}

	@Override
	public AxisAlignedBB getDoorAABB() {
		return this.getDefaultEntryBox();
		
	}

    @Override
    public AxisAlignedBB getRenderBoundingBox() {
        return RENDER.offset(getPos());
    }

}
