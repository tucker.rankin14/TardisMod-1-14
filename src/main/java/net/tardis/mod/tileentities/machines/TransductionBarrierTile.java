package net.tardis.mod.tileentities.machines;

import net.minecraft.block.BlockState;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.network.NetworkManager;
import net.minecraft.network.play.server.SUpdateTileEntityPacket;
import net.minecraft.tileentity.ITickableTileEntity;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.tileentity.TileEntityType;
import net.minecraft.util.Direction;
import net.minecraftforge.common.capabilities.Capability;
import net.minecraftforge.common.util.LazyOptional;
import net.minecraftforge.energy.CapabilityEnergy;
import net.minecraftforge.energy.EnergyStorage;
import net.tardis.mod.tileentities.TTiles;
import net.tardis.mod.tileentities.ConsoleTile;

public class TransductionBarrierTile extends TileEntity implements ITickableTileEntity{

	private String landingCode = "";
	
	private EnergyStorage power = new EnergyStorage(1500);
	private LazyOptional<EnergyStorage> powerHolder = LazyOptional.of(() -> this.power);
	
	public TransductionBarrierTile(TileEntityType<?> tileEntityTypeIn) {
		super(tileEntityTypeIn);
	}
	
	public TransductionBarrierTile() {
		this(TTiles.TRANSDUCTION_BARRIER.get());
	}
	
	public void setCode(String code) {
		this.landingCode = code;
		this.markDirty();
	}
	
	public String getCode() {
		return this.landingCode;
	}
	
	public boolean canLand(ConsoleTile tile) {
		return this.power.getEnergyStored() > 100 &&
				tile.getLandingCode().toLowerCase().contentEquals(landingCode.toLowerCase());
	}
	
	public void onBlockedTARDIS(ConsoleTile tile) {
		this.power.extractEnergy(100, false);
		this.markDirty();
	}

	@Override
	public void read(BlockState state, CompoundNBT compound) {
		super.read(state, compound);
		this.landingCode = compound.getString("landing_code");
		this.power.receiveEnergy(compound.getInt("power"), false);
	}

	@Override
	public CompoundNBT write(CompoundNBT compound) {
		compound.putString("landing_code", this.landingCode);
		compound.putInt("power", this.power.getEnergyStored());
		return super.write(compound);
	}

	@Override
	public void onDataPacket(NetworkManager net, SUpdateTileEntityPacket pkt) {
		super.onDataPacket(net, pkt);
		this.deserializeNBT(pkt.getNbtCompound());
	}

	@Override
	public SUpdateTileEntityPacket getUpdatePacket() {
		return new SUpdateTileEntityPacket(this.getPos(), -1, this.getUpdateTag());
	}

	@Override
	public CompoundNBT getUpdateTag() {
		return this.serializeNBT();
	}

	@Override
	public <T> LazyOptional<T> getCapability(Capability<T> cap, Direction side) {
		
		if(cap == CapabilityEnergy.ENERGY)
			return powerHolder.cast();
		
		return super.getCapability(cap, side);
	}

	@Override
	public void tick() {
		if(!world.isRemote && power.getEnergyStored() > 0 && world.getGameTime()+ 5 % 20 == 0)
			this.power.extractEnergy(1, false);
	}
	
}
